import { Component, ElementRef, ViewChild } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Color } from "tns-core-modules/color/color";
import { User } from "~/app/models/user.model";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { UserService } from "~/app/services/user.service";
import { Page } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css'],
})

export class LoginComponent {

    inputStyle = 'inputInactive';
    textfield: TextField;
    forgot_password: boolean;
    userNameText: string = "";
    passwordText: string = "";
    userIcon: string;
    passwordIcon: string;
    isRendering: boolean;
    renderViewTimeout;
    user: User;
    isLoading: boolean;

    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.userIcon = "res://profile";
        this.passwordIcon = "res://password";
        this.user = new User();
        this.isLoading = false;
        this.userService.activeScreen("login");

        if (localstorage.getItem("userId") != null && localstorage.getItem("userId") != undefined) {
            if (localstorage.getItem("userType") == "student") {
                this.routerExtensions.navigate(['./homeStudent'], {
                    clearHistory: true
                });
            }
            else if (localstorage.getItem("userType") == "teacher") {
                this.routerExtensions.navigate(['./homeTeacher'], {
                    clearHistory: true
                });
            }
            else {
                this.routerExtensions.navigate(['./homeAdmin'], {
                    clearHistory: true
                });
            }
        }
    }

    ngOnInit(): void {
        this.inputStyle = 'inputInactive';
        this.forgot_password = false;
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }


    onGridLoaded(args: any) {
        var gridMain = <any>args.object;
        setTimeout(() => {
            if (gridMain.android) {
                let nativeGridMain = gridMain.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridMain.ios) {
                let nativeGridMain = gridMain.ios;
                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 400)

    }

    public userNameTextField(args) {
        var textField = <TextField>args.object;
        this.userNameText = textField.text;
    }

    public passwordTextField(args) {
        var textField = <TextField>args.object;
        this.passwordText = textField.text;
    }

    onLoginClick() {
        if (this.userNameText == "") {
            alert("Username cannot be empty");
        }
        else if (this.passwordText == "") {
            alert("Passsword cannot be empty");
        }
        else {
            this.user.userName = "";
            this.user.email = "";
            if (this.userNameText.includes("@")) {
                this.user.email = this.userNameText;
            } else {
                this.user.userName = this.userNameText;
            }
            this.isLoading = true;
            this.user.password = this.passwordText;
            this.userService.showLoadingState(true);
            this.http
                .post(Values.BASE_URL + "users/login", this.user)
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.trace(res);
                            // this.userService.showLoadingState(false);
                            localstorage.setItem("userId", res.data.id);
                            localstorage.setItem("userType", res.data.userType);
                            localstorage.setItem("profilePic", res.data.image_url);
                            localstorage.setItem("userName", res.data.name);
                            localstorage.setItem("classId", res.data.classId);
                            localstorage.setItem("classSection", res.data.classSection);
                            localstorage.setItem("token", res.data.token);
                            localstorage.setItem("admissionNumber", res.data.admissionNo);
                            console.log("USER TYPE:::::", res.data.userType);
                            if (res.data.userType == "teacher") {
                                console.log("Incharge:::", res.data.classIncharge);
                                localstorage.setItem("classIncharge", res.data.classIncharge);
                                localstorage.setItem("regNo", res.data.regNo);
                            }
                            Toast.makeText("Login successfully!!!", "long").show();
                            this.isLoading = false;
                            if (res.data.userType == "student") {
                                this.routerExtensions.navigate(['./homeStudent'], {
                                    clearHistory: true
                                });
                            }
                            else if (res.data.userType == "teacher") {
                                this.routerExtensions.navigate(['./homeTeacher'], {
                                    clearHistory: true
                                });
                            }
                            else {
                                this.routerExtensions.navigate(['./homeAdmin'], {
                                    clearHistory: true
                                });
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    alert(error.error.error);
                });
        }
    }

    onForgotPassword() {
        this.router.navigate(['/forgotPassword']);
    }

}