import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from "~/app/services/user.service";
import * as Toast from 'nativescript-toast';
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { ModalComponent } from "~/app/modals/modal.component";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-teachers",
    moduleId: module.id,
    templateUrl: "./teachers.component.html",
    styleUrls: ['./teachers.component.css'],
})

export class TeachersComponent implements OnInit {

    // @ViewChild('selectClassDialog') selectClassDialog: ModalComponent;
    // @ViewChild('selectSectionDialog') selectSectionDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    teachers;
    year: string;
    classId: string;
    profilePic: string;
    isLoading: boolean;
    section: string;
    pageNo: number;
    count: number;
    headers: HttpHeaders;
    token: string;
    // isClassPicker: boolean;
    // isSectionPicker: boolean;
    // classes;
    // sections;
    // classButton: string;
    // sectionButton: string;
    // height: string;
    // marginTop: string;
    isClassTeacher: boolean;
    // isClassPicker: boolean;
    // isSectionPicker: boolean;
    // classes;
    // sections;
    // classButton: string;
    // sectionButton: string;
    query: string;
    rowHeight: string;
    height: string;
    query1: string;
    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.classId = "";
        this.teachers = [];
        this.profilePic = "res://profile";
        this.isLoading = false;
        this.section = "";
        this.pageNo = 1;
        this.count = 10;
        this.token = "";
        this.isClassTeacher = true;
        this.rowHeight = "40%";
        this.height = "20%";
        // this.classButton = "Select Class";
        // this.sectionButton = "Select Section";
        this.query = "&classIncharge=true";
        this.userService.activeScreen("teachers");
        this.query1 = "";
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            if (localstorage.getItem("userType") == "student") {
                this.query1 = `&classId=${localstorage.getItem("classId")}&classSection=${localstorage.getItem("classSection")}`;
                this.getTeachers();
            }
            else {
                this.query1 = "";
                this.getTeachers();
            }
            // if (localstorage.getItem("userType") == "student") {
            //     this.isClassPicker = false;
            //     this.isSectionPicker = false;
            // this.classId = localstorage.getItem("classId");
            // this.section = localstorage.getItem("classSection");
            // } else {
            //     this.isClassPicker = true;
            //     this.isSectionPicker = true;
            // }
        }
        // this.classButton = "Select Class";
        // this.sectionButton = "Select Section";
        // this.classes = [];
        // this.sections = [];

        // if (localstorage.getItem("userType") == "admin") {
        //     this.isClassPicker = true;
        //     this.isSectionPicker = false;
        //     this.height = "50%";
        //     this.marginTop = "32%";
        // }
        // else {
        //     this.isClassPicker = false;
        //     this.isSectionPicker = false;
        //     this.height = "65%";
        //     this.marginTop = "35%";
        // }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
        // this.birthdays.push({ date: "07 April", name: "Aarti", });
        // this.birthdays.push({ date: "14 April", name: "Emmanual", });
        // this.birthdays.push({ date: "16 April", name: "Abhishek Shukla", });
        // this.birthdays.push({ date: "21 April", name: "Diksha Rani", });
        // this.birthdays.push({ date: "28 April", name: "Shubham", });
        // this.birthdays.push({ date: "05 May", name: "Mukesh Kumar)", });
        // this.birthdays.push({ date: "12 May", name: "Aakshi Jain", });
        // this.birthdays.push({ date: "15 May", name: "Anita Rani", });
        // this.birthdays.push({ date: "19 May", name: "Anupreet Kaur", });
        // this.birthdays.push({ date: "26 May", name: "Aarti Rani", });

    }

    // onClass() {
    //     this.isSectionPicker = false;
    //     this.sectionButton = "Select Section";
    //     this.birthdays = [];
    //     this.getClasses();
    // }

    // onSection() {
    //     this.getSections();
    // }

    // getClasses() {
    //     this.isLoading = true;
    //     this.http
    //         .get(Values.BASE_URL + "classes", {
    //             headers: this.headers
    //         })
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     this.classes = [];
    //                     for (var i = 0; i < res.data.length; i++) {
    //                         this.classes.push({
    //                             id: res.data[i].id,
    //                             name: res.data[i].name,
    //                         });
    //                     }
    //                     this.selectClassDialog.show();
    //                     // this.userService.showLoadingState(false);
    //                     this.isLoading = false;
    //                 }
    //             }
    //         }, error => {
    //             // this.userService.showLoadingState(false);
    //             this.isLoading = false;
    //             console.log(error.error.error);
    //         });
    // }

    // getSections() {
    //     this.isLoading = true;
    //     this.http
    //         .get(Values.BASE_URL + "classes/" + this.classId, {
    //             headers: this.headers
    //         })
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     this.sections = [];
    //                     for (var i = 0; i < res.data.section.length; i++) {
    //                         this.sections.push({
    //                             name: res.data.section[i].classSection,
    //                         });
    //                     }
    //                     this.selectSectionDialog.show();
    //                     // this.userService.showLoadingState(false);
    //                     this.isLoading = false;
    //                 }
    //             }
    //         }, error => {
    //             // this.userService.showLoadingState(false);
    //             this.isLoading = false
    //             console.log(error.error.error);
    //         });
    // }

    // onClassButton(item: any) {
    //     this.classId = item.id;
    //     this.classButton = item.name;
    //     this.isSectionPicker = true;
    //     this.sectionButton = "Select Section";
    //     this.section = "";
    // }

    // onSectionButton(item: any) {
    //     this.sectionButton = item.name;
    //     this.section = item.name;
    //     console.log("CLASS ID:::", this.classId);
    //     console.log("SECTION NAME:::", this.section);
    //     this.pageNo = 1;
    //     this.count = 10;
    //     this.teachers = [];
    //     this.getTeachers();
    // }

    // onClass() {
    //     this.getClasses();
    //     this.selectClassDialog.show();
    // }

    // onSection() {
    //     if (this.classId == "") {
    //         alert("Please select class first.");
    //     } else {
    //         this.getSections();
    //         this.selectSectionDialog.show();
    //     }
    // }

    // onSectionOutsideClick() {
    //     this.selectSectionDialog.hide();
    // }

    // onClassOutsideClick() {
    //     this.selectClassDialog.hide();
    // }

    // onClassOutsideClick() {
    //     this.selectClassDialog.hide();
    // }

    // onSectionOutsideClick() {
    //     this.selectSectionDialog.hide();
    // }

    // getClassSection() {
    //     this.isLoading = true;
    //     this.http
    //         .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"))
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     this.classId = res.data.classId;
    //                     this.section = res.data.classSection;
    //                     this.getBirthdays();
    //                 }
    //             }
    //         }, error => {
    //             this.isLoading = false;
    //             console.log(error.error.error);
    //         });
    // }

    getTeachers() {
        console.log(Values.BASE_URL + `users/search?pageNo=${this.pageNo}&count=${this.count}&userType=teacher&active=true` + this.query1 + this.query);
        this.http
            .get(Values.BASE_URL + `users/search?pageNo=${this.pageNo}&${this.count}&userType=teacher&active=true` + this.query1 + this.query, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.trace("RES::::", res);
                        if (res.data.users.length > 0) {
                            for (var i = 0; i < res.data.users.length; i++) {
                                // if (res.data.users[i].classIncharge == "false") {
                                var classNames = "";
                                for (var j = 0; j < res.data.users[i].classTeacher.length; j++) {
                                    var className = res.data.users[i].classTeacher[j].name + "(";
                                    for (var k = 0; k < res.data.users[i].classTeacher[j].section.length; k++) {
                                        className = className + res.data.users[i].classTeacher[j].section[k].classSection;
                                        if (k < res.data.users[i].classTeacher[j].section.length - 1) {
                                            className = className + ", ";
                                        }
                                    }
                                    classNames = classNames + className + ")";
                                    if (j < res.data.users[i].classTeacher.length - 1) {
                                        classNames = classNames + ", "
                                    }
                                }
                                if (res.data.users[i].classIncharge == "true") {
                                    var classIncharge = res.data.users[i].className + " (" + res.data.users[i].classSection + ")";
                                }
                                this.teachers.push({
                                    className: classNames,
                                    classIncharge: classIncharge,
                                    classSection: res.data.users[i].classSection,
                                    email: res.data.users[i].email,
                                    name: res.data.users[i].firstName + " " + res.data.users[i].lastName,
                                    subject: res.data.users[i].subjectTeacher
                                });
                                // } else {
                                //     this.teachers.push({
                                //         className: res.data.users[i].className,
                                //         classSection: res.data.users[i].classSection,
                                //         email: res.data.users[i].email,
                                //         name: res.data.users[i].firstName + " " + res.data.users[i].lastName,
                                //         subject: res.data.users[i].subjectTeacher
                                //     });
                                // }
                            }
                            this.pageNo = this.pageNo + 1;
                            this.getTeachers();
                            this.isLoading = false;
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    // onClassDialogLoaded(args) {
    //     var classDialog = <any>args.object;
    //     setTimeout(() => {
    //         if (classDialog.android) {
    //             let nativeImageView = classDialog.android;
    //             var shape = new android.graphics.drawable.GradientDrawable();
    //             shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
    //             shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
    //             nativeImageView.setElevation(20)
    //         } else if (classDialog.ios) {
    //             let nativeImageView = classDialog.ios;
    //             nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
    //             nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
    //             nativeImageView.layer.shadowOpacity = 0.5
    //             nativeImageView.layer.shadowRadius = 5.0
    //         }
    //     }, 400)
    // }

    // onSectionDialogLoaded(args) {
    //     var sectionDialog = <any>args.object;
    //     setTimeout(() => {
    //         if (sectionDialog.android) {
    //             let nativeImageView = sectionDialog.android;
    //             var shape = new android.graphics.drawable.GradientDrawable();
    //             shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
    //             shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
    //             nativeImageView.setElevation(20)
    //         } else if (sectionDialog.ios) {
    //             let nativeImageView = sectionDialog.ios;
    //             nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
    //             nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
    //             nativeImageView.layer.shadowOpacity = 0.5
    //             nativeImageView.layer.shadowRadius = 5.0
    //         }
    //     }, 400)
    // }

    onBack() {
        this.routerExtensions.back();
    }

    onClassTeacher() {
        this.isClassTeacher = true;
        this.rowHeight = "40%";
        this.height = "20%";
        this.query = "&classIncharge=true";
        this.pageNo = 1;
        this.count = 10;
        this.teachers = [];
        this.getTeachers();
    }

    onSubjectTeacher() {
        this.isClassTeacher = false;
        this.rowHeight = "35%";
        this.height = "25%";
        this.query = "&classIncharge=false";
        this.pageNo = 1;
        this.count = 10;
        this.teachers = [];
        this.getTeachers();
    }

}