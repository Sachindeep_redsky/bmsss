import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ViewHomeworkComponent } from "./components/view-homework.component";
import { ViewHomeworkRoutingModule } from "./view-homework-routing.module";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from 'nativescript-ng-shadow';


@NgModule({
    imports: [
        HttpModule,
        ViewHomeworkRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        ViewHomeworkComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class ViewHomeworkModule { }
