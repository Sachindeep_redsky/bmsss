import { UserService } from './../../services/user.service';
import { Component, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-viewHomework",
    moduleId: module.id,
    templateUrl: "./view-homework.component.html",
    styleUrls: ['./view-homework.component.css'],
})

export class ViewHomeworkComponent implements OnInit {


    renderViewTimeout;
    isRendering: boolean;
    homework = [];
    date: string;

    constructor(private routerExtensions: RouterExtensions, private userService: UserService) {
        this.isRendering = false;
        this.date = "14 july,2019 (Saturday)";
        this.userService.activeScreen("viewHomework");
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)

        this.homework.push({ subjectName: "Hindi", homework: "Revise unseen passage page no 399 grammar book", });
        this.homework.push({ subjectName: "English", homework: "Test of letter writing (Formal)", });
        this.homework.push({ subjectName: "Mathematics", homework: "Test of chapter-2 on Friday", });
        this.homework.push({ subjectName: "Punjabi", homework: "Test of first five chapters", });
        this.homework.push({ subjectName: "Hindi", homework: "Revise unseen passage page no 399 grammar book", });
        this.homework.push({ subjectName: "English", homework: "Test of letter writing (Formal)", });
        this.homework.push({ subjectName: "Mathematics", homework: "Test of chapter-2 on Friday", });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onHomeworkLoaded(args) {
        var homework = <any>args.object;

        setTimeout(() => {
            if (homework.android) {
                let nativeImageView = homework.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (homework.ios) {
                let nativeImageView = homework.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onBack() {
        alert("back click");
    }

    onLeftArrow() {
        alert("left click");
    }

    onRightArrow() {
        alert("right click");
    }
}