import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ViewHomeworkComponent } from "./components/view-homework.component";

const routes: Routes = [
    { path: "", component: ViewHomeworkComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ViewHomeworkRoutingModule { }
