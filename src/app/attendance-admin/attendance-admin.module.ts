import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { RadioButtonModule } from '../shared/radio-button/radio-button.module'
import { AttendanceAdminRoutingModule } from "./attendance-admin-routing.module";
import { AttendanceAdminComponent } from "./components/attendance-admin.component";
import { NgModalModule } from "../modals/ng-modal";
import { GridViewModule } from "nativescript-grid-view/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
    imports: [
        HttpModule,
        AttendanceAdminRoutingModule,
        NgModalModule,
        GridViewModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        AttendanceAdminComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AttendanceAdminModule { }