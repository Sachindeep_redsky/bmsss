import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { ModalComponent } from '~/app/modals/modal.component';
import * as localstorage from "nativescript-localstorage";
import { DatePicker } from "tns-core-modules/ui/date-picker";
import { Values } from '~/app/values/values';
import { UserService } from "~/app/services/user.service";

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-attendanceAdmin",
    moduleId: module.id,
    templateUrl: "./attendance-admin.component.html",
    styleUrls: ['./attendance-admin.component.css'],
})

export class AttendanceAdminComponent {

    @ViewChild('viewDatePickerDialog', { static: false }) viewDatePickerDialog: ModalComponent;
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;
    @ViewChild('selectSectionDialog', { static: false }) selectSectionDialog: ModalComponent;
    @ViewChild('selectRollNoDialog', { static: false }) selectRollNoDialog: ModalComponent;

    students;
    renderViewTimeout;
    isRendering: boolean;
    date: string;
    classButton: string;
    sectionButton: string;
    rollNoButton: string;
    day: number;
    month: number;
    year: number;
    isClassPicker: boolean;
    isSectionPicker: boolean;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    classes;
    classId: string;
    sectionName: string;
    sections;
    rollNumbers;
    rollNumber: string;
    noOfStudents: number;
    attendanceList;
    query: string;
    pageNo: number;
    count: number;
    isData: boolean;
    attendanceMessage: string;
    forRollNo: boolean;
    classTeacher;
    index: number;
    profilePic: string;
    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        var date = new Date();
        this.classes = [];
        this.students = [];
        this.classId = "";
        this.sectionName = "";
        this.sections = [];
        this.rollNumbers = [];
        this.attendanceList = [];
        this.rollNumber = "";
        this.noOfStudents = 0;
        this.day = date.getDate();
        this.month = (date.getMonth() + 1);
        this.year = date.getFullYear();
        this.date = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
        this.classButton = "Select Class";
        this.sectionButton = "Select Section";
        this.rollNoButton = "Select Roll No."
        this.isClassPicker = true;
        this.isSectionPicker = true;
        this.isLoading = false;
        this.token = "";
        this.pageNo = 1;
        this.count = 10;
        this.isData = true;
        this.attendanceMessage = "Select class, section and roll number to show the attendance.";
        this.forRollNo = false;
        this.classTeacher = [];
        this.index = 0;
        this.profilePic = "res://profile";
        this.userService.activeScreen("attendanceAdmin");
        if (date.getDate() < 10) {
            if ((date.getMonth() + 1) < 10) {
                this.date = '0' + date.getDate() + '/0' + (date.getMonth() + 1) + '/' + date.getFullYear();
            }
            else {
                this.date = '0' + date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            }
        }
        else {
            if ((date.getMonth() + 1) < 10) {
                this.date = date.getDate() + '/0' + (date.getMonth() + 1) + '/' + date.getFullYear();
            }
            else {
                this.date = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            }
        }

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
        // this.query = `pageNo=${this.pageNo}&${this.count}&active=${this.active}`;
    }

    onBack() {
        // this.router.navigate(['/homeTeacher']);
        this.routerExtensions.back();
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onDatePickerDialogLoaded(args) {
        var datePickerDialog = <any>args.object;
        setTimeout(() => {
            if (datePickerDialog.android) {
                let nativeImageView = datePickerDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (datePickerDialog.ios) {
                let nativeImageView = datePickerDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSectionDialogLoaded(args) {
        var sectionDialog = <any>args.object;
        setTimeout(() => {
            if (sectionDialog.android) {
                let nativeImageView = sectionDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (sectionDialog.ios) {
                let nativeImageView = sectionDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onRollNoDialogLoaded(args) {
        var rollNoDialog = <any>args.object;
        setTimeout(() => {
            if (rollNoDialog.android) {
                let nativeImageView = rollNoDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (rollNoDialog.ios) {
                let nativeImageView = rollNoDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onAttendanceLoaded(args) {
        var attendance = <any>args.object;
        setTimeout(() => {
            if (attendance.android) {
                let nativeImageView = attendance.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                nativeImageView.setElevation(5)
            } else if (attendance.ios) {
                let nativeImageView = attendance.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onDate() {
        this.viewDatePickerDialog.show();
        // this.classButton = "Select Class";
        // this.homeworkList = [];
        // this.isClassPicker = false;
        // this.isSectionPicker = false;
    }

    onPickerLoaded(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.year = date.getFullYear();
        datePicker.month = date.getMonth() + 1;
        datePicker.day = date.getDate();
        datePicker.minDate = new Date(2020, 3, 1);
        datePicker.maxDate = new Date(2021, 2, 31);
    }

    onDateChanged(args) {

    }

    onDayChanged(args) {
        this.day = args.value;
    }

    onMonthChanged(args) {
        this.month = args.value;
    }

    onYearChanged(args) {
        this.year = args.value;
    }

    onOkDate() {
        this.viewDatePickerDialog.hide();
        // this.date = this.day + "/" + this.month + "/" + this.year;
        // this.isClassPicker = true;
        if (this.day < 10) {
            if ((this.month + 1) < 10) {
                this.date = '0' + this.day + '/0' + (this.month) + '/' + this.year;
            }
            else {
                this.date = '0' + this.day + '/' + (this.month) + '/' + this.year;
            }
        }
        else {
            if ((date.getMonth() + 1) < 10) {
                this.date = this.day + '/0' + (this.month) + '/' + this.year;
            }
            else {
                this.date = this.day + '/' + (this.month) + '/' + this.year;
            }
        }
        this.rollNumber = "";
        this.rollNoButton = "Select Roll No."
        if (this.classId != "" && this.sectionName != "") {
            this.query = `date=${this.date}&classId=${this.classId}&section=${this.sectionName}`;
            this.attendanceList = [];
            this.pageNo = 1;
            this.count = 10;
            this.getAttendance(this.query);
        }
    }

    onClass() {
        // this.isSectionPicker = false;
        // this.sectionButton = "Select Section";
        // this.homeworkList = [];
        // this.classes = [];

        // this.selectClassDialog.show();
        this.pageNo = 1;
        this.getClasses();
    }

    onSection() {
        if (this.classId != "" && this.classId != undefined) {
            if (localstorage.getItem("classIncharge") == "false") {
                this.sections = [];
                console.log("INDEX:::", this.index)
                console.log(this.classTeacher[this.index].section.length);
                for (var i = 0; i < this.classTeacher[this.index].section.length; i++) {
                    this.sections.push({
                        name: this.classTeacher[this.index].section[i].classSection,
                    });
                }
                this.selectSectionDialog.show();
            }
            else {
                this.getSections();
            }
        }
        else {
            alert("Please select class first.");
        }
    }

    getClasses() {
        this.isLoading = true;
        if (localstorage.getItem("classIncharge") == "false") {
            this.http
                .get(Values.BASE_URL + `users/search?pageNo=${this.pageNo}&userType=teacher&regNo=${localstorage.getItem("regNo")}`, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("RESSSS", res);
                            if (res.data.users[0].classTeacher.length > 0) {
                                this.classTeacher = res.data.users[0].classTeacher;
                                console.trace("CLASS TEACHER", this.classTeacher);
                                this.classes = [];
                                for (var i = 0; i < this.classTeacher.length; i++) {
                                    this.classes.push({
                                        index: i,
                                        id: this.classTeacher[i].id,
                                        name: this.classTeacher[i].name
                                    });
                                }
                                this.selectClassDialog.show();
                                this.isLoading = false;
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
        else {
            this.http
                .get(Values.BASE_URL + "classes", {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        this.classes = [];
                        if (res.isSuccess == true) {
                            for (var i = 0; i < res.data.length; i++) {
                                this.classes.push({
                                    id: res.data[i].id,
                                    name: res.data[i].name,
                                });
                            }
                            this.selectClassDialog.show();
                            //     // this.userService.showLoadingState(false);
                            this.isLoading = false;
                        }
                    }
                }, error => {
                    // this.userService.showLoadingState(false);
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    getSections() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "classes/" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.sections = [];
                        for (var i = 0; i < res.data.section.length; i++) {
                            this.sections.push({
                                name: res.data.section[i].classSection,
                            });
                        }
                        this.selectSectionDialog.show();
                        // this.userService.showLoadingState(false);
                        this.isLoading = false;
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false
                console.log(error.error.error);
            });
    }

    onClassButton(item: any) {
        this.index = item.index;
        this.classId = item.id;
        this.classButton = item.name;
        this.isSectionPicker = true;
        console.log(this.rollNumber);
        this.rollNumber = "";
        this.rollNoButton = "Select Roll No."
        this.forRollNo = false;
        if (this.sectionName != "") {
            this.query = `date=${this.date}&classId=${this.classId}&section=${this.sectionName}`;
            this.attendanceList = [];
            this.rollNumbers = [];
            this.pageNo = 1;
            this.count = 10;
            this.getAttendance(this.query);
        }
        // if (this.sectionName != "" && this.rollNumber != "") {
        //     this.query = `pageNo=${this.pageNo}&count=${this.count}&date=${this.date}&classId=${this.classId}&section=${this.sectionName}&rollNo=${this.rollNumber}`;
        //     this.attendanceList = [];
        //     // this.rollNumbers = [];
        //     this.getAttendance(this.query);
        // }
    }

    onSectionButton(item: any) {
        this.sectionName = item.name;
        this.sectionButton = this.sectionName;
        // this.section = item.name;
        // if (localstorage.getItem("userType") == "admin") {
        //     this.getHomework();
        // }
        this.forRollNo = false;
        this.query = `date=${this.date}&classId=${this.classId}&section=${this.sectionName}`;
        this.attendanceList = [];
        this.rollNumbers = [];
        this.pageNo = 1;
        this.count = 10;
        this.getAttendance(this.query);
        if (this.rollNumber != "") {
            this.forRollNo = true;
            this.query = `date=${this.date}&classId=${this.classId}&section=${this.sectionName}&rollNo=${this.rollNumber}`;
            this.attendanceList = [];
            this.pageNo = 1;
            this.count = 10;
            this.getAttendance(this.query);
        }
    }

    onClassOutsideClick() {
        this.selectClassDialog.hide();
    }

    onSectionOutsideClick() {
        this.selectSectionDialog.hide();
    }

    onRollNoOutsideClick() {
        this.selectRollNoDialog.hide();
    }

    onRollNo() {
        if (this.classId == "") {
            alert("Please select class first");
        }
        else if (this.sectionName == "") {
            alert("Please select section first.");
        }
        else {
            this.getRollNumbers();
        }
    }

    getRollNumbers() {
        // this.rollNumbers = [];
        if (this.noOfStudents > 0) {
            for (var i = 1; i <= this.noOfStudents; i++) {
                this.rollNumbers.push({ 'name': i });
            }
        }
        // this.rollNumbers.push({ 'name': '1' });
        // this.rollNumbers.push({ 'name': '2' });
        this.selectRollNoDialog.show();
        // this.isLoading = true;
        // this.http
        //     .get(Values.BASE_URL + "classes/" + this.classId, {
        //         headers: this.headers
        //     })
        //     .subscribe((res: any) => {
        //         if (res != null && res != undefined) {
        //             if (res.isSuccess == true) {
        //                 console.log(res);
        //                 this.isLoading = false;
        //             }
        //         }
        //     }, error => {
        //         // this.userService.showLoadingState(false);
        //         this.isLoading = false;
        //         console.log(error.error.error);
        //     });
    }

    onRollNumberButton(item: any) {
        this.rollNumber = item.name;
        this.rollNoButton = this.rollNumber;
        this.query = `date=${this.date}&classId=${this.classId}&section=${this.sectionName}&rollNo=${this.rollNumber}`;
        this.attendanceList = [];
        this.forRollNo = true;
        this.pageNo = 1;
        this.count = 10;
        this.getAttendance(this.query);
    }

    getAttendance(query: any) {
        this.isLoading = true;
        console.log(`${Values.BASE_URL}attendanceRecords/search?pageNo=${this.pageNo}&count=${this.count}&${query}`);
        this.http.get(`${Values.BASE_URL}attendanceRecords/search?pageNo=${this.pageNo}&count=${this.count}&${query}`, {
            headers: this.headers
        })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.isLoading = false;
                        console.log(res);
                        this.noOfStudents = res.data.totalCount;
                        if (res.data.attendance.length > 0) {
                            this.isData = false;
                            for (var i = 0; i < res.data.attendance.length; i++) {
                                var attendanceStatus = "";
                                if (res.data.attendance[i].status == "present") {
                                    attendanceStatus = "P";
                                }
                                else if (res.data.attendance[i].status == "absent") {
                                    attendanceStatus = "A";
                                }
                                else {
                                    attendanceStatus = "L";
                                }
                                var annualPercentage = parseInt(res.data.attendance[i].percentageCount);
                                this.attendanceList.push({
                                    id: res.data.attendance[i].id,
                                    name: res.data.attendance[i].name,
                                    class: res.data.attendance[i].className,
                                    section: res.data.attendance[i].section,
                                    rollNo: res.data.attendance[i].rollNo,
                                    attendanceStatus: attendanceStatus,
                                    attendancePercentage: annualPercentage.toPrecision(4)
                                })
                            }
                            if (this.forRollNo == false) {
                                this.pageNo = this.pageNo + 1;
                                this.getAttendance(query);
                            }
                        }
                        else {
                            if (this.pageNo == 1) {
                                this.isData = true;
                                this.attendanceMessage = "There is no attendance for this student.";
                            }
                        }
                        // var n = (res.data.length + 1) - this.count;
                        // if (n > 0) {
                        //     this.count = this.count + 1;
                        //     this.pageNo = this.pageNo + 1;
                        //     this.query = `pageNo=${this.pageNo}&${this.count}&date=${this.date}&classId=${this.classId}&section=${this.sectionName}&rollNo=${this.rollNumber}`;
                        //     this.getAttendance(this.query);
                        // }
                        // if (n >= 10) {
                        //     this.count = this.count + 1;
                        //     this.pageNo = this.pageNo + 1;
                        //     this.query = `pageNo=${this.pageNo}&count=${this.count}&date=${this.date}&classId=${this.classId}&section=${this.sectionName}&rollNo=${this.rollNumber}`;
                        //     this.getAttendance(this.query);
                        // }
                        // if (n < 10 && n > 0) {
                        //     this.count = this.count + n;
                        //     this.pageNo = this.pageNo + 1;
                        //     this.query = `pageNo=${this.pageNo}&count=${this.count}&date=${this.date}&classId=${this.classId}&section=${this.sectionName}&rollNo=${this.rollNumber}`;
                        //     this.getAttendance(this.query);
                        // }
                        // }
                        // else {
                        //   this.isVisibleTable = false;
                        // }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log("ERROR::::", error.error.error);
            });
    }

    onAttendanceClick() {
        // this.router.navigate(['/homeTeacher'])
    }

}