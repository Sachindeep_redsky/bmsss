import { Component } from "@angular/core";
import { TextField } from "tns-core-modules/ui/text-field";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Color } from "tns-core-modules/color/color";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { UserService } from "~/app/services/user.service";
import { User } from "~/app/models/user.model";
import { Page } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-forgotpassword",
    moduleId: module.id,
    templateUrl: "./forgotpassword.component.html",
    styleUrls: ['./forgotpassword.component.css'],
})

export class ForgotPasswordComponent {

    inputStyle = 'inputInactive';
    textfield: TextField;
    emailTerm: string;
    emailTerm2: string;
    emailText: string;
    userIcon: string;
    emailIcon: string;
    isRendering: boolean;
    renderViewTimeout;
    user: User;
    isLoading: boolean;

    constructor(private router: Router, private userService: UserService, private http: HttpClient, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.emailText = "";
        this.isRendering = false;
        this.userIcon = 'res://profile'
        this.emailTerm = "Please enter the email you used to register. Check"
        this.emailTerm2 = "your email for the instructions to receive your password"
        this.user = new User();
        this.isLoading = false;
        this.userService.activeScreen("forgotPassword");
    }

    ngOnInit(): void {
        this.inputStyle = 'inputInactive';
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
    }

    onBack() {
        // this.router.navigate(['/login']);
        this.routerExtensions.back();
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }


    onGridLoaded(args: any) {
        var gridMain = <any>args.object;
        setTimeout(() => {
            if (gridMain.android) {
                let nativeGridMain = gridMain.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridMain.ios) {
                let nativeGridMain = gridMain.ios;
                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 400)
    }

    public emailTextField(args) {
        var textField = <TextField>args.object;
        this.emailText = textField.text;
    }

    onSendOtp() {
        if (this.emailText == "") {
            alert("Email cannot be empty");
            return;
        }
        else {
            this.user.email = this.emailText;
            this.isLoading = true;
            this.http
                .post(Values.BASE_URL + "users/otp", this.user)
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.isLoading = false;
                            localstorage.setItem("otpVerifyToken", res.data.otpVerifyToken);
                            this.userService.showLoadingState(false);
                            this.router.navigate(['/otp']);
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }
}