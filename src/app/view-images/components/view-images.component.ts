import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { PhotoViewer, PhotoViewerOptions, PaletteType, NYTPhotoItem } from "nativescript-photoviewer";
import { UserService } from "~/app/services/user.service";
import { Values } from "~/app/values/values";
import { Downloader, DownloadOptions } from 'nativescript-downloader';
import { Folder } from "tns-core-modules/file-system/file-system";
import * as permissions from "nativescript-permissions";
import { Progress } from "tns-core-modules/ui/progress";
import { ModalComponent } from "~/app/modals/modal.component";
import * as Toast from 'nativescript-toast';
import { Page } from 'tns-core-modules/ui/page/page';
import * as localstorage from "nativescript-localstorage";

declare const android: any;
declare const CGSizeMake: any;
const downloader = new Downloader();

@Component({
    selector: "app-viewImages",
    moduleId: module.id,
    templateUrl: "./view-images.component.html",
    styleUrls: ['./view-images.component.css'],
})

export class ViewImagesComponent implements OnInit {
    @ViewChild('downloadProgressDialog', { static: false }) downloadProgressDialog: ModalComponent;

    images;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    photoViewer: PhotoViewer;
    folderId: string;
    // downloader: Downloader;
    folder: Folder
    downloadProgressValue: number;
    profilePic: string;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    galleryMessage: string;
    isData: boolean;

    constructor(private router: Router, private route: ActivatedRoute, private userService: UserService, private http: HttpClient, private routerExtensions: RouterExtensions, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.images = [];
        this.year =  Values.SESSION;
        this.galleryMessage = "sdhg sdfjgds sdgd sdhgfvsdf hgsdfv dfgvdf hdfsdf ";
        this.isData = false;
        this.photoViewer = new PhotoViewer();
        this.route.queryParams.subscribe(params => {
            this.folderId = params["folderId"];
        });
        this.isLoading = false;
        this.token = "";
        this.userService.activeScreen("viewImages");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        this.getImages();
        this.folder = Folder.fromPath('/storage/emulated/0/bmsss-images')
        Toast.makeText("Long press on image to download image.", "long").show();
        this.profilePic = "res://profile";
        this.downloadProgressValue = 10;
    }

    ngOnInit(): void {
        Downloader.init();
        // Downloader.setTimeout(120)
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        // this.router.navigate(['/viewImagesFolder']);
        this.routerExtensions.back();
    }

    getImages() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "files?id=" + this.folderId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.isData = false;
                        this.images = [];
                        console.log("IMAGES::::", res);
                        if (res.data.length > 0) {
                            for (var i = 0; i < res.data.length; i++) {
                                this.images.push({
                                    id: res.data[i].id,
                                    icon: res.data[i].image_url
                                });
                            }
                        }
                        else {
                            this.isData = true;
                            this.galleryMessage = "There is no images.";
                        }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onImageClick(icon: string) {
        let photoviewerOptions: PhotoViewerOptions = {
            startIndex: 0,
            android: {
                paletteType: PaletteType.DarkVibrant,
                showAlbum: false
            }
        };
        let eventImage = [icon];
        this.photoViewer.showGallery(eventImage, photoviewerOptions);
    }

    onImagePress(icon: string) {
        // this.progressValue = 0;
        // permissions.requestPermission([android.Manifest.permission.WRITE_EXTERNAL_STORAGE])
        //     .then(() => {
        var that = this;

        var imageDownloaderId = downloader.createDownload({
            url: icon,
            path: that.folder.path,
        });

        downloader
            .start(imageDownloaderId, progressData => {
                console.log('Entwered2')
                that.downloadProgressDialog.show();
                that.downloadProgressValue = progressData.value;
                // this.userService.showLoadingState(true);
                console.log(`Progress : ${progressData.value}%`);
            })
            .then(completed => {
                that.downloadProgressDialog.hide();
                // that.userService.showLoadingState(false);
                Toast.makeText(`Image downloaded successfully at path : ${completed.path}`, "long").show();
            })
            .catch(error => {
                console.log(error.message);
            });
        // });
    }

    onOutsideClick() {
        this.downloadProgressDialog.hide();
    }
}