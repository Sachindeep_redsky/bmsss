import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ViewImagesComponent } from "./components/view-images.component";

const routes: Routes = [
    { path: "", component: ViewImagesComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ViewImagesRoutingModule { }
