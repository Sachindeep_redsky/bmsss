import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ModalComponent } from "~/app/modals/modal.component";
import { UserService } from "~/app/services/user.service";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { Classes } from "~/app/models/classes.model";
import { Page } from "tns-core-modules/ui/page/page";
import * as localstorage from "nativescript-localstorage";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-addClass",
    moduleId: module.id,
    templateUrl: "./add-class.component.html",
    styleUrls: ['./add-class.component.css'],
})

export class AddClassComponent implements OnInit {

    @ViewChild('addClassDialog', { static: false }) addClassDialog: ModalComponent;

    classList;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    classHint: string;
    // sectionHint: string;
    classBorderColor: string;
    // sectionBorderColor: string;
    classText: string;
    // sectionText: string;
    dialogAddButton: string;
    type: string;
    classId: number;
    classes: Classes;
    token: string;
    headers: HttpHeaders;
    isLoading: boolean;
    profilePic: string;
    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.classList = [];
        this.year = Values.SESSION;
        this.classHint = "Class name";
        // this.sectionHint = "Section name";
        this.classBorderColor = "black";
        // this.sectionBorderColor = "black";
        this.classText = "";
        // this.sectionText = "";
        this.dialogAddButton = "";
        this.classId = 0;
        this.classes = new Classes();
        this.isLoading = false;
        this.userService.activeScreen("addClass");
        this.profilePic = "res://profile";
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            this.getClasses();
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000);
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    getClasses() {
        this.http
            .get(Values.BASE_URL + "classes", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.classList = [];
                        console.trace(res);
                        for (var i = 0; i < res.data.length; i++) {
                            var name = res.data[i].name.charAt(0).toUpperCase() + res.data[i].name.slice(1);
                            this.classList.push({
                                id: res.data[i].id,
                                name: name,
                                status: res.data[i].status
                            });
                        }
                        this.userService.showLoadingState(false);
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    onBack() {
        // this.router.navigate(['/homeAdmin']);
        this.routerExtensions.back();
    }

    onOutsideClick() {
        this.addClassDialog.hide();
    }

    onClassTextChanged(args) {
        this.classText = args.object.text;
        this.classBorderColor = "white";
        if (this.classText == "") {
            this.classBorderColor = "black";
        }
    }

    // onSectionTextChanged(args) {
    //     this.sectionText = args.object.text;
    //     this.sectionBorderColor = "white";
    //     if (this.sectionText == "") {
    //         this.sectionBorderColor = "black";
    //     }
    // }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onAddClass() {
        this.addClassDialog.show();
        this.dialogAddButton = "Add";
        this.classText = "";
        // this.sectionText = "";
        this.classHint = "Class name";
        // this.sectionHint = "Section name";
        this.type = "";
    }

    onAdd() {
        if (this.classText == "") {
            alert("Class name cannot be empty");
        }
        // else if (this.sectionText == "") {
        //     alert("Section name cannot be empty");
        // }
        else {
            this.classes.name = this.classText;
            this.userService.showLoadingState(true);
            // this.classes.section = this.sectionText;
            if (this.type == "edit") {
                this.http
                    .put(Values.BASE_URL + "classes/update/" + this.classId, this.classes, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                console.log(res);
                                this.userService.showLoadingState(false);
                                Toast.makeText("Class updated successfully.", "long").show();
                                this.addClassDialog.hide();
                                this.getClasses();
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        console.log(error.error.error);
                    });
            }
            else {
                this.classText = "";
                // this.sectionText = "";
                this.http
                    .post(Values.BASE_URL + "classes/create", this.classes, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.userService.showLoadingState(false);
                                Toast.makeText("Class added successfully.", "long").show();
                                this.addClassDialog.hide();
                                this.getClasses();
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        console.log(error.error.error);
                    });
            }
        }
    }

    onEdit(classes: Classes) {
        this.classText = classes.name;
        // this.sectionText = "";
        // this.classHint = classes.name;
        // this.sectionHint = classes.section;
        this.addClassDialog.show();
        this.classId = classes.id;
        this.type = "edit";
        this.dialogAddButton = "Update";
    }

    onActiveInactiveButton(item: any) {
        this.isLoading = true;
        if (item.status == "active") {
            this.classes.status = "inactive";
        }
        else {
            this.classes.status = "active";
        }
        console.log("URL:::", Values.BASE_URL + "classes/update/" + item.id);
        console.log("BODY:::", this.classes);
        this.http
            .put(Values.BASE_URL + "classes/update/" + item.id, this.classes, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        Toast.makeText("Updated", "short").show();
                        this.isLoading = false;
                        this.getClasses();
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    // onDelete(id: any) {
    //     this.isLoading = true;
    //     this.http
    //         .delete(Values.BASE_URL + "classes/delete/" + id, {
    //             headers: this.headers
    //         })
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     Toast.makeText("Deleted", "short").show();
    //                     this.isLoading = false;
    //                     this.getClasses();
    //                 }
    //             }
    //         }, error => {
    //             this.isLoading = false;
    //             console.log(error.error.error);
    //         });
    // }
}