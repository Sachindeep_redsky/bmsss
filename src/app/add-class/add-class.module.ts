import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { GridViewModule } from 'nativescript-grid-view/angular';
import { AddClassComponent } from "./components/add-class.component";
import { AddClassRoutingModule } from "./add-class-routing.module";
// import { NgShadowModule } from "nativescript-ng-shadow";
import { NgModalModule } from "../modals/ng-modal";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
    imports: [
        HttpModule,
        GridViewModule,
        // NgShadowModule,
        NgModalModule,
        AddClassRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        AddClassComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AddClassModule { }
