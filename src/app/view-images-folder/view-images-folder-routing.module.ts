import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ViewImagesFolderComponent } from "./components/view-images-folder.component";

const routes: Routes = [
    { path: "", component: ViewImagesFolderComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ViewImagesFolderRoutingModule { }
