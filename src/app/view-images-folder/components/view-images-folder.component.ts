import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { Values } from "~/app/values/values";
import * as localstorage from "nativescript-localstorage";
import { Page } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-viewImagesFolder",
    moduleId: module.id,
    templateUrl: "./view-images-folder.component.html",
    styleUrls: ['./view-images-folder.component.css'],
})

export class ViewImagesFolderComponent implements OnInit {


    folders;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    profilePic: string;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    galleryMessage: string;
    isData: boolean;

    constructor(private router: Router, private userService: UserService, private http: HttpClient, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.folders = [];
        this.year = Values.SESSION;
        this.profilePic = "res://profile";
        this.isLoading = false;
        this.galleryMessage = "";
        this.isData = false;
        this.userService.activeScreen("viewImagesFolder");

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        this.getFolders();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        // if (localstorage.getItem("userType") == "student") {
        //     this.router.navigate(['/homeStudent']);
        // }
        // else {
        //     this.router.navigate(['/homeAdmin']);
        // }
        this.routerExtensions.back();
    }

    getFolders() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "events", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.folders = [];
                        if (res.data.length > 0) {
                            this.isData = false;
                            for (var i = 0; i < res.data.length; i++) {
                                this.folders.push({
                                    id: res.data[i].id,
                                    name: res.data[i].name,
                                    icon: res.data[i].image_url
                                });
                            }
                        }
                        else {
                            this.isData = true;
                            this.galleryMessage = "There is no images.";
                        }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onFolderClick(id: number) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "folderId": id
            }
        }
        this.router.navigate(['/viewImages'], navigationExtras);
    }
}