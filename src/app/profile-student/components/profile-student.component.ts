import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from "~/app/services/user.service";
import { Page } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-profileStudent",
    moduleId: module.id,
    templateUrl: "./profile-student.component.html",
    styleUrls: ['./profile-student.component.css'],
})

export class ProfileStudentComponent implements OnInit {


    renderViewTimeout;
    isRendering: boolean;
    username: string;
    admissionNumber: string;
    DOB: string;
    class: string;
    section: string;
    fatherName: string;
    motherName: string;
    contactNumber: string;
    emailId: string;
    address: string;
    profilePic: string;
    isLoading: boolean;
    token: string;

    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.username = ""
        this.admissionNumber = "";
        this.DOB = "";
        this.class = "";
        this.section = "";
        this.fatherName = "";
        this.motherName = "";
        this.contactNumber = "";
        this.emailId = "";
        this.isLoading = false;
        this.token = "";
        this.address = "3rd crossing, Street no. 7, Old suraj nagari, Abohar, Punjab (152116)"
        this.userService.activeScreen("profileStudent");

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            let headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            this.isLoading = true;
            this.http
                .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"), {
                    headers: headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.username = res.data.name;
                            this.admissionNumber = res.data.admissionNo;
                            this.DOB = res.data.dateOfBirth;
                            this.class = res.data.className;
                            this.section = res.data.classSection;
                            this.fatherName = res.data.fatherName;
                            this.motherName = res.data.motherName;
                            this.contactNumber = res.data.residenceMobile;
                            this.emailId = res.data.email;
                            this.address = res.data.residenceAddress1;
                            this.isLoading = false;
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        // this.setCardData();
        // this.profilePic = "res://profilepic";
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onChangePassword() {
        this.routerExtensions.navigate(['/changePassword']);
    }

    onLogout() {
        localStorage.removeItem("userId");
        localstorage.removeItem("profilePic");
        localstorage.removeItem("userType");
        localstorage.removeItem("userName");
        this.routerExtensions.navigate(['/login']);
    }

    onBack() {
        this.routerExtensions.navigate(['/homeStudent']);
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onGridTopLoaded(args: any) {
        var gridTop = <any>args.object;

        setTimeout(() => {
            if (gridTop.android) {
                let nativeGridMain = gridTop.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#A61F21'));
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridTop.ios) {
                let nativeGridMain = gridTop.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }

            // this.changeDetector.detectChanges();
        }, 400)

    }

    onGridMiddleLoaded(args: any) {
        var gridMiddle = <any>args.object;

        setTimeout(() => {
            if (gridMiddle.android) {
                let nativeGridMain = gridMiddle.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                shape.setCornerRadius(10)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridMiddle.ios) {
                let nativeGridMain = gridMiddle.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }

            // this.changeDetector.detectChanges();
        }, 400)

    }

    onGridBottomLoaded(args: any) {
        var gridBottom = <any>args.object;

        setTimeout(() => {
            if (gridBottom.android) {
                let nativeGridMain = gridBottom.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                shape.setCornerRadius(10)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridBottom.ios) {
                let nativeGridMain = gridBottom.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }

            // this.changeDetector.detectChanges();
        }, 400)

    }
}