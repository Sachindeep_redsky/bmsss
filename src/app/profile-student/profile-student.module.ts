import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ProfileStudentComponent } from "./components/profile-student.component";
import { ProfileStudentRoutingModule } from "./profile-student-routing.module";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from 'nativescript-ng-shadow';


@NgModule({
    imports: [
        HttpModule,
        ProfileStudentRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        ProfileStudentComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class ProfileStudentModule { }
