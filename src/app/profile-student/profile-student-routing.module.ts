import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ProfileStudentComponent } from "./components/profile-student.component";

const routes: Routes = [
    { path: "", component: ProfileStudentComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ProfileStudentRoutingModule { }
