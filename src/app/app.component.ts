import { RouterExtensions } from 'nativescript-angular/router';
import { Component, NgZone, ViewChild, OnInit, AfterViewInit, OnChanges, ElementRef } from "@angular/core";
import { registerElement } from 'nativescript-angular/element-registry';
import { Router } from "@angular/router";
// import { CardView } from '@nstudio/nativescript-cardview';
import { UserService } from "./services/user.service";
import { requestPermissions } from "nativescript-permissions";
import { Folder, File } from "tns-core-modules/file-system/file-system";
import * as application from "tns-core-modules/application";
import * as Toast from 'nativescript-toast';
import { exit } from 'nativescript-exit';

// registerElement('CardView', () => CardView);
declare var android: any;

@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html",
})

export class AppComponent {

    isVisibleCompose: boolean;
    showLoading: boolean;
    file: File;
    folder: Folder;
    tries: number;

    constructor(private userService: UserService, private routerExtensions: RouterExtensions, private ngZone: NgZone) {
        this.isVisibleCompose = false;
        this.showLoading = false;
        this.userService.showmessageCompose.subscribe((state: boolean) => {
            if (state != undefined) {
                this.isVisibleCompose = state;
            }
        });

        this.userService.showloadingState.subscribe((state: boolean) => {
            if (state != undefined) {
                this.showLoading = state;
            }
        });

        this.ngZone.run(() => {
            this.tries = 0;
            application.android.on(application.AndroidApplication.activityBackPressedEvent, (data: application.AndroidActivityBackPressedEventData) => {
                var screen = this.userService.currentPage
                console.log("SCREEN::::", screen);
                if (screen == "homeStudent" || screen == "homeAdmin" || screen == "homeTeacher") {
                    data.cancel = (this.tries++ > 1) ? false : true;
                    if (this.tries == 1) {
                        Toast.makeText("Press again to exit", "short").show();
                    }
                    if (this.tries == 2) {
                        exit();
                    }
                    setTimeout(() => {
                        this.tries = 0;
                    }, 1000);
                } else if (screen == 'login') {
                    exit();
                }
                else {
                    data.cancel = true;
                    this.routerExtensions.back();
                }
            });
        });


        var permissions: Array<any> = new Array<any>();
        permissions.push(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.push(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        requestPermissions(permissions, 'Application needs location access for its functioning.').then(() => {
            console.log('Permission Granted');
            this.folder = Folder.fromPath('/storage/emulated/0/bmsss')
            Folder.fromPath('/storage/emulated/0/bmsss-images');
            this.file = this.folder.getFile('bmsss.jpg')
        })
    }

    // onMessageCompose() {
    //     this.router.navigate(['/messageCompose']);
    // }
}

