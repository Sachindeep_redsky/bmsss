import { Fees } from './../../models/fees.model';
import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { Leave } from "~/app/models/leave.model";
import * as localstorage from "nativescript-localstorage";
import { Page } from "tns-core-modules/ui/page/page";
import * as Toast from 'nativescript-toast';

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-addFees",
    moduleId: module.id,
    templateUrl: "./add-fees.component.html",
    styleUrls: ['./add-fees.component.css'],
})

export class AddFeesComponent implements OnInit {


    renderViewTimeout;
    isRendering: boolean;
    year: string;
    admissionNoHint: string;
    admissionNoBorderColor: string;
    admissionNo: string;
    feeChargesHint: string;
    feeChargesBorderColor: string;
    feeCharges;
    leave: Leave;
    // userId: string;
    profilePic: string;
    isLoading: boolean;
    token: any;
    headers: HttpHeaders;
    feeChargesList;
    addFeesButton: string;
    fees: Fees;
    from: string;
    isEdit: boolean;

    constructor(private route: ActivatedRoute, private userService: UserService, private http: HttpClient, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.admissionNoHint = "Admission number";
        this.admissionNoBorderColor = "black";
        this.admissionNo = "";
        this.feeChargesHint = "0";
        this.feeChargesBorderColor = "black";
        this.feeCharges = [];
        this.leave = new Leave();
        // this.userId = "";
        this.profilePic = "res://profile";
        this.isLoading = false;
        this.feeChargesList = [];
        this.addFeesButton = "Add";
        this.fees = new Fees();
        this.from = "";
        this.isEdit = false;
        this.userService.activeScreen("addFees");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        this.route.queryParams.subscribe(params => {
            this.admissionNo = params["admissionNo"];
            this.from = params["from"];
        });
        // if (localstorage.getItem("userId") != null && localstorage.getItem("userId") != undefined) {
        //     this.userId = localstorage.getItem("userId");
        // }
        this.getFeeCharges();
        // this.getConfigId();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        this.routerExtensions.back();
    }

    // getConfigId() {
    //     this.isLoading = true;
    //     this.http
    //         .get(Values.BASE_URL + "configs", {
    //             headers: this.headers
    //         })
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     localstorage.setItem("configId", res.data[0].id);
    //                     this.isLoading = false;
    //                 }
    //             }
    //         }, error => {
    //             this.isLoading = false;
    //             console.log(error.error.error);
    //         });
    // }

    getFeeCharges() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "feeCharges", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        for (var i = 0; i < res.data.length; i++) {
                            this.feeChargesList.push({
                                sno: i,
                                id: res.data[i].id,
                                headId: res.data[i].headId,
                                headName: res.data[i].headName,
                                dueAmount: "0",
                                type: "add",
                                feesId: ""
                            });
                        }
                        if (this.from == "edit") {
                            this.isEdit = true;
                            this.getFees();
                        }
                        // this.getFees();
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    getFees() {
        // if (this.admissionNo == "") {
        //     alert("Please select admission number first.");
        // }
        // else {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "fees?admissionNo=" + this.admissionNo, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.trace(res);
                        if (res.data.fee.length > 0) {
                            for (var i = 0; i < res.data.fee.length; i++) {
                                for (var j = 0; j < this.feeChargesList.length; j++) {
                                    // console.log(this.feeChargesList[j].headId);
                                    // console.log(res.data.fee[i].feeChargesId);
                                    if (this.feeChargesList[j].headId == res.data.fee[i].feeChargesId) {
                                        // this.feeChargesList.splice(j, 1);
                                        this.feeChargesList[j].dueAmount = res.data.fee[i].dueAmount;
                                        this.feeChargesList[j].type = "update";
                                        this.feeChargesList[j].feesId = res.data.fee[i].id;
                                        // this.feeChargesList.push({
                                        //     sno: i,
                                        //     id: res.data[i].id,
                                        //     headId: res.data[i].feeChargesId,
                                        //     headName: res.data[i].feeChargesName,
                                        //     dueAmount: res.data[i].dueAmount
                                        // });
                                    }
                                }
                            }
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
        // }
    }

    onAdmissionNoTextChanged(args) {
        this.admissionNo = args.object.text;
        this.admissionNoBorderColor = "black";
        if (this.admissionNo == "") {
            this.admissionNoBorderColor = "black";
        }
    }

    onfeeChargesTextChanged(args, sno: number) {
        this.feeCharges[sno] = args.object.text;
        this.feeChargesBorderColor = "black";
        if (this.feeCharges == "") {
            this.feeChargesBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onAddFees(item: any) {
        console.log()
        if (this.admissionNo == "") {
            alert("Please enter admission number.");
        }
        else if (this.feeCharges[item.sno] == undefined || this.feeCharges[item.sno] == "") {
            alert("Please enter fee charges.");
        }
        else {
            this.isLoading = true;
            this.fees.admissionNo = this.admissionNo;
            this.fees.feeChargesId = this.feeChargesList[item.sno].headId;
            this.fees.dueAmount = this.feeCharges[item.sno];
            // console.log(this.fees);

            if (item.type == "add") {
                this.http
                    .post(Values.BASE_URL + "fees", this.fees, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                // this.feeChargesList = [];
                                this.isLoading = false;
                                Toast.makeText("Added", "long").show();
                                // this.getFeeCharges();
                            }
                        }
                    }, error => {
                        this.isLoading = false;
                        if (error.error.error == "EXIST") {
                            alert("Already added, you can update the fee.");
                        }
                        console.log(error.error.error);
                    });
            }
            else {
                console.log(Values.BASE_URL + "fees/" + item.feesId);
                console.log(this.fees);
                this.http
                    .put(Values.BASE_URL + "fees/" + item.feesId, this.fees, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.feeChargesList = [];
                                this.isLoading = false;
                                Toast.makeText("Updated", "long").show();
                                this.getFeeCharges();
                            }
                        }
                    }, error => {
                        this.isLoading = false;
                        console.log("ERROR::::", error.error.error);
                    });
            }
        }
    }
}