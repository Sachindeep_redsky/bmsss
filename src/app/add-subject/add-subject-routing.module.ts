import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AddSubjectComponent } from "./components/add-subject.component";

const routes: Routes = [
    { path: "", component: AddSubjectComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AddSubjectRoutingModule { }
