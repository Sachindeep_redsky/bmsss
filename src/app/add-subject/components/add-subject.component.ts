import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ModalComponent } from "~/app/modals/modal.component";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { UserService } from "~/app/services/user.service";
import { Subject } from "~/app/models/subject.model";
import { Classes } from "~/app/models/classes.model";
import { Page } from "tns-core-modules/ui/page/page";
import * as localstorage from "nativescript-localstorage";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-addSubject",
    moduleId: module.id,
    templateUrl: "./add-subject.component.html",
    styleUrls: ['./add-subject.component.css'],
})

export class AddSubjectComponent implements OnInit {

    @ViewChild('addSubjectDialog', { static: false }) addSubjectDialog: ModalComponent;
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;

    subjects;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    subjectHint: string;
    subjectBorderColor: string;
    subjectText: string;
    subject: Subject;
    subjectId: number;
    selectClassText: string;
    classes;
    classId: string;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    subjectMessage: string;
    isData: boolean;
    dialogAddButton: string;
    type: string;
    profilePic: string;

    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.subjects = [];
        this.year =  Values.SESSION;
        this.subjectHint = "Subject name";
        this.subjectBorderColor = "black";
        this.subjectText = "";
        this.subject = new Subject();
        this.subjectId = 0;
        this.selectClassText = "Select Class";
        this.classes = [];
        this.classId = "";
        this.isLoading = false;
        this.token = "";
        this.subjectMessage = "Select class to show the subject list.";
        this.isData = true;
        this.dialogAddButton = "";
        this.type = "";
        this.userService.activeScreen("addSubject");
        this.profilePic = "";
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)

        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        // this.router.navigate(['/homeAdmin']);
        this.routerExtensions.back();
    }

    onSubjectOutsideClick() {
        this.addSubjectDialog.hide();
    }

    onClassOutsideClick() {
        this.selectClassDialog.hide();
    }

    onSubjectTextChanged(args) {
        this.subjectText = args.object.text;
        // this.subjectBorderColor = "white";
        if (this.subjectText == "") {
            // this.subjectBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onSubjectDialogLoaded(args) {
        var subjectDialog = <any>args.object;
        setTimeout(() => {
            if (subjectDialog.android) {
                let nativeImageView = subjectDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#A61F21'));
                nativeImageView.setElevation(10)
            } else if (subjectDialog.ios) {
                let nativeImageView = subjectDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#A61F21'));
                nativeImageView.setElevation(10)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onAddSubject() {
        this.subjectText = "";
        this.subjectHint = "Subject name";
        this.dialogAddButton = "Add";
        this.type = "";
        this.addSubjectDialog.show();
    }

    getSubjects() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "subjects?classId=" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.subjects = [];
                        if (res.data.length > 0) {
                            this.isData = false;
                            for (var i = 0; i < res.data.length; i++) {
                                var name = res.data[i].name.charAt(0).toUpperCase() + res.data[i].name.slice(1);
                                this.subjects.push({
                                    id: res.data[i].id,
                                    name: name,
                                    status: res.data[i].status
                                });
                            }
                        }
                        else {
                            this.isData = true;
                            this.subjectMessage = "There is no subjects added for this class.";
                        }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onAdd() {
        if (this.classId == "") {
            alert("Please select class first");
        }
        else if (this.subjectText == "") {
            alert("Subject name cannot be empty");
        }
        else {
            this.subject.name = this.subjectText;
            this.subject.classId = this.classId;
            if (this.type == "edit") {
                this.http
                    .put(Values.BASE_URL + "subjects/update/" + this.subjectId, this.subject, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                console.log(res);
                                this.userService.showLoadingState(false);
                                Toast.makeText("Subject updated successfully.", "long").show();
                                this.addSubjectDialog.hide();
                                this.getSubjects();
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        console.log(error.error.error);
                    });
            }
            else {
                this.subjectText = "";
                console.log(this.subject);
                this.http
                    .post(Values.BASE_URL + "subjects/create", this.subject, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                console.log("SUBJECT:::", res);
                                this.userService.showLoadingState(false);
                                Toast.makeText("Subject added successfully.", "long").show();
                                this.addSubjectDialog.hide();
                                this.getSubjects();
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        console.log(error.error.error);
                    });
            }
        }
    }

    onSelectClass() {
        this.getClasses();
        this.selectClassDialog.show();
    }

    getClasses() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "classes", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.classes = [];
                        for (var i = 0; i < res.data.length; i++) {

                            this.classes.push({
                                id: res.data[i].id,
                                name: res.data[i].name
                            });
                        }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onClassButton(item: Classes) {
        this.selectClassText = item.name;
        this.classId = item.id.toString();
        this.getSubjects();
    }

    // onDelete(id: any) {
    //     this.isLoading = true;
    //     this.http
    //         .delete(Values.BASE_URL + "subjects/delete/" + id, {
    //             headers: this.headers
    //         })
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     Toast.makeText("Deleted", "short").show();
    //                     this.isLoading = false;
    //                     this.getSubjects();
    //                 }
    //             }
    //         }, error => {
    //             this.isLoading = false;
    //             console.log(error.error.error);
    //         });
    // }

    onActiveInactiveButton(item: any) {
        this.isLoading = true;
        if (item.status == "active") {
            this.subject.status = "inactive";
        }
        else {
            this.subject.status = "active";
        }
        this.http
            .put(Values.BASE_URL + "subjects/update/" + item.id, this.subject, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        Toast.makeText("Updated", "short").show();
                        this.isLoading = false;
                        this.getSubjects();
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onEdit(item: any) {
        this.subjectText = item.name;
        this.subjectId = item.id;
        this.type = "edit";
        this.dialogAddButton = "Update";
        this.addSubjectDialog.show();
    }
}