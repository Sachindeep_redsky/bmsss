import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Mediafilepicker, FilePickerOptions } from 'nativescript-mediafilepicker';
import { knownFolders, Folder, File } from "tns-core-modules/file-system";
import { UserService } from "~/app/services/user.service";
import { session, Request } from 'nativescript-background-http';
import { Values } from "~/app/values/values";
import * as localstorage from "nativescript-localstorage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Page, booleanConverter } from "tns-core-modules/ui/page/page";
import { Syllabus } from "~/app/models/syllabus.model";
import { ModalComponent } from "~/app/modals/modal.component";
import { Classes } from '~/app/models/classes.model';
import { Downloader, DownloadOptions } from 'nativescript-downloader';
import * as Toast from 'nativescript-toast';

declare const android: any;
declare const CGSizeMake: any;
const downloader = new Downloader();

@Component({
    selector: "app-uploadSyllabus",
    moduleId: module.id,
    templateUrl: "./upload-syllabus.component.html",
    styleUrls: ['./upload-syllabus.component.css'],
})

export class UploadSyllabusComponent implements OnInit {

    @ViewChild('uploadProgressDialog', { static: false }) uploadProgressDialog: ModalComponent;
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;
    @ViewChild('downloadProgressDialog', { static: false }) downloadProgressDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    year: string;
    extensions;
    filepicker: FilePickerOptions;
    file: any;
    classId: string;
    uploadSyllabusButton: string;
    isUpdate: boolean;
    syllabusId: string;
    isLoading: boolean;
    profilePic: string;
    token: string;
    fileUrl: string;
    syllabus: Syllabus;
    headers: HttpHeaders;
    uploadProgressValue: number;
    selectClassText: string;
    classes;
    syllabusName: string;
    isVisibleDownload: boolean;
    isVisibleUpload: boolean;
    downloadProgressValue: number;
    folder: Folder;
    isData: boolean;
    syllabusMessage: string;
    classTeacher;
    pageNo: number;
    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.extensions = ['txt', 'pdf'];
        this.classId = "";
        this.file = "";
        this.uploadSyllabusButton = ""
        this.isUpdate = false;
        this.isLoading = false;
        this.profilePic = "res://profile";
        this.token = "";
        this.fileUrl = "";
        this.syllabus = new Syllabus();
        this.uploadProgressValue = 10;
        this.selectClassText = "Select Class"
        this.classes = [];
        this.syllabusName = "";
        this.isVisibleDownload = false;
        this.isVisibleUpload = false;
        this.downloadProgressValue = 10;
        this.isData = true;
        this.classTeacher = [];
        this.pageNo = 1;
        this.syllabusMessage = "Select class to show the syllabus.";
        // this.folder = Folder.fromPath('/storage/emulated/0/bmsss-syllabus');
        this.userService.activeScreen("uploadSyllabus");

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            // this.getClass();
        }
        // if (localstorage.getItem("userId") != null && localstorage.getItem("userId") != undefined) {
        // }
    }

    ngOnInit(): void {
        Downloader.init();
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onClassOutsideClick() {
        this.selectClassDialog.hide();
    }

    onSelectClass() {
        this.getClasses();
        this.selectClassDialog.show();
    }

    getClasses() {
        this.isLoading = true;
        if (localstorage.getItem("userType") == "teacher") {
            this.http
                .get(Values.BASE_URL + `users/search?pageNo=${this.pageNo}&userType=teacher&regNo=${localstorage.getItem("regNo")}`, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("RESSSS", res);
                            if (res.data.users[0].classTeacher.length > 0) {
                                this.classTeacher = res.data.users[0].classTeacher;
                                if (res.data.users[0].classIncharge == "true") {
                                    this.classTeacher.push({
                                        "id": res.data.users[0].classId,
                                        "name": res.data.users[0].className
                                    })
                                }
                                console.trace("CLASS TEACHER", this.classTeacher);
                                this.classes = [];
                                for (var i = 0; i < this.classTeacher.length; i++) {
                                    this.classes.push({
                                        id: this.classTeacher[i].id,
                                        name: this.classTeacher[i].name
                                    });
                                }
                                this.selectClassDialog.show();
                                this.isLoading = false;
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
        else {
            this.http
                .get(Values.BASE_URL + "classes", {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.classes = [];
                            for (var i = 0; i < res.data.length; i++) {
                                this.classes.push({
                                    id: res.data[i].id,
                                    name: res.data[i].name
                                });
                            }
                            this.isLoading = false;
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    onClassButton(item: Classes) {
        this.selectClassText = item.name;
        this.classId = item.id.toString();
        if (localstorage.getItem("userType") == "teacher") {
            this.isVisibleUpload = false;
        }
        else {
            this.isVisibleUpload = true;
        }
        this.getSyllabus();
    }

    getSyllabus() {
        this.isLoading = true;
        console.log(Values.BASE_URL + "syllabus?classId=" + this.classId);
        this.http
            .get(Values.BASE_URL + "syllabus?classId=" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        if (res.data != null && res.data != undefined) {
                            this.isData = false;
                            this.syllabusName = res.data.file_url;
                            var index = this.syllabusName.lastIndexOf('/');
                            this.syllabusName = this.syllabusName.substr(index + 1, this.syllabusName.length);
                            console.log("syllabus:::", this.syllabusName);
                            this.isVisibleDownload = true;
                            this.uploadSyllabusButton = "Update Syllabus";
                        }
                        else {
                            this.syllabusName = "No file is uploaded.";
                            this.isVisibleDownload = false;
                            this.uploadSyllabusButton = "Upload Syllabus";
                        }
                        // for (var i = 0; i < res.data.length; i++) {
                        //     this.syllabuses.push({
                        //         id: res.data[i].id,
                        //         name: res.data[i].name,
                        //         status: res.data[i].status
                        //     });
                        // }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onBack() {
        this.routerExtensions.back();
        // this.router.navigate(['/homeTeacher']);
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onGridMainLoaded(args: any) {
        var gridMain = <any>args.object;

        setTimeout(() => {
            if (gridMain.android) {
                let nativeGridMain = gridMain.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridMain.ios) {
                let nativeGridMain = gridMain.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }

            // this.changeDetector.detectChanges();
        }, 400)

    }

    onUpload() {
        var that = this;
        let options: FilePickerOptions = {
            android: {
                extensions: this.extensions,
                maxNumberFiles: 1
            }
        };
        let mediafilepicker = new Mediafilepicker();
        mediafilepicker.openFilePicker(options);
        mediafilepicker.on("getFiles", function (res) {
            let results = res.object.get('results');
            const file: File = File.fromPath(results[0].file);
            that.file = file.path;

            if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
                that.token = localstorage.getItem("token");
            }
            var uploadSession = session('image-upload');

            var request = {
                url: Values.BASE_URL + "documents",
                method: "POST",
                headers: {
                    "Content-Type": "application/octet-stream",
                    "File-Name": "my file",
                    "x-access-token": that.token
                },
                description: "{'uploading':" + "my file" + "}"
            }

            const params = [
                { name: "file", filename: that.file, mimeType: "application/pdf" },
            ]

            var task = uploadSession.multipartUpload(params, request);

            task.on("progress", (e) => {
                that.uploadProgressValue = (e.currentBytes / e.totalBytes) * 100;
                that.uploadProgressDialog.show();
            });

            task.on("responded", (e: any) => {
                console.log(JSON.parse(e.data).data.file_url);
                that.fileUrl = JSON.parse(e.data).data.file_url;
                that.uploadProgressDialog.hide();
                that.uploadSyllabus();
                this.isLoading = true;
            });

            // task.on("complete", (e: any) => {
            // console.log(that.classId);
            // alert("fileurl::::" + that.fileUrl);
            // });

            task.on("error", (e) => {
                console.log("ERROR::::", e);
                alert("May be your network connection is low.");
            });
        });

        mediafilepicker.on("error", function (res) {
            let msg = res.object.get('msg');
            console.log(msg);
        });

        mediafilepicker.on("cancel", function (res) {
            let msg = res.object.get('msg');
            console.log(msg);
        });


    }

    uploadSyllabus() {
        if (this.fileUrl != null && this.fileUrl != undefined && this.classId != null && this.classId != undefined) {
            this.syllabus.classId = this.classId.toString();
            this.syllabus.file_url = this.fileUrl;
            this.isLoading = true;
            this.http
                .put(Values.BASE_URL + "syllabus/updateOrCreate", this.syllabus, {
                    headers: {
                        "Content-Type": "application/json",
                        "x-access-token": this.token
                    }
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            Toast.makeText("Syllabus is successfully uploaded!!!", "long").show();
                            this.getSyllabus();
                            this.isLoading = false;
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log("ERROR::::", error.error.error);
                });
        }
    }

    downloadSyllabus() {
        var that = this;
        this.http
            .get(Values.BASE_URL + "syllabus?classId=" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true && res.data != null) {
                        console.log(res);
                        var syllabusUrl = res.data.file_url;
                        console.log(syllabusUrl);
                        // downloader = new Downloader();
                        console.log(that.folder.path);
                        var syllabusDownloaderId = downloader.createDownload({
                            url: syllabusUrl,
                            path: that.folder.path,
                        });

                        downloader
                            .start(syllabusDownloaderId, progressData => {
                                that.downloadProgressDialog.show();
                                that.downloadProgressValue = progressData.value;
                                // this.progressValue = progressData.value;
                                // this.userService.showLoadingState(true);
                                console.log(`Progress : ${progressData.value}%`);
                            })
                            .then(completed => {
                                that.downloadProgressDialog.hide();
                                this.userService.showLoadingState(false);
                                Toast.makeText(`Syllabus downloaded successfully at path : ${completed.path}`, "long").show();
                            })
                            .catch(error => {
                                console.log(error.message);
                            });
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

}