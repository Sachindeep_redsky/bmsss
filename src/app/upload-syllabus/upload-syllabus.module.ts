import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { UploadSyllabusComponent } from "./components/upload-syllabus.component";
import { UploadSyllabusRoutingModule } from "./upload-syllabus-routing.module";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from 'nativescript-ng-shadow';
import { NgModalModule } from "../modals/ng-modal";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
    imports: [
        HttpModule,
        UploadSyllabusRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NgModalModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        UploadSyllabusComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class UploadSyllabusModule { }
