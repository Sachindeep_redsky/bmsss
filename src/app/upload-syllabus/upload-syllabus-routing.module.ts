import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { UploadSyllabusComponent } from "./components/upload-syllabus.component";

const routes: Routes = [
    { path: "", component: UploadSyllabusComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class UploadSyllabusRoutingModule { }
