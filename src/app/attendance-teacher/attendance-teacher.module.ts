import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { AttendanceTeacherRoutingModule } from "./attendance-teacher-routing.module";
import { AttendanceTeacherComponent } from "./components/attendance-teacher.component";
import { RadioButtonModule } from '../shared/radio-button/radio-button.module'

@NgModule({
    imports: [
        HttpModule,
        AttendanceTeacherRoutingModule,
        RadioButtonModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        AttendanceTeacherComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AttendanceTeacherModule { }