import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AttendanceTeacherComponent } from "./components/attendance-teacher.component";



const routes: Routes = [
    { path: "", component:  AttendanceTeacherComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AttendanceTeacherRoutingModule { }