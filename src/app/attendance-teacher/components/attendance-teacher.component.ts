import { UserService } from './../../services/user.service';
import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Color } from "tns-core-modules/color/color";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { Values } from "~/app/values/values";
import * as localstorage from "nativescript-localstorage";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-attendanceteacher",
    moduleId: module.id,
    templateUrl: "./attendance-teacher.component.html",
    styleUrls: ['./attendance-teacher.component.css'],
})

export class AttendanceTeacherComponent {

    students;
    renderViewTimeout;
    isRendering: boolean;
    date: string;

    backgroundColor: string;
    borderRadius: string;
    borderWidth: string;
    borderColor: string;

    headers;
    query;
    updateQuery;
    pageNo: number;
    count: number;
    isHoliday: boolean;
    attendanceMessage: string;
    isLoading: boolean;
    profilePic: string;
    constructor(private router: Router, private http: HttpClient, private page: Page, private userService: UserService) {
        this.page.actionBarHidden = true;
        var date = new Date();
        this.isRendering = true;
        this.students = [];
        this.date = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
        this.isLoading = false;
        let dateString = '';
        this.pageNo = 1;
        this.count = 10;
        this.isHoliday = false;
        this.attendanceMessage = "";
        this.profilePic = "res://profile";
        this.userService.activeScreen("attendanceTeacher");

        if (date.getDate() < 10) {
            dateString = dateString + '0' + date.getDate() + '/';
        } else {
            dateString = dateString + date.getDate() + '/';
        }

        if ((date.getMonth() + 1) < 10) {
            dateString = dateString + '0' + (date.getMonth() + 1) + '/';
        } else {
            dateString = dateString + (date.getMonth() + 1) + '/';
        }

        dateString = dateString + date.getFullYear();

        this.headers = {
            'Content-Type': 'application/json',
            'x-access-token': localStorage.getItem('token')
        }

        this.query = `classId=${localStorage.getItem('classId')}&section=${localStorage.getItem('classSection')}&date=${dateString}`
        this.updateQuery = `classId=${localStorage.getItem('classId')}&section=${localStorage.getItem('classSection')}&date=${dateString}`

        if (date.getDate() < 10) {
            if ((date.getMonth() + 1) < 10) {
                this.date = '0' + date.getDate() + '-0' + (date.getMonth() + 1) + '-' + date.getFullYear();
            }
            else {
                this.date = '0' + date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
            }
        }
        else {
            if ((date.getMonth() + 1) < 10) {
                this.date = date.getDate() + '-0' + (date.getMonth() + 1) + '-' + date.getFullYear();
            }
            else {
                this.date = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
            }
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
        this.students = [];
        this.getAttendance();
    }

    getAttendance() {
        // this.isLoading = true;
        this.http.get(Values.BASE_URL + `attendanceRecords/search?pageNo=${this.pageNo}&count=${this.count}&${this.query}`, { headers: this.headers }).subscribe((res: any) => {
            console.log('RES::ATTT:::TECH:::', res)
            if (res.isSuccess) {
                if (res.data.attendance.length > 0) {
                    if (res.data.attendance[0].status == "holiday") {
                        this.isHoliday = true;
                        this.attendanceMessage = "Today is holiday.";
                    }
                    for (var i = 0; i < res.data.attendance.length; i++) {
                        this.students.push({
                            "id": res.data.attendance[i].id,
                            "userId": res.data.attendance[i].userId,
                            "classId": res.data.attendance[i].classId,
                            "className": res.data.attendance[i].className,
                            "section": res.data.attendance[i].section,
                            "name": res.data.attendance[i].name,
                            "rollNo": res.data.attendance[i].rollNo,
                            "status": res.data.attendance[i].status,
                            "studentWorkingDays": res.data.attendance[i].studentWorkingDays,
                            "schoolWorkingDays": res.data.attendance[i].schoolWorkingDays,
                            "percentageCount": res.data.attendance[i].percentageCount
                        })
                    }
                    // this.students = res.data.attendance;
                    this.pageNo = this.pageNo + 1;
                    this.getAttendance();
                }
                // this.isLoading = false;
            }
        }, error => {
            this.isLoading = false;
            console.log('Error:::Attendance:::Teacher:::', error)
        })
    }


    onBack() {
        this.router.navigate(['/homeTeacher']);
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    public whichOneChanged(args) {
        console.log("ARGS:::", args)
    }

    public setStudents() {
        this.students.push({
            'name': 'Rammi',
            'isAbsent': false,
            'onLeave': false
        });
        this.students.push({
            'name': 'Shammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Tammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Dammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Gammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Sammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Fammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Ammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Bammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Cammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Eammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Chammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Gammi',
            'isAbsent': false,
            'onLeave': false
        })
        this.students.push({
            'name': 'Hammi',
            'isAbsent': false,
            'onLeave': false
        })
    }

    onLeftClick(index: number) {
        if (this.students[index].status == 'present') {
            this.students[index].status = 'absent';
        } else {
            this.students[index].status = 'present';
            // this.students[index].status = '';
        }
    }

    onRightClick(index: number) {
        if (this.students[index].status == 'leave') {
            this.students[index].status = 'absent';
        } else {
            this.students[index].status = 'leave';
            // this.students[index].status = '';
        }
    }

    onSubmitClick() {

        let body = { 'attendance': this.students };

        this.http.put(Values.BASE_URL + `attendanceRecords/update?${this.updateQuery}`, body, { headers: this.headers }).subscribe((res: any) => {
            if (res.isSuccess) {
                this.router.navigate(['/homeTeacher'])
            }
        }, error => {
            console.log('Error:::Attendance:::Teacher:::', error)
        })
    }

}