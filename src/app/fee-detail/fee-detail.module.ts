import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { FeeDetailComponent } from "./components/fee-detail.component";
import { FeeDetailRoutingModule } from "./fee-detail-routing.module";
import { GridViewModule } from 'nativescript-grid-view/angular';
import { NgModalModule } from "../modals/ng-modal";
// import { NgShadowModule } from 'nativescript-ng-shadow';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
    imports: [
        HttpModule,
        FeeDetailRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NgModalModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        FeeDetailComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class FeeDetailModule { }
