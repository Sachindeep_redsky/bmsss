import { UserService } from './../../services/user.service';
import { Fees } from './../../models/fees.model';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { ActivatedRoute } from "@angular/router";
import { Values } from '~/app/values/values';
import * as localstorage from "nativescript-localstorage";
import { ModalComponent } from '~/app/modals/modal.component';
import * as Toast from 'nativescript-toast';

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-feeDetail",
    moduleId: module.id,
    templateUrl: "./fee-detail.component.html",
    styleUrls: ['./fee-detail.component.css'],
})

export class FeeDetailComponent implements OnInit {

    @ViewChild('payFeeDialog', { static: false }) payFeeDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    feeDetail = [];
    year: string;
    isLoading: boolean;
    admissionNo: string;
    token: string;
    headers: HttpHeaders;
    fees: Fees;
    dialogCancelButton: string;
    dialogPayButton: string;
    payFeeHint: string;
    payFeeBorderColor: string;
    payFee: number;
    receiptHint: string;
    receiptBorderColor: string;
    receiptNumber: string;
    remarkHint: string;
    remarkBorderColor: string;
    remark: string;
    transRemarkHint: string;
    transRemarkBorderColor: string;
    transRemark: string;
    transNoHint: string;
    transNoBorderColor: string;
    transNo: string;
    bankNameHint: string;
    bankNameBorderColor: string;
    bankName: string;
    feeId: string;
    isStudent: boolean;
    isCash: boolean;
    feeChargesId: number;
    profilePic: string;
    isData: boolean;
    feeDetailMessage: string;

    constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions, private page: Page, private http: HttpClient, private userService: UserService) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.isLoading = false;
        this.admissionNo = "";
        this.token = "";
        this.fees = new Fees();
        this.payFeeHint = "Enter payment amount";
        this.payFeeBorderColor = "white";
        this.payFee = 0;
        this.receiptHint = "Enter receipt number";
        this.receiptBorderColor = "white";
        this.receiptNumber = "";
        this.remarkHint = "Enter remark (optional)";
        this.remarkBorderColor = "white";
        this.remark = "";
        this.transRemarkHint = "Enter transaction remark (optional)";
        this.transRemarkBorderColor = "white";
        this.transRemark = "";
        this.transNoHint = "Enter transaction number";
        this.transNoBorderColor = "white";
        this.transNo = "";
        this.bankNameHint = "Enter bank name";
        this.bankNameBorderColor = "white";
        this.bankName = "";
        this.feeId = "";
        this.isStudent = false;
        this.dialogCancelButton = "Cancel";
        this.dialogPayButton = "Pay";
        this.isCash = true;
        this.feeChargesId = 0;
        this.profilePic = "";
        this.isData = false;
        this.feeDetailMessage = "";
        this.userService.activeScreen("feeDetail");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        if (localstorage.getItem("userType") != null && localstorage.getItem("userType") != undefined) {
            if (localstorage.getItem("userType") == "student") {
                this.isStudent = true;
            }
        }
        this.route.queryParams.subscribe(params => {
            this.admissionNo = params["admissionNo"];
            localstorage.setItem("admissionNo", this.admissionNo);
        });
        this.getFees();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)

        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }

        // this.feeDetail.push({ timePeriod: "QUARTER 1", totalFee: "6250", paidFee: "6250", dueFee: "0", date: "01-04-2019" });
        // this.feeDetail.push({ timePeriod: "QUARTER 2", totalFee: "6250", paidFee: "0", dueFee: "6250", date: "01-07-2019" });
        // this.feeDetail.push({ timePeriod: "QUARTER 3", totalFee: "3500", paidFee: "0", dueFee: "3500", date: "01-10-2019" });
        // this.feeDetail.push({ timePeriod: "QUARTER 4", totalFee: "3500", paidFee: "0", dueFee: "3500", date: "01-01-2019" });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onPayFeeDialogLoaded(args) {
        var payFeeDialog = <any>args.object;
        setTimeout(() => {
            if (payFeeDialog.android) {
                let nativeImageView = payFeeDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (payFeeDialog.ios) {
                let nativeImageView = payFeeDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onPayFeeTextChanged(args) {
        this.payFee = args.object.text;
        // this.payFeeBorderColor = "white";
        // if (this.payFee == "") {
        //     this.payFeeBorderColor = "black";
        // }
    }

    onReceiptTextChanged(args) {
        this.receiptNumber = args.object.text;
        // this.receiptBorderColor = "white";
        // if (this.receiptNumber == "") {
        //     this.receiptBorderColor = "black";
        // }
    }

    onRemarkTextChanged(args) {
        this.remark = args.object.text;
        // this.remarkBorderColor = "white";
        // if (this.remark == "") {
        //     this.remarkBorderColor = "black";
        // }
    }

    onTransRemarkTextChanged(args) {
        this.transRemark = args.object.text;
        // this.transRemarkBorderColor = "white";
        // if (this.transRemark == "") {
        //     this.transRemarkBorderColor = "black";
        // }
    }

    onTransNoTextChanged(args) {
        this.transNo = args.object.text;
        // this.transNoBorderColor = "white";
        // if (this.transNo == "") {
        //     this.transNoBorderColor = "black";
        // }
    }

    onBankNameTextChanged(args) {
        this.bankName = args.object.text;
        // this.bankNameBorderColor = "white";
        // if (this.bankName == "") {
        //     this.bankNameBorderColor = "black";
        // }
    }

    getFees() {
        if (this.admissionNo == "") {
            alert("Please select admission number first.");
        }
        else {
            this.isLoading = true;
            this.http
                .get(Values.BASE_URL + "fees?admissionNo=" + this.admissionNo, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.trace(res);
                            this.isLoading = false;
                            if (res.data.fee.length > 0) {
                                this.isData = false;
                                this.feeDetail = [];
                                for (var i = 0; i < res.data.fee.length; i++) {
                                    this.feeDetail.push({
                                        "id": res.data.fee[i].id,
                                        "feeChargesId": res.data.fee[i].feeChargesId,
                                        "totalAmount": res.data.fee[i].dueAmount,
                                        "balanceAmount": res.data.fee[i].balanceAmount,
                                        "feeCharges": res.data.fee[i].feeChargesName
                                    })
                                }
                            }
                            else {
                                this.isData = true;
                                this.feeDetailMessage = "There is no fees for this admission number.";
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    onPayFees(item: any) {
        this.feeId = item.id;
        this.feeChargesId = item.feeChargesId;
        this.payFee = 0;
        this.receiptNumber = "";
        this.transNo = "";
        this.bankName = "";
        this.transRemark = "";
        this.remark = "";
        this.payFeeDialog.show();
    }

    onCancel() {
        this.payFeeDialog.hide();
    }

    onPay() {
        if (this.receiptNumber == "") {
            alert("Please enter receipt number.")
        }
        else if (this.payFee == 0) {
            alert("Please enter payment amount.")
        }
        else {
            this.isLoading = true;
            this.fees = new Fees();
            this.fees.feeChargesId = this.feeChargesId;
            this.fees.admissionNo = localstorage.getItem("admissionNo");
            this.fees.receiptNo = this.receiptNumber;
            this.fees.payAmount = this.payFee;
            this.fees.remark = this.remark;
            if (!this.isCash) {
                if (this.transNo == "") {
                    alert("Please enter transaction number.");
                }
                else if (this.bankName == "") {
                    alert("Please enter bank name.");
                }
                else {
                    this.fees.transNo = this.transNo;
                    this.fees.bankName = this.bankName;
                    this.fees.transRemark = this.transRemark;
                    this.fees.transType = "cheque";
                }
            }
            // if (this.isCash == false) {
            //     this.fees.transNo = this.transNo;
            //     this.fees.bankName = this.bankName;
            //     this.fees.transRemark = this.transRemark;
            //     this.fees.transType = "cheque";
            // }
            // else {
            // this.fees.transType = "cash";
            // }
            console.log(this.fees);
            this.http
                .put(Values.BASE_URL + "fees/" + this.feeId, this.fees, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.isLoading = false;
                            Toast.makeText("Fee is successfully updated.", "long").show();
                            this.payFeeDialog.hide();
                            this.getFees();
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    this.payFeeDialog.hide();
                    alert(error.error.error);
                });
        }
    }

    onCash() {
        this.isCash = true;
    }

    onCheck() {
        this.isCash = false;
    }

    onBack() {
        this.routerExtensions.back();
    }

    onFeeReceipts() {
        this.routerExtensions.navigate(['/feeReceipts'], {
            queryParams: {
                "admissionNo": localstorage.getItem("admissionNo")
            }
        });
    }
}