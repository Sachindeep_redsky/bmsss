import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { FeeDetailComponent } from "./components/fee-detail.component";

const routes: Routes = [
    { path: "", component: FeeDetailComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class FeeDetailRoutingModule { }
