import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { UserService } from "~/app/services/user.service";
import * as localstorage from "nativescript-localstorage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { Router, NavigationExtras } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { ModalComponent } from '~/app/modals/modal.component';
import { DatePicker } from "tns-core-modules/ui/date-picker";

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-messages",
    moduleId: module.id,
    templateUrl: "./messages.component.html",
    styleUrls: ['./messages.component.css'],
})

export class MessagesComponent implements OnInit {
    @ViewChild('viewMessageDialog', { static: false }) viewMessageDialog: ModalComponent;
    @ViewChild('filterDialog', { static: false }) filterDialog: ModalComponent;
    @ViewChild('sortDialog', { static: false }) sortDialog: ModalComponent;
    @ViewChild('viewDatePickerDialog', { static: false }) viewDatePickerDialog: ModalComponent;
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;
    @ViewChild('selectSectionDialog', { static: false }) selectSectionDialog: ModalComponent;
    @ViewChild('selectTeacherDialog', { static: false }) selectTeacherDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    messages;
    year: string;
    userType: string;
    pageNo: number;
    count: number;
    classId: string;
    classSection: string;
    date: string;
    replyDate: string;
    queryDate: string;
    pendingMessage: string;
    profilePic: string;
    isVisibleCompose: boolean;
    question: string;
    answer: string;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    query: string;
    sortQuery: string;
    day: string;
    month: string;
    sessionYear: string;
    isClassPicker: boolean;
    isSectionPicker: boolean;
    classTeacher;
    classes;
    classButton: string;
    sectionButton: string;
    teacherButton: string;
    index: number;
    sections;
    isTeacher: boolean;
    isShowFilters: boolean;
    isShowMessages: boolean;
    isMessages: boolean;
    messagesText: string;
    teachers;
    isStudent: boolean;
    teacherId: string;
    allClasses;
    selectedClasses;
    isVisibleAnswer: boolean;
    constructor(private router: Router, private userService: UserService, private http: HttpClient, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.sessionYear =  Values.SESSION;
        this.userType = "";
        this.pageNo = 1;
        this.count = 10;
        this.classId = "";
        this.classSection = "";
        this.messages = [];
        this.date = "";
        this.queryDate = date.toString();
        this.isClassPicker = true;
        this.isSectionPicker = false;
        this.replyDate = "";
        this.profilePic = "res://profile";
        this.question = "";
        this.answer = "";
        this.isLoading = false;
        this.token = "";
        this.query = "";
        this.sortQuery = "";
        this.classTeacher = [];
        this.classes = [];
        this.day = date.getDate().toString();
        this.month = (date.getMonth() + 1).toString();
        this.year = date.getFullYear().toString();
        this.classButton = "Select class";
        this.sectionButton = "Select section";
        this.teacherButton = "Select teacher";
        this.index = 0;
        this.sections = [];
        this.isTeacher = false;
        this.isStudent = false;
        this.isShowFilters = false;
        this.isShowMessages = true;
        this.isMessages = false;
        this.messagesText = "Select teacher to show messages";
        this.teachers = [];
        this.allClasses = [];
        this.selectedClasses = [];
        this.userService.activeScreen("messages");
        this.isVisibleAnswer = true;
        this.page.on('navigatedTo', (data) => {
            if (data.isBackNavigation) {
                this.userService.activeScreen("messages");
                this.messages = [];
                this.getMessages();
            }
        });
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            if (localstorage.getItem("userType") == "student") {
                this.userType = "student";
                this.isTeacher = false;
                this.isShowFilters = true;
                this.isShowMessages = true;
                this.isStudent = true;
                this.getClassSection();
                this.pendingMessage = "Answer is pending...";
                this.answer = "Answer is pending...";
                this.isVisibleCompose = true;
            }
            else if (localstorage.getItem("userType") == "admin") {
                this.userType = "admin";
                this.isVisibleCompose = true;
                this.isShowFilters = false;
                this.isShowMessages = false;
                this.isTeacher = true;
                this.isStudent = false;
            }
            else {
                this.teacherId = localstorage.getItem("userId");
                this.isMessages = true;
                this.messagesText = "Select class and section to show messages.";
                this.isShowFilters = false;
                this.isShowMessages = false;
                this.userType = "teacher";
                this.isTeacher = true;
                this.isStudent = false;
                // this.getClassSection();
                this.isVisibleCompose = false;
                this.pendingMessage = "Answer is pending, click here to reply...";
                this.answer = "Answer is pending, click here to reply...";
            }
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        if (this.userType == "teacher") {
            this.router.navigate(['/homeTeacher']);
        }
        else {
            this.router.navigate(['/homeStudent']);
        }
    }

    onClass() {
        this.isMessages = true;
        this.messagesText = "Select class and section to show messages.";
        this.isSectionPicker = false;
        this.isShowFilters = false;
        this.isShowMessages = false;
        this.sectionButton = "Select Section";
        // this.homeworkList = [];
        this.getClasses();
    }

    onSelectTeacher() {
        this.selectTeacherDialog.show();
        this.isShowMessages = false;
        this.teachers = [];
        this.getTeachers();
    }

    getTeachers() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + `users/${localstorage.getItem("userId")}`, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        // console.log("TEACHERS RESSSS::::", res);
                        if (res.data.classTeacher.length > 0) {
                            for (var i = 0; i < res.data.classTeacher.length; i++) {
                                this.teachers.push({
                                    id: res.data.classTeacher[i].id,
                                    name: res.data.classTeacher[i].name
                                })
                            }
                            this.isLoading = false;
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onTeacherButton(item: any) {
        this.teacherId = item.id;
        this.teacherButton = item.name;
        this.isShowMessages = true;
        this.messages = [];
        this.getMessages();
    }

    onTeacherOutsideClick() {
        this.selectTeacherDialog.hide();
    }

    onClassOutsideClick() {
        this.selectClassDialog.hide();
    }

    onSectionOutsideClick() {
        this.selectSectionDialog.hide();
    }

    onClassButton(item: any, index: any) {
        this.classId = item.id;
        this.classButton = item.name;
        this.index = index;
        this.isSectionPicker = true;
    }

    onSectionButton(item: any) {
        this.sectionButton = item.classSection;
        this.classSection = item.classSection;
        if (this.userType == "teacher") {
            this.isShowFilters = true;
            this.isShowMessages = true;
        }
        else {
            this.isShowFilters = false;
            this.isShowMessages = true;
        }
        this.messages = [];
        this.isLoading = true;
        this.getMessages();
    }

    onSection() {
        this.sections = [];
        if (this.userType == "admin") {
            this.sections = this.classes[this.index].section;
        } else {
            // console.log(this.classTeacher[this.index].section.length);
            for (var i = 0; i < this.classTeacher[this.index].section.length; i++) {
                this.sections.push({
                    classSection: this.classTeacher[this.index].section[i].classSection,
                });
            }
        }
        this.selectSectionDialog.show();
    }

    getClasses() {
        this.isLoading = true;
        // console.log(`users/search?pageNo=${this.pageNo}&count=${this.count}&userType=teacher&active=true&classIncharge=false&regNo=${localstorage.getItem("regNo")}`)
        if (this.userType == "admin") {
            this.http
                .get(Values.BASE_URL + `classes`, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            if (res.data.length > 0) {
                                this.classes = [];
                                for (var i = 0; i < res.data.length; i++) {
                                    if (res.data[i].status == "active") {
                                        this.classes.push({
                                            id: res.data[i].id,
                                            isSelected: false,
                                            name: res.data[i].name,
                                            section: res.data[i].section
                                        })
                                    }
                                }
                                this.selectClassDialog.show();
                                this.isLoading = false;
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        } else {
            this.http
                .get(Values.BASE_URL + `users/search?pageNo=${this.pageNo}&userType=teacher&regNo=${localstorage.getItem("regNo")}`, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("RESSSS", res);
                            if (res.data.users[0].classTeacher.length > 0) {
                                this.classTeacher = res.data.users[0].classTeacher;
                                if (res.data.users[0].classIncharge == "true") {
                                    this.classTeacher.push({
                                        "id": res.data.users[0].classId,
                                        "name": res.data.users[0].className,
                                        "section": [{ "classSection": res.data.users[0].classSection }]
                                    })
                                }
                                console.trace("CLASS TEACHER", this.classTeacher);
                                this.classes = [];
                                for (var i = 0; i < this.classTeacher.length; i++) {
                                    this.classes.push({
                                        index: i,
                                        id: this.classTeacher[i].id,
                                        name: this.classTeacher[i].name
                                    });
                                }
                                this.selectClassDialog.show();
                                this.isLoading = false;
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    onMessageCompose() {
        this.routerExtensions.navigate(['/messageCompose']);
    }

    getClassSection() {
        this.userService.showLoadingState(true);
        this.http
            .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"), {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.classId = res.data.classId;
                        this.classSection = res.data.classSection;
                        this.userService.showLoadingState(false);
                        // this.getMessages();
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    onDescending() {
        this.sortQuery = "&isDescending=true";
        this.messages = [];
        this.getMessages();
    }

    onAscending() {
        this.sortQuery = "&isDescending=false";
        this.messages = [];
        this.getMessages();
    }

    onToday() {
        this.query = "&messageStatus=today";
        this.messages = [];
        this.getMessages();
    }

    onSevenDays() {
        this.query = "&messageStatus=sevenday";
        this.messages = [];
        this.getMessages();
    }

    onCustom() {
        this.viewDatePickerDialog.show();
    }

    getMessages() {
        console.log(`msges?pageNo=${this.pageNo}&count=${this.count}&teacherId=${this.teacherId}&classId=${this.classId}&classSection=${this.classSection}` + this.query + this.sortQuery);
        if (this.userType == "admin") {
            console.log(`msges?pageNo=${this.pageNo}&count=${this.count}&type=admin&classId=${this.classId}&classSection=${this.classSection}`)
            this.http
                .get(Values.BASE_URL + `msges?pageNo=${this.pageNo}&count=${this.count}&type=${this.userType}&classId=${this.classId}&classSection=${this.classSection}`, {
                    // .get(Values.BASE_URL + `msges?pageNo=${this.pageNo}&count=${this.count}`, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("MSGES::::", res);
                            if (res.data.length > 0) {
                                this.isMessages = false;
                                this.messagesText = "";
                                for (var i = 0; i < res.data.length; i++) {
                                    // if (res.data[i].answer == null) {
                                    //     this.messages.push({
                                    //         question: res.data[i].question,
                                    //         userName: res.data[i].userName,
                                    //         teacherName: res.data[i].teacherName,
                                    //         answer: "Answer is pending, click here to reply",
                                    //         date: res.data[i].date
                                    //     })
                                    // }
                                    this.date = res.data[i].date;
                                    this.replyDate = "";
                                    this.replyDate = res.data[i].replyDate;
                                    var dateTime = new Date(this.date);
                                    var replyDateTime = new Date(this.replyDate);
                                    var hours = dateTime.getHours();
                                    var replyHours = replyDateTime.getHours();
                                    var ampm = "am";
                                    var replyampm = "am";
                                    console.log("HOURS::::", hours)
                                    var finalHours = "";
                                    if (hours > 12) {
                                        var hours = hours - 12;
                                        ampm = "pm";
                                        if (hours < 10) {
                                            finalHours = "0" + hours;
                                        }
                                        else {
                                            finalHours = hours.toString();
                                        }
                                    }
                                    else {
                                        finalHours = hours.toString();
                                    }
                                    var finalReplyHours = "";
                                    if (replyHours > 12) {
                                        var replyHours = replyHours - 12;
                                        replyampm = "pm";
                                        if (replyHours < 10) {
                                            finalReplyHours = "0" + replyHours;
                                        }
                                        else {
                                            finalReplyHours = replyHours.toString();
                                        }
                                    }
                                    else {
                                        finalReplyHours = replyHours.toString();
                                    }
                                    var minutes = dateTime.getMinutes().toString()
                                    if (minutes.length < 2) {
                                        minutes = "0" + minutes;
                                    }
                                    var replyMinutes = replyDateTime.getMinutes().toString();
                                    if (replyMinutes.length < 2) {
                                        replyMinutes = "0" + replyMinutes;
                                    }
                                    this.date = dateTime.getDate().toString() + "/" + (dateTime.getMonth() + 1).toString() + "/" + dateTime.getFullYear().toString() + " (" + finalHours + ":" + minutes + " " + ampm + ")";
                                    this.replyDate = replyDateTime.getDate().toString() + "/" + (replyDateTime.getMonth() + 1).toString() + "/" + replyDateTime.getFullYear().toString() + " (" + finalReplyHours + ":" + replyMinutes + " " + replyampm + ")";
                                    this.messages.push({
                                        id: res.data[i].id,
                                        userType: res.data[i].userType,
                                        question: res.data[i].question,
                                        userName: res.data[i].userName,
                                        teacherName: res.data[i].teacherName,
                                        answer: res.data[i].answer,
                                        date: this.date,
                                        replyDate: this.replyDate
                                    })
                                }
                                // this.userService.showLoadingState(false);
                                this.isLoading = false;
                                // this.pageNo = this.pageNo + 1;
                                // this.getMessages();
                            }
                            if (res.data.length == 0) {
                                // this.userService.showLoadingState(false);
                                this.isLoading = false;
                                this.isMessages = true;
                                this.messagesText = "There is no messages to show.";
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    // this.userService.showLoadingState(false);
                    console.log(error.error.error);
                });
        }
        else {
            this.http
                .get(Values.BASE_URL + `msges?pageNo=${this.pageNo}&count=${this.count}&teacherId=${this.teacherId}&classId=${this.classId}&classSection=${this.classSection}` + this.query + this.sortQuery, {
                    // .get(Values.BASE_URL + `msges?pageNo=${this.pageNo}&count=${this.count}`, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("MSGES::::", res);
                            if (res.data.length > 0) {
                                this.isMessages = false;
                                this.messagesText = "";
                                for (var i = 0; i < res.data.length; i++) {
                                    // if (res.data[i].answer == null) {
                                    //     this.messages.push({
                                    //         question: res.data[i].question,
                                    //         userName: res.data[i].userName,
                                    //         teacherName: res.data[i].teacherName,
                                    //         answer: "Answer is pending, click here to reply",
                                    //         date: res.data[i].date
                                    //     })
                                    // }
                                    this.date = res.data[i].date;
                                    this.replyDate = "";
                                    this.replyDate = res.data[i].replyDate;
                                    var dateTime = new Date(this.date);
                                    var replyDateTime = new Date(this.replyDate);
                                    var hours = dateTime.getHours();
                                    var replyHours = replyDateTime.getHours();
                                    var ampm = "am";
                                    var replyampm = "am";
                                    console.log("HOURS::::", hours)
                                    var finalHours = "";
                                    if (hours > 12) {
                                        var hours = hours - 12;
                                        ampm = "pm";
                                        if (hours < 10) {
                                            finalHours = "0" + hours;
                                        }
                                        else {
                                            finalHours = hours.toString();
                                        }
                                    }
                                    else {
                                        finalHours = hours.toString();
                                    }
                                    var finalReplyHours = "";
                                    if (replyHours > 12) {
                                        var replyHours = replyHours - 12;
                                        replyampm = "pm";
                                        if (replyHours < 10) {
                                            finalReplyHours = "0" + replyHours;
                                        }
                                        else {
                                            finalReplyHours = replyHours.toString();
                                        }
                                    }
                                    else {
                                        finalReplyHours = replyHours.toString();
                                    }
                                    var minutes = dateTime.getMinutes().toString()
                                    if (minutes.length < 2) {
                                        minutes = "0" + minutes;
                                    }
                                    var replyMinutes = replyDateTime.getMinutes().toString();
                                    if (replyMinutes.length < 2) {
                                        replyMinutes = "0" + replyMinutes;
                                    }
                                    this.date = dateTime.getDate().toString() + "/" + (dateTime.getMonth() + 1).toString() + "/" + dateTime.getFullYear().toString() + " (" + finalHours + ":" + minutes + " " + ampm + ")";
                                    this.replyDate = replyDateTime.getDate().toString() + "/" + (replyDateTime.getMonth() + 1).toString() + "/" + replyDateTime.getFullYear().toString() + " (" + finalReplyHours + ":" + replyMinutes + " " + replyampm + ")";
                                    this.messages.push({
                                        id: res.data[i].id,
                                        userType: res.data[i].userType,
                                        question: res.data[i].question,
                                        userName: res.data[i].userName,
                                        teacherName: res.data[i].teacherName,
                                        answer: res.data[i].answer,
                                        date: this.date,
                                        replyDate: this.replyDate
                                    })
                                }
                                // this.userService.showLoadingState(false);
                                this.isLoading = false;
                                // this.pageNo = this.pageNo + 1;
                                // this.getMessages();
                            }
                            if (res.data.length == 0) {
                                // this.userService.showLoadingState(false);
                                this.isLoading = false;
                                this.isMessages = true;
                                this.messagesText = "There is no messages to show.";
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    // this.userService.showLoadingState(false);
                    console.log(error.error.error);
                });
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onPickerLoaded(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.year = date.getFullYear();
        datePicker.month = date.getMonth() + 1;
        datePicker.day = date.getDate();
        datePicker.minDate = new Date(2020, 3, 1);
        datePicker.maxDate = new Date(2021, 2, 31);
    }

    onDateChanged(args) {
        console.log("Date", args.value);
        this.queryDate = args.value;
    }

    onDayChanged(args) {
        this.day = args.value;
    }

    onMonthChanged(args) {
        this.month = args.value;
    }

    onYearChanged(args) {
        this.year = args.value;
    }

    onOkDate() {
        this.viewDatePickerDialog.hide();
        this.query = "&messageStatus=custom&date=" + this.queryDate;
        this.messages = [];
        this.getMessages();
    }

    onDatePickerDialogLoaded(args) {
        var datePickerDialog = <any>args.object;
        setTimeout(() => {
            if (datePickerDialog.android) {
                let nativeImageView = datePickerDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#D9D9D9'));
                nativeImageView.setElevation(10)
            } else if (datePickerDialog.ios) {
                let nativeImageView = datePickerDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSectionDialogLoaded(args) {
        var sectionDialog = <any>args.object;
        setTimeout(() => {
            if (sectionDialog.android) {
                let nativeImageView = sectionDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (sectionDialog.ios) {
                let nativeImageView = sectionDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(10)
            } else if (image.ios) {
                let nativeImageView = image.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onMessageDialogLoaded(args) {
        var messageDialog = <any>args.object;

        setTimeout(() => {
            if (messageDialog.android) {
                let nativeImageView = messageDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#D9D9D9'));
                nativeImageView.setElevation(10)
            } else if (messageDialog.ios) {
                let nativeImageView = messageDialog.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onFilterDialogLoaded(args) {
        var filterDialog = <any>args.object;

        setTimeout(() => {
            if (filterDialog.android) {
                let nativeImageView = filterDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (filterDialog.ios) {
                let nativeImageView = filterDialog.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSortDialogLoaded(args) {
        var sortDialog = <any>args.object;

        setTimeout(() => {
            if (sortDialog.android) {
                let nativeImageView = sortDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20);
            } else if (sortDialog.ios) {
                let nativeImageView = sortDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onAnswerClick(id: string, answer: string) {
        if (this.userType == "teacher") {
            this.viewMessageDialog.hide();
            let navigationExtras: NavigationExtras = {
                queryParams: {
                    "messageId": id,
                    "answer": answer,
                }
            }
            this.routerExtensions.navigate(['/messageCompose'], navigationExtras)
        }
        else {
            this.viewMessageDialog.show();
        }
    }

    onMessageClick(question: string, answer: string, userType: string) {
        this.question = question;
        if (answer != null && answer != undefined) {
            this.answer = answer;
        }
        else {
            if (localstorage.getItem("userType") == "student") {
                this.answer = "Answer is pending...";
            }
            else {
                this.answer = "Answer is pending, click here to reply...";
            }
        }
        if (userType == "admin") {
            this.isVisibleAnswer = false;
        } else {
            this.isVisibleAnswer = true;
        }
        this.viewMessageDialog.show();
    }

    onHide() {
        this.viewMessageDialog.hide();
    }
    onOutsideClick() {
        this.viewMessageDialog.hide();
    }

    onFilterOutsideClick() {
        this.filterDialog.hide();
    }

    onFilterClick() {
        this.filterDialog.show();
    }

    onSortOutsideClick() {
        this.sortDialog.hide();
    }

    onSortClick() {
        this.sortDialog.show();
    }
}