import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AddHomeworkComponent } from "./components/add-homework.component";

const routes: Routes = [
    { path: "", component: AddHomeworkComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AddHomeworkRoutingModule { }
