import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { ModalComponent } from "~/app/modals/modal.component";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { Homework } from "~/app/models/homework.model";
import * as localstorage from "nativescript-localstorage";
import * as Toast from 'nativescript-toast';
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-addHomework",
    moduleId: module.id,
    templateUrl: "./add-homework.component.html",
    styleUrls: ['./add-homework.component.css'],
})

export class AddHomeworkComponent implements OnInit {
    @ViewChild('selectSubjectDialog', { static: false }) selectSubjectDialog: ModalComponent;
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    year: string;
    homeworkHint: string;
    homeworkBorderColor: string;
    classworkHint: string;
    classworkBorderColor: string;
    classwork: string;
    homework: string;
    subjects;
    selectSubjectText: string;
    subjectName: string;
    selectClassText: string;
    classes;
    classId: string;
    subjectId: string;
    homeworks: Homework;
    section: string;
    type: string;
    homeworkId: string;
    token: string;
    headers: HttpHeaders;
    profilePic: string;
    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private route: ActivatedRoute, private http: HttpClient, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;;
        this.homeworkHint = "Enter homework";
        this.homeworkBorderColor = "black";
        this.classworkHint = "Enter classwork";
        this.classworkBorderColor = "black";
        this.classwork = "";
        this.userService.showMessageCompose(false);
        this.userService.activeScreen("addHomework");
        this.selectSubjectText = "Select Subject";
        this.subjectName = "";
        this.selectClassText = "Select Class";
        this.classes = [];
        this.subjects = [];
        this.classId = "";
        this.subjectId = "";
        this.homeworks = new Homework();
        this.section = "";
        this.type = "";
        this.homeworkId = "";
        this.token = "";
        this.profilePic = "res://profile";
        this.route.queryParams.subscribe(params => {
            if (params["subjectName"] != undefined) {
                this.selectSubjectText = params["subjectName"];
                this.subjectName = params["subjectName"];
            }
            if (params["homeworkDetail"] != undefined) {
                this.homework = params["homeworkDetail"];
            }
            if (params["classworkDetail"] != undefined) {
                this.classwork = params["classworkDetail"];
            }
            if (params["type"] != undefined) {
                this.type = params["type"];
            }
            if (params["homeworkId"] != undefined) {
                // this.classId = params["classId"];
                this.homeworkId = params["homeworkId"];
            }
        });

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            // if (localstorage.getItem("userType") == "teacher") {
            this.route.queryParams.subscribe(params => {
                if (params["classId"] != undefined) {
                    this.classId = params["classId"];
                }
                if (params["section"] != undefined) {
                    this.section = params["section"];
                }
            });
            // }
            // else {
            //     this.getClassSection();
            // }
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)

        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    getClassSection() {
        this.userService.showLoadingState(true);
        this.http
            .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"), {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.section = res.data.classSection;
                        this.classId = res.data.classId;
                        this.userService.showLoadingState(false);
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    onBack() {
        // this.routerExtensions.navigate(['/homework']);
        this.routerExtensions.back();
    }

    onClassworkTextChanged(args) {
        this.classwork = args.object.text;
        this.classworkBorderColor = "#A61F21";
        if (this.classwork == "") {
            this.homeworkBorderColor = "black";
        }
    }

    onHomeworkTextChanged(args) {
        this.homework = args.object.text;
        this.homeworkBorderColor = "#A61F21";
        if (this.classwork == "") {
            this.homeworkBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onGridLoaded(args) {
        var grid = <any>args.object;
        setTimeout(() => {
            if (grid.android) {
                let nativeImageView = grid.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                nativeImageView.setElevation(20)
            } else if (grid.ios) {
                let nativeImageView = grid.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSubjectDialogLoaded(args) {
        var subjectDialog = <any>args.object;
        setTimeout(() => {
            if (subjectDialog.android) {
                let nativeImageView = subjectDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (subjectDialog.ios) {
                let nativeImageView = subjectDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    // onClassDialogLoaded(args) {
    //     var classDialog = <any>args.object;
    //     setTimeout(() => {
    //         if (classDialog.android) {
    //             let nativeImageView = classDialog.android;
    //             var shape = new android.graphics.drawable.GradientDrawable();
    //             shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
    //             shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
    //             nativeImageView.setElevation(20)
    //         } else if (classDialog.ios) {
    //             let nativeImageView = classDialog.ios;
    //             nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
    //             nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
    //             nativeImageView.layer.shadowOpacity = 0.5
    //             nativeImageView.layer.shadowRadius = 5.0
    //         }
    //     }, 400)
    // }

    onSubmitHomework() {
        if (this.subjectName == "") {
            alert("Please select subject.");
        }
        else if (this.homework == "") {
            alert("Please enter homework.");
        }
        else {
            this.homeworks.classWork = this.classwork;
            this.homeworks.homeWork = this.homework;
            this.homeworks.classId = this.classId;
            this.homeworks.subjectId = this.subjectId;
            this.homeworks.section = this.section;
            this.userService.showLoadingState(true);
            console.log(this.homeworks);
            if (this.type == "edit") {
                this.http
                    .put(Values.BASE_URL + "homeworks/update/" + this.homeworkId, this.homeworks, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.userService.showLoadingState(false);
                                Toast.makeText("Homework updated successfully!!!", "long").show();
                                // this.routerExtensions.navigate(['/homework']);
                                this.routerExtensions.back();
                            }
                            else {
                                // alert(res.message);
                                this.userService.showLoadingState(false);
                                // this.routerExtensions.navigate(['/homework']);
                                this.routerExtensions.back();
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        alert(error.error.error);
                    });
            }
            else {
                this.http
                    .post(Values.BASE_URL + "homeworks/create", this.homeworks, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.userService.showLoadingState(false);
                                Toast.makeText("Homework added successfully!!!", "long").show();
                                // this.routerExtensions.navigate(['/homework']);
                                this.routerExtensions.back();
                            }
                            else {
                                alert(res.message);
                                this.userService.showLoadingState(false);
                                // this.routerExtensions.navigate(['/homework']);
                                this.routerExtensions.back();
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        alert(error.error.error);
                    });
            }


            // let navigationExtras: NavigationExtras = {
            //     queryParams: {
            //         "message": this.homework
            //     }
            // }
            // this.router.navigate(['./homeworkTeacher'], navigationExtras);
        }
    }

    // getClasses() {
    //     this.http
    //         .get(Values.BASE_URL + "classes")
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     this.classes = [];
    //                     for (var i = 0; i < res.data.length; i++) {
    //                         this.classes.push({
    //                             id: res.data[i].id,
    //                             name: res.data[i].name,
    //                         });
    //                     }
    //                     this.selectClassDialog.show();
    //                     this.userService.showLoadingState(false);
    //                 }
    //             }
    //         }, error => {
    //             this.userService.showLoadingState(false);
    //             console.log(error.error.error);
    //         });
    // }

    getSubjects() {
        this.userService.showLoadingState(true);
        this.http
            .get(Values.BASE_URL + "subjects?classId=" + this.classId + "&status=active", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.subjects = [];
                        for (var i = 0; i < res.data.length; i++) {
                            this.subjects.push({
                                id: res.data[i].id,
                                name: res.data[i].name,
                                status: res.data[i].status
                            });
                        }
                        this.selectSubjectDialog.show();
                        this.userService.showLoadingState(false);
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    onSelectSubject() {
        this.getSubjects();
    }

    // onSelectClass() {
    //     this.getClasses();
    // }

    onSubjectButton(item: any) {
        this.subjectId = item.id;
        this.selectSubjectText = item.name;
        this.subjectName = item.name;
    }

    // onClassButton(item: any) {
    //     this.selectClassText = item.name;
    //     this.classId = item.id;
    // }

    onSubjectOutsideClick() {
        this.selectSubjectDialog.hide();
    }

    onClassOutsideClick() {
        this.selectClassDialog.hide();
    }
}