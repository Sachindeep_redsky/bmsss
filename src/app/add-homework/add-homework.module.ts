import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from 'nativescript-ng-shadow';
import { AddHomeworkRoutingModule } from "./add-homework-routing.module";
import { AddHomeworkComponent } from "./components/add-homework.component";
import { NgModalModule } from "../modals/ng-modal";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
    imports: [
        HttpModule,
        AddHomeworkRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NgModalModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        AddHomeworkComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AddHomeworkModule { }