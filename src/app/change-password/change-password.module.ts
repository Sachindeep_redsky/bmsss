import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ChangePasswordComponent } from "./components/change-password.component";

@NgModule({
    bootstrap: [
        ChangePasswordComponent
    ],
    imports: [
        HttpModule,
        ChangePasswordRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        ChangePasswordComponent,
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class ChangePasswordModule { }
