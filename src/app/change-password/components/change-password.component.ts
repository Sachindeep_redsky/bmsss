import { Component, ElementRef, ViewChild } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Color } from "tns-core-modules/color/color";
import { User } from "~/app/models/user.model";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { UserService } from "~/app/services/user.service";
import { Page } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-changePassword",
    moduleId: module.id,
    templateUrl: "./change-password.component.html",
    styleUrls: ['./change-password.component.css'],
})

export class ChangePasswordComponent {

    inputStyle = 'inputInactive';
    textfield: TextField;
    forgot_password: boolean;
    oldPasswordText: string = "";
    newPasswordText: string = "";
    repeatNewPasswordText: string = "";
    passwordIcon: string;
    isRendering: boolean;
    renderViewTimeout;
    user: User;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.passwordIcon = "res://password";
        this.user = new User();
        this.isLoading = false;
        this.userService.activeScreen("changePassword");
        this.token = "";
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
    }

    ngOnInit(): void {
        this.inputStyle = 'inputInactive';
        this.forgot_password = false;
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }


    onGridLoaded(args: any) {
        var gridMain = <any>args.object;
        setTimeout(() => {
            if (gridMain.android) {
                let nativeGridMain = gridMain.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridMain.ios) {
                let nativeGridMain = gridMain.ios;
                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 400)

    }

    public oldPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.oldPasswordText = textField.text;
    }

    public newPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.newPasswordText = textField.text;
    }

    public repeatNewPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.repeatNewPasswordText = textField.text;
    }

    onChangePasswordClick() {
        if (this.oldPasswordText == "") {
            alert("Old password cannot be empty");
        }
        else if (this.newPasswordText == "") {
            alert("New passsword cannot be empty");
        }
        else if (this.repeatNewPasswordText == "") {
            alert("Repeat new passsword cannot be empty");
        }
        else if (this.newPasswordText != this.repeatNewPasswordText) {
            alert("New password and repeat password must be same");
        }
        else {
            this.isLoading = true;
            this.user.oldPassword = this.oldPasswordText;
            this.user.newPassword = this.newPasswordText;
            this.userService.showLoadingState(true);
            this.http
                .post(Values.BASE_URL + "users/resetPassword", this.user, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.trace(res);
                            this.isLoading = false;
                            Toast.makeText("Password changed successfully", "long").show();
                            localStorage.removeItem("userId");
                            localstorage.removeItem("profilePic");
                            localstorage.removeItem("userType");
                            localstorage.removeItem("userName");
                            this.routerExtensions.navigate(['/login']);
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log("ERROR:::::", error);
                    alert(error.error.error);
                });
        }
    }

    onForgotPassword() {
        this.router.navigate(['/forgotPassword']);
    }

    onBack() {
        this.routerExtensions.back();
    }

}