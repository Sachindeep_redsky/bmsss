import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { Values } from "~/app/values/values";
import { ModalComponent } from "~/app/modals/modal.component";
import * as Toast from 'nativescript-toast';
import { PhotoViewer, PhotoViewerOptions, PaletteType, NYTPhotoItem } from "nativescript-photoviewer";
import { Page } from "tns-core-modules/ui/page/page";
import * as localstorage from "nativescript-localstorage";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-addImages",
    moduleId: module.id,
    templateUrl: "./add-images.component.html",
    styleUrls: ['./add-images.component.css'],
})

export class AddImagesComponent implements OnInit {
    @ViewChild('deleteImageDialog', { static: false }) deleteImageDialog: ModalComponent;

    images;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    folderId: string;
    imageId: number;
    photoViewer: PhotoViewer;
    profilePic: string;
    token: string;
    headers: HttpHeaders;
    galleryMessage: string;
    isData: boolean;

    constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.images = [];
        this.year =  Values.SESSION;
        this.folderId = "";
        this.imageId = 0;
        this.photoViewer = new PhotoViewer();
        this.profilePic = "res://profile";
        this.galleryMessage = "";
        this.isData = false;
        this.userService.activeScreen("addImages");

        this.route.queryParams.subscribe(params => {
            this.folderId = params["folderId"];
        });
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        this.getImages();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        // this.router.navigate(['/addImagesFolder']);
        this.routerExtensions.back();
    }

    onDeleteDialogLoaded(args) {
        var deleteDialog = <any>args.object;
        setTimeout(() => {
            if (deleteDialog.android) {
                let nativeImageView = deleteDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (deleteDialog.ios) {
                let nativeImageView = deleteDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    getImages() {
        this.userService.showLoadingState(true);
        this.http
            .get(Values.BASE_URL + "files?id=" + this.folderId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.images = [];
                        this.isData = false;
                        if (res.data.length > 0) {
                            for (var i = 0; i < res.data.length; i++) {
                                this.images.push({
                                    id: res.data[i].id,
                                    icon: res.data[i].image_url
                                });
                            }
                        }
                        else {
                            this.isData = true;
                            this.galleryMessage = "There is no images.";
                        }
                        this.userService.showLoadingState(false);
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    onImagePress(id: number) {
        this.imageId = id;
        this.deleteImageDialog.show();
    }

    onYes() {
        this.userService.showLoadingState(true);
        this.http
            .delete(Values.BASE_URL + "events/files/delete/" + this.imageId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        Toast.makeText("Image deleted successfully.", "long").show();
                        this.userService.showLoadingState(false);
                        this.deleteImageDialog.hide();
                        this.getImages();
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    onNo() {
        this.deleteImageDialog.hide();
    }

    onOutsideClick() {
        this.deleteImageDialog.hide();
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onImageClick(icon: string) {
        let photoviewerOptions: PhotoViewerOptions = {
            startIndex: 0,
            android: {
                paletteType: PaletteType.DarkVibrant,
                showAlbum: false
            }
        };
        let eventImage = [icon];
        this.photoViewer.showGallery(eventImage, photoviewerOptions);
    }

    onAddImage() {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "from": "addImages",
                "folderId": this.folderId
            }
        }
        this.router.navigate(['/uploadImage'], navigationExtras)
    }
}