import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Messages } from "~/app/models/messages.model";
import * as Toast from 'nativescript-toast';
import { Page } from "tns-core-modules/ui/page/page";
import { ModalComponent } from '~/app/modals/modal.component';

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-messageCompose",
    moduleId: module.id,
    templateUrl: "./message-compose.component.html",
    styleUrls: ['./message-compose.component.css'],
})

export class MessageComposeComponent implements OnInit {

    @ViewChild('selectTeacherDialog', { static: false }) selectTeacherDialog: ModalComponent;
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;
    @ViewChild('selectSectionDialog', { static: false }) selectSectionDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    year: string;
    messageHint: string;
    messageBorderColor: string;
    message: string;
    userType: string;
    alertMessage: string;
    messages: Messages;
    classId: string;
    section: string;
    messageId: string;
    profilePic: string;
    answer: string;
    token: any;
    headers: HttpHeaders;
    teacherButton: string;
    teachers;
    isLoading: boolean;
    askedTo: string;
    isTeacherButton: boolean;
    classButton: string;
    sectionButton: string;
    isAdmin: boolean;
    isClassPicker: boolean;
    isSectionPicker: boolean;
    allClasses;
    index: number;
    sections;
    isSelectedSection: boolean;
    selectSectionAllButton: string;
    isSelectedClass: boolean;
    selectClassAllButton: string;
    selectedClasses;
    constructor(private router: Router, private userService: UserService, private http: HttpClient, private routerExtensions: RouterExtensions, private route: ActivatedRoute, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.messageBorderColor = "black";
        this.message = "";
        this.userType = "";
        this.alertMessage = "";
        this.messages = new Messages();
        this.classId = "";
        this.section = "";
        this.profilePic = "res://profile";
        this.userService.showMessageCompose(false);
        this.userService.activeScreen("messageCompose");
        this.teacherButton = "Select Teacher";
        this.teachers = [];
        this.isLoading = false;
        this.askedTo = "";
        this.classButton = "Select  class";
        this.sectionButton = "Select section";
        this.isAdmin = false;
        this.isClassPicker = true;
        this.isSectionPicker = false;
        this.allClasses = [];
        this.index = 0;
        this.isSelectedSection = true;
        this.selectSectionAllButton = "Unselect all";
        this.isSelectedClass = false;
        this.selectClassAllButton = "Select all";
        this.selectedClasses = [];
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            if (localstorage.getItem("userType") == "student") {
                this.isTeacherButton = true;
                this.userType = "student";
                this.messageHint = "Type your question";
                this.alertMessage = "Please enter question";
                this.getClassSection();
            }
            else if (localstorage.getItem("userType") == "admin") {
                this.userType = "admin";
                this.isAdmin = true;
                this.messageHint = "Type your message";
                this.alertMessage = "Please enter message";
            }
            else {
                this.isTeacherButton = false;
                this.userType = "teacher";
                this.messageHint = "Type your answer";
                this.alertMessage = "Please enter answer";
                this.route.queryParams.subscribe(params => {
                    this.messageId = params["messageId"];
                    if (params["answer"] != null && params["answer"] != undefined) {
                        this.answer = params["answer"];
                    }
                })
            }
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    getClassSection() {
        this.userService.showLoadingState(true);
        this.http
            .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"), {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.classId = res.data.classId;
                        this.section = res.data.classSection;
                        this.userService.showLoadingState(false);
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    onTeacherOutsideClick() {
        this.selectTeacherDialog.hide();
    }

    onSelectTeacher() {
        this.selectTeacherDialog.show();
        this.getTeachers();
    }

    getTeachers() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + `users/${localstorage.getItem("userId")}`, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.log("TEACHERS RESSSS::::", res);
                        if (res.data.classTeacher.length > 0) {
                            for (var i = 0; i < res.data.classTeacher.length; i++) {
                                this.teachers.push({
                                    id: res.data.classTeacher[i].id,
                                    name: res.data.classTeacher[i].name
                                })
                            }
                            this.isLoading = false;
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onTeacherButton(item: any) {
        this.askedTo = item.id;
        this.teacherButton = item.name;
    }

    onBack() {
        // this.router.navigate(['/messages']);
        this.routerExtensions.back();
    }

    onMessageTextChanged(args) {
        this.message = args.object.text;
        this.messageBorderColor = "black";
        if (this.message == "") {
            this.messageBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onGridLoaded(args) {
        var grid = <any>args.object;
        setTimeout(() => {
            if (grid.android) {
                let nativeImageView = grid.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                nativeImageView.setElevation(10)
            } else if (grid.ios) {
                let nativeImageView = grid.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onTeacherDialogLoaded(args) {
        var teacherDialog = <any>args.object;
        setTimeout(() => {
            if (teacherDialog.android) {
                let nativeImageView = teacherDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (teacherDialog.ios) {
                let nativeImageView = teacherDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSectionDialogLoaded(args) {
        var sectionDialog = <any>args.object;
        setTimeout(() => {
            if (sectionDialog.android) {
                let nativeImageView = sectionDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (sectionDialog.ios) {
                let nativeImageView = sectionDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSendMessage() {
        if (this.message == "" || this.message == undefined) {
            alert(this.alertMessage);
        }
        else {
            if (this.userType == "student") {
                if (this.askedTo == "") {
                    alert("Please select class");
                } else {
                    this.userService.showLoadingState(true);
                    this.messages.userType = this.userType;
                    this.messages.askedBy = localstorage.getItem("userId");
                    this.messages.askedTo = this.askedTo;
                    this.messages.question = this.message;
                    this.messages.classId = this.classId;
                    this.messages.classSection = this.section;
                    this.http.post(Values.BASE_URL + "msges", this.messages, {
                        headers: this.headers
                    })
                        .subscribe((res: any) => {
                            if (res != null && res != undefined) {
                                if (res.isSuccess == true) {
                                    this.userService.showLoadingState(false);
                                    Toast.makeText("Message sent successfully", "long").show();
                                    // this.router.navigate(['/messages']);
                                    this.routerExtensions.back();
                                }
                            }
                        }, error => {
                            this.userService.showLoadingState(false);
                            alert(error.error.error);
                        });
                }
            }
            else if (this.userType == "admin") {
                if (this.selectedClasses.length == 0) {
                    alert("Please select class");
                } else {
                    this.messages.userType = this.userType;
                    this.messages.question = this.message;
                    this.messages.classes = this.selectedClasses;
                    console.log(this.messages);
                    this.http.post(Values.BASE_URL + "msges", this.messages, {
                        headers: this.headers
                    })
                        .subscribe((res: any) => {
                            if (res != null && res != undefined) {
                                if (res.isSuccess == true) {
                                    this.userService.showLoadingState(false);
                                    Toast.makeText("Message sent successfully", "long").show();
                                    // this.router.navigate(['/messages']);
                                    this.routerExtensions.back();
                                }
                            }
                        }, error => {
                            this.userService.showLoadingState(false);
                            alert(error.error.error);
                        });
                }
            }
            else {
                this.userService.showLoadingState(true);
                this.messages.id = this.messageId;
                this.messages.repliedBy = localstorage.getItem("userId");
                this.messages.answer = this.message;
                this.http.post(Values.BASE_URL + "msges/sendMsg", this.messages, {
                    headers: this.headers
                })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.userService.showLoadingState(false);
                                this.router.navigate(['/messages']);
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        alert(error.error.error);
                    });
            }

            // let navigationExtras: NavigationExtras = {
            //     queryParams: {
            //         "message": this.message
            //     }
            // }
            // this.router.navigate(['./messages'], navigationExtras);
        }
    }

    getClasses() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + `classes`, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        if (res.data.length > 0) {
                            this.allClasses = [];
                            for (var i = 0; i < res.data.length; i++) {
                                if (res.data[i].status == "active") {
                                    this.allClasses.push({
                                        id: res.data[i].id,
                                        isSelected: false,
                                        name: res.data[i].name,
                                        section: res.data[i].section
                                    })
                                }
                            }
                            this.selectClassDialog.show();
                            this.isLoading = false;
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onClassButton(item: any, index: any) {
        this.index = index;
        if (item.isSelected == false) {
            this.allClasses[index].isSelected = true;
            // this.section = this.classes[index].section;
            this.sections = [];
            for (var i = 0; i < this.allClasses[index].section.length; i++) {
                this.sections.push({
                    isSelected: true,
                    classSection: this.allClasses[index].section[i].classSection
                })
            }
            this.selectSectionAllButton = "Unselect all";
            this.isSelectedSection = true;
            this.selectSectionDialog.show();
        }
        else {
            this.sections = [];
            this.allClasses[index].isSelected = false;
        }
    }

    onSectionButton(item: any, index: any) {
        if (item.isSelected == false) {
            this.sections[index].isSelected = true;
        } else {
            this.sections[index].isSelected = false;
        }
    }

    onClass() {
        this.getClasses();
    }

    onSelectSectionAll() {
        if (this.isSelectedSection == false) {
            this.isSelectedSection = true;
            this.selectSectionAllButton = "Unselect all";
            for (var i = 0; this.sections.length; i++) {
                this.sections[i].isSelected = true;
            }
        }
        else {
            this.isSelectedSection = false;
            this.selectSectionAllButton = "Select all";
            for (var i = 0; this.sections.length; i++) {
                this.sections[i].isSelected = false;
            }
        }
    }

    onSelectClassAll() {
        if (this.isSelectedClass == false) {
            this.isSelectedClass = true;
            this.selectClassAllButton = "Unselect all";
            for (var i = 0; this.allClasses.length; i++) {
                this.allClasses[i].isSelected = true;
            }
        } else {
            this.isSelectedClass = false;
            this.selectClassAllButton = "Select all";
            for (var i = 0; this.allClasses.length; i++) {
                this.allClasses[i].isSelected = false;
            }
        }
    }

    onSectionOk() {
        this.allClasses[this.index].section = this.sections;
        this.selectSectionDialog.hide();
    }

    onClassOk() {
        this.selectClassDialog.hide();
        this.selectedClasses = [];
        for (var i = 0; i < this.allClasses.length; i++) {
            if (this.allClasses[i].isSelected == true) {
                if (this.allClasses[i].section.length > 0) {
                    var isSection = false;
                    for (var l = 0; l < this.allClasses[i].section.length; l++) {
                        if (this.allClasses[i].section[l].isSelected != undefined && this.allClasses[i].section[l].isSelected == true) {
                            isSection = true;
                        }
                    }
                    if (isSection == true) {
                        this.selectedClasses.push({
                            classId: this.allClasses[i].id,
                            sections: []
                        })
                        for (var j = 0; j < this.allClasses[i].section.length; j++) {
                            if (this.allClasses[i].section[j].isSelected != undefined && this.allClasses[i].section[j].isSelected == true) {
                                this.selectedClasses[this.selectedClasses.length - 1].sections.push({
                                    classSection: this.allClasses[i].section[j].classSection
                                })
                            }
                        }
                    }
                }
            }
        }
        console.log("SELECTED CLASSES", this.selectedClasses);
    }
}