import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AddImagesFolderComponent } from "./components/add-images-folder.component";

const routes: Routes = [
    { path: "", component: AddImagesFolderComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AddImagesFolderRoutingModule { }
