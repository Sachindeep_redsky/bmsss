import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from "~/app/services/user.service";
import { ModalComponent } from "~/app/modals/modal.component";
import * as Toast from 'nativescript-toast';
import { Page } from "tns-core-modules/ui/page/page";
import * as localstorage from "nativescript-localstorage";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-addImagesFolder",
    moduleId: module.id,
    templateUrl: "./add-images-folder.component.html",
    styleUrls: ['./add-images-folder.component.css'],
})

export class AddImagesFolderComponent implements OnInit {
    @ViewChild('deleteFolderDialog', { static: false }) deleteFolderDialog: ModalComponent;

    folders;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    folderId: number;
    profilePic: string;
    token: string;
    headers: HttpHeaders;
    isLoading: boolean;
    galleryMessage: string;
    isData: boolean;

    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.folders = [];
        this.year =  Values.SESSION;
        this.folderId = 0;
        this.profilePic = "res://profile";
        this.isLoading = false;
        this.galleryMessage = "";
        this.isData = false;
        this.userService.activeScreen("addImagesFolder");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        this.getFolders();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        this.routerExtensions.back();
    }

    getFolders() {
        this.isLoading = true;
        // this.userService.showLoadingState(true);
        this.http
            .get(Values.BASE_URL + "events", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                console.log(res);
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.folders = [];
                        if (res.data.length > 0) {
                            this.isData = false;
                            for (var i = 0; i < res.data.length; i++) {
                                this.folders.push({
                                    id: res.data[i].id,
                                    name: res.data[i].name,
                                    icon: res.data[i].image_url
                                });
                            }
                        }
                        else {
                            this.isData = true;
                            this.galleryMessage = "There is no images.";
                        }
                        // this.userService.showLoadingState(false);
                        this.isLoading = false;
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onDeleteDialogLoaded(args) {
        var deleteDialog = <any>args.object;
        setTimeout(() => {
            if (deleteDialog.android) {
                let nativeImageView = deleteDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (deleteDialog.ios) {
                let nativeImageView = deleteDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onFolderPress(id: number) {
        this.folderId = id;
        this.deleteFolderDialog.show();
    }

    onDelete() {
        this.isLoading = true;
        // this.userService.showLoadingState(true);
        this.http
            .delete(Values.BASE_URL + "events/delete/" + this.folderId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        Toast.makeText("Folder deleted successfully.", "long").show();
                        // this.userService.showLoadingState(false);
                        this.isLoading = false;
                        this.deleteFolderDialog.hide();
                        this.getFolders();
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onUpdate() {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "folderId": this.folderId,
                "from": "addImagesFolder",
                "type": "edit"
            }
        }
        this.router.navigate(['/uploadImage'], navigationExtras);
    }

    onCancel() {
        this.deleteFolderDialog.hide();
    }

    onOutsideClick() {
        this.deleteFolderDialog.hide();
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onFolderClick(id: number) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "folderId": id
            }
        }
        this.router.navigate(['/addImages'], navigationExtras);
    }

    onAddImageFolder() {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "from": "addImagesFolder"
            }
        }
        this.router.navigate(['/uploadImage'], navigationExtras)
    }
}