import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AppliedLeaveComponent } from "./components/applied-leave.component";

const routes: Routes = [
    { path: "", component: AppliedLeaveComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppliedLeaveRoutingModule { }
