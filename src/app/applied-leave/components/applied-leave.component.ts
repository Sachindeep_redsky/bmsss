import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, ActivatedRoute } from "@angular/router";
import * as localstorage from "nativescript-localstorage";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { Page } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-appliedLeave",
    moduleId: module.id,
    templateUrl: "./applied-leave.component.html",
    styleUrls: ['./applied-leave.component.css'],
})

export class AppliedLeaveComponent implements OnInit {

    renderViewTimeout;
    isRendering: boolean;
    year: string;
    reason: string;
    otp: string;
    userId: string;
    // from: string;
    profilePic: string;
    isLoading: boolean;
    headers: HttpHeaders;
    token: string;
    status: string;
    isHalfDayLeave: boolean;
    leaveType: string;
    showHalfDayLeave: boolean;
    fullDayLeaves;

    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private http: HttpClient, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.otp = "123456";
        this.reason = "";
        this.otp = "";
        // this.from = "";
        this.profilePic = "res://profile";
        this.isLoading = false;
        this.token = "";
        this.userId = "";
        this.status = "";
        this.isHalfDayLeave = true;
        this.leaveType = "halfDay";
        this.showHalfDayLeave = false;
        this.fullDayLeaves = [];
        // this.route.queryParams.subscribe(params => {
        //     if (params["from"] != undefined && params["from"] != null) {
        //         this.from = params["from"];
        //     }
        // });
        this.userService.activeScreen("appliedLeave");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            if (localstorage.getItem("userId") != null && localstorage.getItem("userId") != undefined) {
                this.userId = localstorage.getItem("userId");
                this.getLeave();
            }
        }

    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onGridLoaded(args) {
        var grid = <any>args.object;

        setTimeout(() => {
            if (grid.android) {
                let nativeImageView = grid.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                nativeImageView.setElevation(10)
            } else if (grid.ios) {
                let nativeImageView = grid.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onBack() {
        // this.routerExtensions.back();
        // this.router.navigate(['./homeStudent']);
        this.routerExtensions.back();
        // this.userService.showLoadingState(true);
        // this.http
        //     .get(Values.BASE_URL + "leaves/" + localstorage.getItem("userId"))
        //     .subscribe((res: any) => {
        //         if (res != null && res != undefined) {
        //             if (res.isSuccess == true) {
        //                 this.userService.showLoadingState(false);
        //                 this.router.navigate(['./homeStudent']);
        //             }
        //         }
        //         else {
        //             this.userService.showLoadingState(false);
        //             this.router.navigate(['./applyLeave']);
        //         }
        //     }, error => {
        //         if (error.error.isSuccess == false) {
        //             this.router.navigate(['./applyLeave']);
        //         }
        //         this.userService.showLoadingState(false);
        //         console.log(error.error.error);
        //     });
    }

    getLeave() {
        this.isLoading = true;
        if (this.isHalfDayLeave == true) {
            this.http
                .get(Values.BASE_URL + "leaves?userId=" + this.userId + "&leaveType=halfDay", {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("EAVEEE::::", res);
                            this.isLoading = false;
                            if (res.data.length > 0) {
                                this.showHalfDayLeave = true;
                                this.reason = res.data[0].reason;
                                this.status = res.data[0].status;
                                this.otp = res.data[0].otp;
                            }
                            console.log("STATUS::::", this.status);
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
        else {
            this.http
                .get(Values.BASE_URL + "leaves?userId=" + this.userId + "&leaveType=fullDay", {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("EAVEEEEEEE::::", res);
                            // this.userService.showLoadingState(false);
                            this.isLoading = false;
                            if (res.data.length > 0) {
                                this.showHalfDayLeave = false;
                                this.fullDayLeaves = res.data;
                            }
                        }
                    }
                }, error => {
                    // this.userService.showLoadingState(false);
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    onHalfDayLeave() {
        this.isHalfDayLeave = true;
        this.getLeave();
    }

    onFullDayLeave() {
        this.isHalfDayLeave = false;
        this.showHalfDayLeave = false;
        this.getLeave();
    }
}