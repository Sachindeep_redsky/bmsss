import { Holidays } from './../../models/holidays.model';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { ModalComponent } from "~/app/modals/modal.component";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { Homework } from "~/app/models/homework.model";
import * as localstorage from "nativescript-localstorage";
import * as Toast from 'nativescript-toast';
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { DatePicker } from "tns-core-modules/ui/date-picker";

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-addHolidays",
    moduleId: module.id,
    templateUrl: "./add-holidays.component.html",
    styleUrls: ['./add-holidays.component.css'],
})

export class AddHolidaysComponent implements OnInit {
    @ViewChild('datePickerDialog', { static: false }) datePickerDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    date: string;
    year: string;
    sessionYear: string;
    day: string;
    month: string;
    holidayHint: string;
    holidayBorderColor: string;
    holidayReason: string;
    type: string;
    token: string;
    headers: HttpHeaders;
    isLoading: boolean;
    isSingleHoliday: boolean;
    fromDate: string;
    toDate: string;
    singleDate: string;
    // singleDate: string;
    from: string;
    // isNormalHoliday: boolean;
    holidays: Holidays;
    isEvent: string;
    profilePic: string;
    constructor(private page: Page, private routerExtensions: RouterExtensions, private userService: UserService, private route: ActivatedRoute, private http: HttpClient) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.sessionYear =  Values.SESSION;
        this.day = date.getDate().toString();
        this.month = (date.getMonth() + 1).toString();
        this.year = date.getFullYear().toString();
        this.holidayHint = "Enter Holiday Reason";
        this.holidayBorderColor = "black";
        this.holidayReason = "";
        this.userService.showMessageCompose(false);
        this.userService.activeScreen("addHolidays");
        this.type = "";
        this.token = "";
        this.isLoading = false;
        this.isSingleHoliday = true;
        this.fromDate = "From Date";
        this.toDate = "To date";
        this.singleDate = "Select Date";
        this.from = "";
        // this.isNormalHoliday = true;
        this.holidays = new Holidays();
        this.date = "";
        this.isEvent = "false";
        this.profilePic = "res://profile";
        // this.route.queryParams.subscribe(params => {
        // if (params["subjectName"] != undefined) {
        //     this.selectSubjectText = params["subjectName"];
        // }
        // if (params["homeworkDetail"] != undefined) {
        //     this.holidayHint = params["homeworkDetail"];
        // }
        // if (params["type"] != undefined) {
        //     this.type = params["type"];
        // }
        // if (params["homeworkId"] != undefined) {
        //     this.homeworkId = params["homeworkId"];
        // }
        // });

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            // this.getClassSection();
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        this.routerExtensions.back();
    }

    onPickerLoaded(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.year = date.getFullYear();
        datePicker.month = date.getMonth() + 1;
        datePicker.day = date.getDate();
        datePicker.minDate = new Date(2020, 3, 1);
        datePicker.maxDate = new Date(2021, 2, 31);
    }

    onDateChanged(args) {

    }

    onDayChanged(args) {
        this.day = args.value;
    }

    onMonthChanged(args) {
        this.month = args.value;
    }

    onYearChanged(args) {
        this.year = args.value;
    }

    onOkDate() {
        this.datePickerDialog.hide();
        this.date = this.day + "/" + this.month + "/" + this.year;
        if (this.from == "single") {
            this.singleDate = this.date;
            this.fromDate = this.date;
            this.toDate = this.date;
        }
        if (this.from == "from") {
            this.fromDate = this.date;
        }
        if (this.from == "to") {
            this.toDate = this.date;
        }
    }


    onSingleDate() {
        this.datePickerDialog.show();
        this.from = "single";
    }

    onFromDate() {
        this.datePickerDialog.show();
        this.from = "from";
    }

    onToDate() {
        this.datePickerDialog.show();
        this.from = "to";
    }

    onHolidayTextChanged(args) {
        this.holidayReason = args.object.text;
        this.holidayBorderColor = "black";
        if (this.holidayReason == "") {
            this.holidayBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onGridLoaded(args) {
        var grid = <any>args.object;
        setTimeout(() => {
            if (grid.android) {
                let nativeImageView = grid.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                nativeImageView.setElevation(5)
            } else if (grid.ios) {
                let nativeImageView = grid.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onDatePickerDialogLoaded(args) {
        var datePickerDialog = <any>args.object;
        setTimeout(() => {
            if (datePickerDialog.android) {
                let nativeImageView = datePickerDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#D9D9D9'));
                nativeImageView.setElevation(10)
            } else if (datePickerDialog.ios) {
                let nativeImageView = datePickerDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSubmitHomework() {
        if (this.holidayReason == "") {
            alert("Please enter homework.");
        }
        else {
            // this.homework.description = this.holidayReason;
            // this.homework.classId = this.classId;
            // this.homework.subjectId = this.subjectId;
            // this.homework.section = this.section;
            // this.userService.showLoadingState(true);
            // if (this.type == "edit") {
            //     this.http
            //         .put(Values.BASE_URL + "homeworks/update/" + this.homeworkId, this.homework, {
            //             headers: this.headers
            //         })
            //         .subscribe((res: any) => {
            //             if (res != null && res != undefined) {
            //                 if (res.isSuccess == true) {
            //                     this.userService.showLoadingState(false);
            //                     Toast.makeText("Homework updated successfully!!!", "long").show();
            //                     this.routerExtensions.navigate(['/homework']);
            //                 }
            //                 else {
            //                     alert(res.message);
            //                     this.userService.showLoadingState(false);
            //                     this.routerExtensions.navigate(['/homework']);
            //                 }
            //             }
            //         }, error => {
            //             this.userService.showLoadingState(false);
            //             alert(error.error.error);
            //         });
            // }
            // else {
            //     this.http
            //         .post(Values.BASE_URL + "homeworks/create", this.homework, {
            //             headers: this.headers
            //         })
            //         .subscribe((res: any) => {
            //             if (res != null && res != undefined) {
            //                 if (res.isSuccess == true) {
            //                     this.userService.showLoadingState(false);
            //                     Toast.makeText("Homework added successfully!!!", "long").show();
            //                     this.routerExtensions.navigate(['/homework']);
            //                 }
            //                 else {
            //                     alert(res.message);
            //                     this.userService.showLoadingState(false);
            //                     this.routerExtensions.navigate(['/homework']);

            //                 }
            //             }
            //         }, error => {
            //             this.userService.showLoadingState(false);
            //             alert(error.error.error);
            //         });
            // }


            // let navigationExtras: NavigationExtras = {
            //     queryParams: {
            //         "message": this.homework
            //     }
            // }
            // this.router.navigate(['./homeworkTeacher'], navigationExtras);
        }
    }

    onSingleHoliday() {
        this.isSingleHoliday = true;
    }

    onMultipleHoliday() {
        this.isSingleHoliday = false;
    }

    // onNormalHoliday() {
    //     this.isNormalHoliday = true;
    //     this.isEvent = "false";
    // }

    // onEventHoliday() {
    //     this.isNormalHoliday = false;
    //     this.isEvent = "true";
    // }

    onSubmitHoliday() {
        if (this.date == "") {
            alert("Please select date.");
        }
        else if (this.holidayReason == "") {
            alert("Please enter holiday reason.");
        }
        else {
            this.isLoading = true;
            this.holidays.year = this.year;
            this.holidays.month = parseInt(this.month);
            this.holidays.fromDate = this.fromDate;
            this.holidays.toDate = this.toDate;
            this.holidays.description = this.holidayReason;
            this.holidays.isEvent = this.isEvent;
            this.http
                .put(Values.BASE_URL + "holidays", this.holidays, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.isLoading = false;
                            Toast.makeText("holiday added successfully", "long").show();
                            this.routerExtensions.back();
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    alert(error.error.error);
                });
        }
    }

}