import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AddHolidaysComponent } from "./components/add-holidays.component";

const routes: Routes = [
    { path: "", component: AddHolidaysComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AddHolidaysRoutingModule { }
