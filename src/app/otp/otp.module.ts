import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { OTPRoutingModule } from "./otp-routing.module";
import { OTPComponent } from "./components/otp.component";


@NgModule({
    imports: [
        HttpModule,
        OTPRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        OTPComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class OTPModule { }
