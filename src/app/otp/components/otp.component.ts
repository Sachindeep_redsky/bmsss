import { Component, ElementRef, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router"
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { UserService } from "~/app/services/user.service";
import { User } from "~/app/models/user.model";
import { Page, Color } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;
@Component({
    selector: "app-otp",
    moduleId: module.id,
    templateUrl: "./otp.component.html",
    styleUrls: ['./otp.component.css'],
})

export class OTPComponent {

    inputStyle = 'inputInactive';
    textfield: TextField;
    otpText: string;
    newPasswordText: string;
    confirmNewPasswordText: string;
    otpIcon: string;
    passwordIcon: string;
    isRendering: boolean;
    renderViewTimeout;
    user: User;
    isLoading: boolean;
    headers: HttpHeaders;
    token: any;

    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.newPasswordText = "";
        this.confirmNewPasswordText = "";
        this.otpText = "";
        this.isRendering = false;
        this.otpIcon = 'res://otp'
        this.passwordIcon = "res://password"
        this.user = new User();
        this.isLoading = false;
        this.userService.activeScreen("otp");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
    }

    ngOnInit(): void {
        this.inputStyle = 'inputInactive';
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
    }
    
    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }


    onGridLoaded(args: any) {
        var gridMain = <any>args.object;
        setTimeout(() => {
            if (gridMain.android) {
                let nativeGridMain = gridMain.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridMain.ios) {
                let nativeGridMain = gridMain.ios;
                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 400)
    }

    onBack() {
        this.routerExtensions.back();
    }

    public otpTextField(args) {
        var textField = <TextField>args.object;
        this.otpText = textField.text;
    }
    public passwordTextField(args) {
        var textField = <TextField>args.object;
        this.newPasswordText = textField.text;
    }
    public rePasswordTextField(args) {
        var textField = <TextField>args.object;
        this.confirmNewPasswordText = textField.text;
    }

    onConfirmClick() {
        if (this.otpText == "") {
            alert("Otp cannot be empty");
            return;
        }
        else if (this.otpText.length < 6) {
            alert("Please enter six digit otp");
            return;
        }
        else if (this.newPasswordText == "") {
            alert("New password cannot be empty");
            return;
        }
        else if (this.confirmNewPasswordText == "") {
            alert("Confirm new password cannot be empty");
            return;
        }
        else if (this.newPasswordText != this.confirmNewPasswordText) {
            alert("New password and confirm new password should be same!!!");
            return;
        }
        else {
            if (localstorage.getItem("otpVerifyToken") != null && localstorage.getItem("otpVerifyToken") != undefined) {
                this.user.otpVerifyToken = localstorage.getItem("otpVerifyToken");
            }
            this.user.otp = this.otpText;
            this.user.newPassword = this.newPasswordText;
            this.isLoading = true;
            this.http
                .post(Values.BASE_URL + "users/otp/verify", this.user, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.http
                                .put(Values.BASE_URL + "users/forgotPassword", this.user, {
                                    headers: this.headers
                                })
                                .subscribe((res: any) => {
                                    if (res != null && res != undefined) {
                                        if (res.isSuccess == true) {
                                            this.isLoading = false;
                                            Toast.makeText("Password set successfully!!!", "long").show();
                                            this.router.navigate(['/login']);
                                        }
                                    }
                                }, error => {
                                    this.isLoading = false;
                                    alert(error.error.error);
                                });
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    alert(error.error.error);
                });
        }
    }
}