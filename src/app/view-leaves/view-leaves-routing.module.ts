import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ViewLeavesComponent } from "./components/view-leaves.component";

const routes: Routes = [
    { path: "", component: ViewLeavesComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ViewLeavesRoutingModule { }
