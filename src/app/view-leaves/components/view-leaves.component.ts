import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { Values } from "~/app/values/values";
import * as localstorage from "nativescript-localstorage";
import { Page } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-viewLeaves",
    moduleId: module.id,
    templateUrl: "./view-leaves.component.html",
    styleUrls: ['./view-leaves.component.css'],
})

export class ViewLeavesComponent implements OnInit {

    halfDayLeaves;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    date: string;
    profilePic: string;
    isLoading: boolean;
    headers: HttpHeaders;
    token: string;
    isHalfDayLeave: boolean;
    fullDayLeaves;
    userType: string;
    classId: string;
    classSection: string;
    constructor(private router: Router, private route: ActivatedRoute, private userService: UserService, private http: HttpClient, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.halfDayLeaves = [];
        this.fullDayLeaves = [];
        this.date = date.getDate().toString() + "/" + (date.getMonth() + 1).toString() + "/" + date.getFullYear().toString();
        this.isLoading = false;
        this.token = "";
        this.userService.activeScreen("viewLeaves");
        this.isHalfDayLeave = true;
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            if (localstorage.getItem("classId") != null && localstorage.getItem("classId") != undefined) {
                this.classId = localstorage.getItem("classId");
            }
            if (localstorage.getItem("classSection") != null && localstorage.getItem("classSection") != undefined) {
                this.classSection = localstorage.getItem("classSection");
            }
            if (localstorage.getItem("userType") == "teacher" && localstorage.getItem("classIncharge") == "true") {
                this.userType = "classIncharge";
                this.isHalfDayLeave = false;
                this.getFullDayLeaves();
            }
            else {
                this.getHalfDayLeaves();
            }
        }
        this.profilePic = "res://profilePic";
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        this.router.navigate(['/homeAdmin']);
    }

    getHalfDayLeaves() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "leaves?leaveType=halfDay", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.log(res);
                        this.halfDayLeaves = [];
                        for (var i = 0; i < res.data.length; i++) {
                            this.halfDayLeaves.push({
                                id: res.data[i].id,
                                name: res.data[i].name,
                                class: res.data[i].class,
                                section: res.data[i].section,
                                rollNo: res.data[i].rollNo,
                                status: res.data[i].status,
                                reason: res.data[i].reason,
                                leaveToken: res.data[i].leaveToken
                            });
                        }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    getFullDayLeaves() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "leaves?leaveType=fullDay", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.fullDayLeaves = [];
                        console.log("res");
                        for (var i = 0; i < res.data.length; i++) {
                            var fromDate = res.data[i].fromDate;
                            var toDate = res.data[i].toDate;
                            var fromYear = fromDate.split("/")[2];
                            var fromMonth = fromDate.split("/")[1];
                            var fromDay = fromDate.split("/")[0];
                            var toYear = toDate.split("/")[2];
                            var toMonth = toDate.split("/")[1];
                            var toDay = toDate.split("/")[0];
                            if (localstorage.getItem("userType") == "teacher" && localstorage.getItem("classIncharge") == "true") {
                                if (fromYear == toYear) {
                                    if (fromMonth == toMonth) {
                                        if (toDay - fromDay <= 2) {
                                            if (res.data[i].classId == this.classId && res.data[i].section == this.classSection) {
                                                this.fullDayLeaves.push({
                                                    id: res.data[i].id,
                                                    name: res.data[i].name,
                                                    class: res.data[i].class,
                                                    section: res.data[i].section,
                                                    rollNo: res.data[i].rollNo,
                                                    status: res.data[i].status,
                                                    reason: res.data[i].reason,
                                                    leaveToken: res.data[i].leaveToken,
                                                    fromDate: res.data[i].fromDate,
                                                    toDate: res.data[i].toDate,
                                                    fullDayType: res.data[i].fullDayType,
                                                    rejectionReason: res.data[i].rejectionReason
                                                });
                                            }
                                        }
                                    }
                                    else {
                                        var fromMonthDays: number;
                                        if (fromMonth == 4 || fromMonth == 6 || fromMonth == 9 || fromMonth == 11) {
                                            fromMonthDays = 30;
                                        } else {
                                            if (fromMonth == 2) {
                                                fromMonthDays = (fromYear) ? 29 : 28;
                                            }
                                            else {
                                                fromMonthDays = 31;
                                            }
                                        }
                                        if ((parseInt(((fromMonthDays - fromDay) + 1).toString()) + parseInt(toDay)) <= 2) {
                                            if (res.data[i].classId == this.classId && res.data[i].section == this.classSection) {
                                                this.fullDayLeaves.push({
                                                    id: res.data[i].id,
                                                    name: res.data[i].name,
                                                    class: res.data[i].class,
                                                    section: res.data[i].section,
                                                    rollNo: res.data[i].rollNo,
                                                    status: res.data[i].status,
                                                    reason: res.data[i].reason,
                                                    leaveToken: res.data[i].leaveToken,
                                                    fromDate: res.data[i].fromDate,
                                                    toDate: res.data[i].toDate,
                                                    fullDayType: res.data[i].fullDayType,
                                                    rejectionReason: res.data[i].rejectionReason
                                                });
                                            }
                                        }
                                    }
                                }
                                else {
                                    var fromMonthDays: number;
                                    if (fromMonth == 4 || fromMonth == 6 || fromMonth == 9 || fromMonth == 11) {
                                        fromMonthDays = 30;
                                    } else {
                                        if (fromMonth == 2) {
                                            fromMonthDays = (fromYear) ? 29 : 28;
                                        }
                                        else {
                                            fromMonthDays = 31;
                                        }
                                    }
                                    if ((parseInt(((fromMonthDays - fromDay) + 1).toString()) + parseInt(toDay)) <= 2) {
                                        if (res.data[i].classId == this.classId && res.data[i].section == this.classSection) {
                                            this.fullDayLeaves.push({
                                                id: res.data[i].id,
                                                name: res.data[i].name,
                                                class: res.data[i].class,
                                                section: res.data[i].section,
                                                rollNo: res.data[i].rollNo,
                                                status: res.data[i].status,
                                                reason: res.data[i].reason,
                                                leaveToken: res.data[i].leaveToken,
                                                fromDate: res.data[i].fromDate,
                                                toDate: res.data[i].toDate,
                                                fullDayType: res.data[i].fullDayType,
                                                rejectionReason: res.data[i].rejectionReason
                                            });
                                        }
                                    }
                                }
                            }
                            else {
                                if (fromYear == toYear) {
                                    if (fromMonth == toMonth) {
                                        if (toDay - fromDay > 2) {
                                            this.fullDayLeaves.push({
                                                id: res.data[i].id,
                                                name: res.data[i].name,
                                                class: res.data[i].class,
                                                section: res.data[i].section,
                                                rollNo: res.data[i].rollNo,
                                                status: res.data[i].status,
                                                reason: res.data[i].reason,
                                                leaveToken: res.data[i].leaveToken,
                                                fromDate: res.data[i].fromDate,
                                                toDate: res.data[i].toDate,
                                                fullDayType: res.data[i].fullDayType,
                                                rejectionReason: res.data[i].rejectionReason
                                            });
                                        }
                                    }
                                    else {
                                        var fromMonthDays: number;
                                        if (fromMonth == 4 || fromMonth == 6 || fromMonth == 9 || fromMonth == 11) {
                                            fromMonthDays = 30;
                                        } else {
                                            if (fromMonth == 2) {
                                                fromMonthDays = (fromYear) ? 29 : 28;
                                            }
                                            else {
                                                fromMonthDays = 31;
                                            }
                                        }
                                        if ((parseInt(((fromMonthDays - fromDay) + 1).toString()) + parseInt(toDay)) > 2) {
                                            this.fullDayLeaves.push({
                                                id: res.data[i].id,
                                                name: res.data[i].name,
                                                class: res.data[i].class,
                                                section: res.data[i].section,
                                                rollNo: res.data[i].rollNo,
                                                status: res.data[i].status,
                                                reason: res.data[i].reason,
                                                leaveToken: res.data[i].leaveToken,
                                                fromDate: res.data[i].fromDate,
                                                toDate: res.data[i].toDate,
                                                fullDayType: res.data[i].fullDayType,
                                                rejectionReason: res.data[i].rejectionReason
                                            });
                                        }
                                    }
                                }
                                else {
                                    var fromMonthDays: number;
                                    if (fromMonth == 4 || fromMonth == 6 || fromMonth == 9 || fromMonth == 11) {
                                        fromMonthDays = 30;
                                    } else {
                                        if (fromMonth == 2) {
                                            fromMonthDays = (fromYear) ? 29 : 28;
                                        }
                                        else {
                                            fromMonthDays = 31;
                                        }
                                    }
                                    if ((parseInt(((fromMonthDays - fromDay) + 1).toString()) + parseInt(toDay)) > 2) {
                                        this.fullDayLeaves.push({
                                            id: res.data[i].id,
                                            name: res.data[i].name,
                                            class: res.data[i].class,
                                            section: res.data[i].section,
                                            rollNo: res.data[i].rollNo,
                                            status: res.data[i].status,
                                            reason: res.data[i].reason,
                                            leaveToken: res.data[i].leaveToken,
                                            fromDate: res.data[i].fromDate,
                                            toDate: res.data[i].toDate,
                                            fullDayType: res.data[i].fullDayType,
                                            rejectionReason: res.data[i].rejectionReason
                                        });
                                    }
                                }
                            }
                        }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onHalfDayLeaveClick(item: any) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "name": item.name,
                "class": item.class,
                "section": item.section,
                "rollNo": item.rollNo,
                "reason": item.reason,
                "leaveToken": item.leaveToken,
                "status": item.status,
                "from": "halfDayLeave",
            }
        }
        this.router.navigate(['/viewLeave'], navigationExtras);
    }

    onFullDayLeaveClick(item: any) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "leaveId": item.id,
                "name": item.name,
                "class": item.class,
                "section": item.section,
                "rollNo": item.rollNo,
                "reason": item.reason,
                "status": item.status,
                "fromDate": item.fromDate,
                "toDate": item.toDate,
                "fullDayType": item.fullDayType,
                "from": "fullDayLeave",
                "rejectionReason": item.rejectionReason
            }
        }
        this.router.navigate(['/viewLeave'], navigationExtras);
    }

    onHalfDayLeave() {
        this.isHalfDayLeave = true;
        this.getHalfDayLeaves();
    }
    onFullDayLeave() {
        this.isHalfDayLeave = false;
        this.getFullDayLeaves();
    }
}