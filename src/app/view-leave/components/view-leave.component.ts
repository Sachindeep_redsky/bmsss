import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { UserService } from "~/app/services/user.service";
import { Leave } from "~/app/models/leave.model";
import { ModalComponent } from "~/app/modals/modal.component";
import * as Toast from 'nativescript-toast';
import { Page } from "tns-core-modules/ui/page/page";
import * as localstorage from "nativescript-localstorage";

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-viewLeave",
    moduleId: module.id,
    templateUrl: "./view-leave.component.html",
    styleUrls: ['./view-leave.component.css'],
})

export class ViewLeaveComponent implements OnInit {

    @ViewChild('verifyOtpDialog', { static: false }) verifyOtpDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    date: string;
    name: string;
    class: string;
    section: string;
    reason: string;
    otp: string;
    leaveId: string;
    rollNo: string;
    leave: Leave;
    leaveToken: string;
    otpText: string;
    otpBorderColor: string;
    otpHint: string;
    rejectionReasonText: string;
    rejectionReasonBorderColor: string;
    rejectionReasonHint: string;
    dialogOkButton: string;
    dialogHideButton: string;
    isConfirmReject: boolean;
    status: string;
    profilePic: string;
    isLoading: boolean;
    headers: HttpHeaders;
    token: any;
    fullDayType: string;
    fromDate: string;
    toDate: string;
    from: string;
    showConfirmMsg: boolean;
    confirmMsg: string;
    isRejectionReason: boolean;
    rejectionReason: string;
    constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.date = date.getDate().toString() + "/" + (date.getMonth() + 1).toString() + "/" + date.getFullYear().toString();
        this.name = "";
        this.class = "";
        this.section = "";
        this.reason = "";
        this.otp = "";
        this.leaveId = "";
        this.rollNo = "";
        this.leaveToken = "";
        this.leave = new Leave();
        this.otpText = "";
        this.otpBorderColor = "black";
        this.otpHint = "Enter leave OTP";
        this.rejectionReasonText = "";
        this.rejectionReasonBorderColor = "black";
        this.rejectionReasonHint = "Enter leave rejection reason";
        this.dialogOkButton = "OK";
        this.dialogHideButton = "Hide";
        this.isConfirmReject = false;
        this.status = "";
        this.profilePic = "res://profile";
        this.isLoading = false;
        this.fullDayType = "";
        this.fromDate = "";
        this.toDate = "";
        this.from = "";
        this.showConfirmMsg = false;
        this.confirmMsg = "Are you sure you want to confirm this leave?";
        this.isRejectionReason = false;
        this.rejectionReason = "";
        this.userService.activeScreen("viewLeave");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        this.route.queryParams.subscribe(params => {
            if (params["name"] != null && params["name"] != undefined) {
                this.name = params["name"];
            }
            if (params["class"] != null && params["class"] != undefined) {
                this.class = params["class"];
            }
            if (params["section"] != null && params["section"] != undefined) {
                this.section = params["section"];
            }
            if (params["rollNo"] != null && params["rollNo"] != undefined) {
                this.rollNo = params["rollNo"];
            }
            if (params["reason"] != null && params["reason"] != undefined) {
                this.reason = params["reason"];
            }
            if (params["leaveToken"] != null && params["leaveToken"] != undefined) {
                this.leaveToken = params["leaveToken"];
            }
            if (params["from"] != null && params["from"] != undefined) {
                this.from = params["from"];
            }
            if (params["status"] != null && params["status"] != undefined) {
                this.status = params["status"];
                console.log(this.status);
                if (this.status == "rejected" && this.from == "fullDayLeave") {
                    this.isRejectionReason = true;
                }
                if (params["status"] == "pending") {
                    this.isConfirmReject = false;
                }
                else {
                    this.isConfirmReject = true;
                }
            }
            if (params["fullDayType"] != null && params["fullDayType"] != undefined) {
                this.fullDayType = params["fullDayType"];
            }
            if (params["fromDate"] != null && params["fromDate"] != undefined) {
                this.fromDate = params["fromDate"];
            }
            if (params["toDate"] != null && params["toDate"] != undefined) {
                this.toDate = params["toDate"];
            }
            if (params["leaveId"] != null && params["leaveId"] != undefined) {
                this.leaveId = params["leaveId"];
            }
            if (params["rejectionReason"] != null && params["rejectionReason"] != undefined) {
                this.rejectionReason = params["rejectionReason"];
            }
        });
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000);
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onOtpTextChanged(args) {
        this.otpText = args.object.text;
        this.otpBorderColor = "white";
        if (this.otpText == "") {
            this.otpBorderColor = "black";
        }
    }

    onRejectionReasonTextChanged(args) {
        this.rejectionReasonText = args.object.text;
        this.rejectionReasonBorderColor = "white";
        if (this.rejectionReasonText == "") {
            this.rejectionReasonBorderColor = "black";
        }
    }

    onBack() {
        this.routerExtensions.back();
    }

    onConfirm() {
        if (this.from == "fullDayLeave") {
            this.showConfirmMsg = true;
        }
        this.verifyOtpDialog.show();
        this.isLoading = false;
        this.leave.status = "confirmed";
    }

    onReject() {
        this.showConfirmMsg = false;
        this.verifyOtpDialog.show();
        this.leave.status = "rejected";
    }

    onHide() {
        this.verifyOtpDialog.hide();
    }

    onOk() {
        console.log(this.from);
        if (this.from == "halfDayLeave") {
            if (this.otpText == "") {
                alert("Please enter otp.");
            }
            else if (this.otpText.length < 6) {
                alert("OTP should be of six digits.");
            }
            else {
                this.leave.otp = this.otpText;
                this.leave.leaveToken = this.leaveToken;
                this.isLoading = true;
                this.http
                    .post(Values.BASE_URL + "leaves/verifyOtp", this.leave, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.isLoading = false;
                                if (this.leave.status == "confirmed") {
                                    Toast.makeText("Leave is successfully confirmed", "long").show();
                                }
                                else {
                                    Toast.makeText("Leave is successfully rejected", "long").show();
                                }
                                this.verifyOtpDialog.hide();
                                this.routerExtensions.navigate(['/viewLeaves'], {
                                    clearHistory: true
                                });
                            }
                        }
                    }, error => {
                        this.isLoading = false;
                        alert(error.error.error);
                    });
            }
        }
        else {
            if (this.leave.status == "confirmed") {
                this.isLoading = true;
                this.updateFullDayLeave();
            }
            else {
                if (this.rejectionReasonText == "") {
                    alert("Please enter rejection reason.");
                }
                else {
                    this.leave.rejectionReason = this.rejectionReasonText;
                    this.isLoading = true;
                    this.updateFullDayLeave();
                }
            }
        }
    }

    updateFullDayLeave() {
        this.isLoading = true;
        console.log(this.leave);
        this.http
            .put(Values.BASE_URL + "leaves/update/" + this.leaveId, this.leave, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.isLoading = false;
                        if (this.leave.status == "confirmed") {
                            Toast.makeText("Leave is successfully confirmed", "long").show();
                        }
                        else {
                            Toast.makeText("Leave is successfully rejected", "long").show();
                        }
                        this.verifyOtpDialog.hide();
                        this.routerExtensions.navigate(['/viewLeaves'], {
                            clearHistory: true
                        });
                    }
                }
            }, error => {
                this.isLoading = false;
                alert(error.error.error);
            });
    }

}