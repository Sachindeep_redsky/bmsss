import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ViewLeaveComponent } from "./components/view-leave.component";
import { ViewLeaveRoutingModule } from "./view-leave-routing.module";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from 'nativescript-ng-shadow';
import { NgModalModule } from "../modals/ng-modal";


@NgModule({
    imports: [
        HttpModule,
        ViewLeaveRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NgModalModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        ViewLeaveComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class ViewLeaveModule { }
