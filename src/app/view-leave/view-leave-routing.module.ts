import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ViewLeaveComponent } from "./components/view-leave.component";

const routes: Routes = [
    { path: "", component: ViewLeaveComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ViewLeaveRoutingModule { }
