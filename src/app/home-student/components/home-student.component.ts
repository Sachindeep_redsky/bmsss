import { Component, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "~/app/services/user.service";
import * as localstorage from "nativescript-localstorage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { NavigationExtras, Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;
@Component({
    selector: "app-homeStudent",
    moduleId: module.id,
    templateUrl: "./home-student.component.html",
    styleUrls: ['./home-student.component.css'],
})
export class HomeStudentComponent implements OnInit {
    cards;
    renderViewTimeout;
    isRendering: boolean;
    profilePic: string;
    userName: string;
    headers: HttpHeaders;
    token: string;
    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.cards = [];
        this.userService.showMessageCompose(false);
        this.userService.showLoadingState(false);
        this.userService.activeScreen("homeStudent");
        this.userName = "Hello, "
        this.token = "";
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        if (localstorage.getItem("userName") != null && localstorage.getItem("userName") != undefined) {
            this.userName = this.userName + localstorage.getItem("userName");
        }

        this.page.on('navigatedTo', (data) => {
            if (data.isBackNavigation) {
                this.userService.activeScreen("homeStudent");
            }
        });

    }
    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
            this.setCardData();
        }, 1000);

        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }

    }
    protected get shadowColor(): Color {
        return new Color('#888888');
    }
    protected get shadowOffset(): number {
        return 2.0;
    }
    setCardData() {
        this.cards = [{
            'name': 'Profile',
            'icon': 'res://profile'
        },
        {
            'name': 'Attendance',
            'icon': 'res://attendance'
        },
        {
            'name': 'Homework',
            'icon': 'res://homework'
        },
        {
            'name': 'Syllabus',
            'icon': 'res://syllabus'
        },
        {
            'name': 'Fees',
            'icon': 'res://fees'
        },
        {
            'name': 'Calender',
            'icon': 'res://calender'
        },
        {
            'name': 'Birthdays',
            'icon': 'res://birthday'
        },
        {
            'name': 'Teachers',
            'icon': 'res://teacher'
        },
        {
            'name': 'Messages',
            'icon': 'res://messages'
        },
        {
            'name': 'Leave',
            'icon': 'res://leaves'
        },
        {
            'name': 'Gallery',
            'icon': 'res://gallery'
        },
        {
            'name': 'Transport',
            'icon': 'res://transport'
        },
        {
            'name': 'Library',
            'icon': 'res://library'
        },
        {
            'name': 'Result',
            'icon': 'res://result'
        }
        ];
    }
    onImageLoaded(args) {
        var image = <any>args.object;
        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20);
            }
            else if (image.ios) {
                let nativeImageView = image.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5;
                nativeImageView.layer.shadowRadius = 5.0;
            }
        }, 400);
    }

    onOptionClick(i: number) {
        if (i == 0) {
            this.routerExtensions.navigate(['/profileStudent']);
        }
        if (i == 1) {
            this.routerExtensions.navigate(['/attendanceStudent']);
        }
        if (i == 2) {
            this.routerExtensions.navigate(['/homework']);
        }
        if (i == 3) {
            this.routerExtensions.navigate(['/downloadSyllabus']);
        }
        if (i == 4) {
            this.routerExtensions.navigate(['/fees']);
        }
        if (i == 5) {
            this.routerExtensions.navigate(['/calender']);
        }
        if (i == 6) {
            this.routerExtensions.navigate(['/birthdays']);
        }
        if (i == 7) {
            this.routerExtensions.navigate(['/teachers']);
        }
        if (i == 8) {
            this.routerExtensions.navigate(['/messages']);
        }
        if (i == 9) {
            // this.routerExtensions.navigate(['./applyLeave']);
            this.routerExtensions.navigate(['./applyLeave']);
        }
        if (i == 10) {
            this.routerExtensions.navigate(['/viewImagesFolder']);
        }
        if (i == 11) {
            this.routerExtensions.navigate(['/transport']);
        }
    }
}
