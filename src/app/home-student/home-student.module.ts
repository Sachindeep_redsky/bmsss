import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from 'nativescript-ng-shadow';
import { HomeStudentRoutingModule } from "./home-student-routing.module";
import { HomeStudentComponent } from "./components/home-student.component";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
    imports: [
        HttpModule,
        HomeStudentRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule

    ],
    declarations: [
        HomeStudentComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class HomeStudentModule { }
