import { UserService } from './../../services/user.service';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { Values } from "~/app/values/values";
import * as localstorage from "nativescript-localstorage";
import { ModalComponent } from '~/app/modals/modal.component';

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-fees",
    moduleId: module.id,
    templateUrl: "./fees.component.html",
    styleUrls: ['./fees.component.css'],
})

export class FeesComponent implements OnInit {

    @ViewChild('payFeeDialog', { static: false }) payFeeDialog: ModalComponent;
    // @ViewChild('selectChargesDialog') selectChargesDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    year: string;
    totalFee: string;
    balanceFee: string;
    isLoading: boolean;
    admissionNoHint: string;
    admissionNoBorderColor: string;
    admissionNo: string;
    isVisible: boolean;
    token: string;
    headers: HttpHeaders;
    feeMessage: string;
    isFeeMessage: boolean;
    feeChargesList;
    selectChargesText: string;
    payFeeHint: string;
    payFeeBorderColor: string;
    payFee: string;
    dialogCancelButton: string;
    dialogPayButton: string;
    feeChargesId: string;
    feesId: string;
    isStudent: boolean;
    profilePic: string;
    constructor(private routerExtensions: RouterExtensions, private page: Page, private http: HttpClient, private userService: UserService) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.totalFee = "";
        this.balanceFee = "";
        // this.upcomingFee = "7000";
        this.isLoading = false;
        this.admissionNoHint = "Admission number";
        this.admissionNoBorderColor = "black";
        this.admissionNo = "";
        this.payFeeHint = "Enter payment amount";
        this.payFeeBorderColor = "white";
        this.payFee = "";
        this.isVisible = true;
        this.feeMessage = "Enter admission number to show and pay the fees."
        this.isFeeMessage = true;
        this.feeChargesList = [];
        this.selectChargesText = "Select Fee Charges";
        this.dialogCancelButton = "Cancel";
        this.dialogPayButton = "Pay";
        this.feeChargesId = "";
        this.feesId = "";
        this.isStudent = false;
        this.profilePic = "";
        this.userService.activeScreen("fees");

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        if (localstorage.getItem("userType") != null && localstorage.getItem("userType") != undefined) {
            if (localstorage.getItem("userType") == "student") {
                this.isStudent = true;
                this.feeMessage = ""
                this.isFeeMessage = true;
                if (localstorage.getItem("admissionNumber") != null && localstorage.getItem("admissionNumber") != undefined) {
                    this.admissionNo = localstorage.getItem("admissionNumber");
                    this.getFees();
                }
            }
        }
        this.page.on('navigatedTo', (data) => {
            if (data.isBackNavigation) {
                console.log("BACK FROM");
            }
        });
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onGridLoaded(args) {
        var grid = <any>args.object;

        setTimeout(() => {
            if (grid.android) {
                let nativeImageView = grid.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                // nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (grid.ios) {
                let nativeImageView = grid.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onPayFeeDialogLoaded(args) {
        var payFeeDialog = <any>args.object;
        setTimeout(() => {
            if (payFeeDialog.android) {
                let nativeImageView = payFeeDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (payFeeDialog.ios) {
                let nativeImageView = payFeeDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onPayFeeOutsideClick() {
        this.payFeeDialog.hide();
    }

    onChargesDialogLoaded(args) {
        var chargesDialog = <any>args.object;
        setTimeout(() => {
            if (chargesDialog.android) {
                let nativeImageView = chargesDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (chargesDialog.ios) {
                let nativeImageView = chargesDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    // onChargesOutsideClick() {
    //     this.selectChargesDialog.hide();
    // }

    onBack() {
        this.routerExtensions.back();
    }

    onAdmissionNoTextChanged(args) {
        this.admissionNo = args.object.text;
        this.admissionNoBorderColor = "black";
        if (this.admissionNo == "") {
            this.admissionNoBorderColor = "black";
        }
    }

    onPayFeeTextChanged(args) {
        this.payFee = args.object.text;
        this.payFeeBorderColor = "white";
        if (this.payFee == "") {
            this.payFeeBorderColor = "black";
        }
    }

    onMoreDetails() {
        // this.routerExtensions.navigate(['/feeDetail']);

        if (this.admissionNo == "") {
            alert("Please select admission number first.");
        }
        else {
            this.routerExtensions.navigate(['/feeDetail'], {
                queryParams: {
                    "admissionNo": this.admissionNo
                }
            });
        }
        // else {
        //     this.feeChargesId = "";
        //     this.selectChargesText = "Select Fee Charges";
        //     this.payFee = "";
        //     this.getFeeCharges();
        //     this.payFeeDialog.show();
        // }
    }

    onFeeCharges() {
        this.routerExtensions.navigate(['/feeCharges'])
    }

    onAddFees() {
        this.routerExtensions.navigate(['/addFees']);
    }

    onGetFee() {
        if (this.admissionNo == "") {
            alert("Please select admission number first.");
        }
        else {
            this.isLoading = true;
            this.getFees();
        }
    }

    getFees() {
        this.http
            .get(Values.BASE_URL + "fees?admissionNo=" + this.admissionNo, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.trace(res);
                        if (res.data.fee.length > 0) {
                            this.totalFee = res.data.totalDueAmount;
                            this.balanceFee = res.data.totalBalanceAmount;
                            this.isVisible = false;
                            this.isLoading = false;
                            this.isFeeMessage = false;
                        }
                        else {
                            this.isVisible = true;
                            this.isLoading = false;
                            this.isFeeMessage = true;
                            this.feeMessage = "There is no fee data for this admission number."
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onUpdateFee() {
        if (this.admissionNo == "") {
            alert("Please select admission number first.");
        }
        else {
            this.routerExtensions.navigate(['/addFees'], {
                queryParams: {
                    "from": "edit",
                    "admissionNo": this.admissionNo
                }
            });
        }
    }

    // onSelectCharges() {
    //     this.selectChargesDialog.show();
    //     this.selectChargesText = "Select Fee Charges";
    // }

    // getFeeCharges() {
    //     this.isLoading = true;
    //     this.http
    //         .get(Values.BASE_URL + "feeCharges", {
    //             headers: this.headers
    //         })
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     this.feeChargesList = [];
    //                     for (var i = 0; i < res.data.length; i++) {
    //                         this.feeChargesList.push({
    //                             id: res.data[i].id,
    //                             headId: res.data[i].headId,
    //                             headName: res.data[i].headName,
    //                         });
    //                     }
    //                     this.isLoading = false;
    //                 }
    //             }
    //         }, error => {
    //             this.isLoading = false;
    //             console.log(error.error.error);
    //         });
    // }

    onChargesButton(item: any) {
        this.feeChargesId = item.headId;
        this.selectChargesText = item.headName;
    }

    // onCancel() {
    //     this.payFeeDialog.hide();
    // }

    // onPay() {
    //     if (this.feeChargesId == "") {
    //         alert("Please select fee charges.");
    //     }
    //     else if (this.payFee == "") {
    //         alert("Please enter payment amount.")
    //     }
    //     else {
    //         alert("hit api here.");
    //     }
    // }
}