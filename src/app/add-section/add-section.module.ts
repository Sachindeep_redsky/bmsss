import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from "nativescript-ng-shadow";
import { NgModalModule } from "../modals/ng-modal";
import { AddSectionRoutingModule } from "./add-section-routing.module";
import { AddSectionComponent } from "./components/add-section.component";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

@NgModule({
    imports: [
        HttpModule,
        GridViewModule,
        // NgShadowModule,
        NgModalModule,
        AddSectionRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        AddSectionComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AddSectionModule { }
