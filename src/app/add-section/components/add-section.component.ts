import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ModalComponent } from "~/app/modals/modal.component";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { UserService } from "~/app/services/user.service";
import { Classes } from "~/app/models/classes.model";
import { Page } from "tns-core-modules/ui/page/page";
import * as localstorage from "nativescript-localstorage";
import { Section } from '~/app/models/section.model';

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-addSection",
    moduleId: module.id,
    templateUrl: "./add-section.component.html",
    styleUrls: ['./add-section.component.css'],
})

export class AddSectionComponent implements OnInit {

    @ViewChild('addSectionDialog', { static: false }) addSectionDialog: ModalComponent;
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;

    sections;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    sectionHint: string;
    sectionBorderColor: string;
    sectionText: string;
    section: Section;
    sectionId: number;
    selectClassText: string;
    classes;
    classId: string;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    sectionMessage: string;
    isData: boolean;
    type: string;
    dialogAddButton: string;
    profilePic: string;

    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.sections = [];
        this.year =  Values.SESSION;
        this.sectionHint = "Section name";
        this.sectionBorderColor = "black";
        this.sectionText = "";
        this.section = new Section();
        this.sectionId = 0;
        this.selectClassText = "Select Class"
        this.classes = [];
        this.classId = "";
        this.isLoading = false;
        this.token = "";
        this.sectionMessage = "Select class to show the sections list.";
        this.isData = true;
        this.type = "";
        this.dialogAddButton = "";
        this.userService.activeScreen("addSection");
        this.profilePic = "";
        
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        // this.router.navigate(['/homeAdmin']);
        this.routerExtensions.back();
    }

    onSubjectOutsideClick() {
        this.addSectionDialog.hide();
    }

    onClassOutsideClick() {
        this.selectClassDialog.hide();
    }

    onSectionTextChanged(args) {
        this.sectionText = args.object.text;
        // this.subjectBorderColor = "white";
        if (this.sectionText == "") {
            // this.subjectBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onSectionDialogLoaded(args) {
        var sectionDialog = <any>args.object;
        setTimeout(() => {
            if (sectionDialog.android) {
                let nativeImageView = sectionDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#A61F21'));
                nativeImageView.setElevation(10)
            } else if (sectionDialog.ios) {
                let nativeImageView = sectionDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#A61F21'));
                nativeImageView.setElevation(10)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onAddSection() {
        this.sectionText = "";
        this.dialogAddButton = "Add";
        this.sectionHint = "Section name";
        this.type = "";
        this.addSectionDialog.show();
    }

    onSectionOutsideClick() {
        this.addSectionDialog.hide();
    }

    getSections() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "sections?classId=" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.sections = [];
                        if (res.data.length > 0) {
                            this.isData = false;
                            for (var i = 0; i < res.data.length; i++) {
                                var name = res.data[i].name.charAt(0).toUpperCase() + res.data[i].name.slice(1);
                                this.sections.push({
                                    id: res.data[i].id,
                                    name: name,
                                    status: res.data[i].status
                                });
                            }
                        }
                        else {
                            this.isData = true;
                            this.sectionMessage = "There is no sections added for this class.";
                        }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onAdd() {
        if (this.classId == "") {
            alert("Please select class first");
        }
        else if (this.sectionText == "") {
            alert("Section name cannot be empty");
        }
        else {
            this.section.name = this.sectionText;
            if (this.type == "edit") {
                this.http
                    .put(Values.BASE_URL + "sections/update/" + this.sectionId, this.section, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                console.log(res);
                                this.userService.showLoadingState(false);
                                Toast.makeText("Section updated successfully.", "long").show();
                                this.addSectionDialog.hide();
                                this.getSections();
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        console.log(error.error.error);
                    });
            }
            else {
                this.sectionText = "";
                this.section.classId = parseInt(this.classId);
                this.http
                    .post(Values.BASE_URL + "sections", this.section, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.userService.showLoadingState(false);
                                Toast.makeText("Section added successfully.", "long").show();
                                this.addSectionDialog.hide();
                                this.getSections();
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        console.log(error.error.error);
                    });
            }
        }
    }

    onSelectClass() {
        this.getClasses();
        this.selectClassDialog.show();
    }

    getClasses() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "classes", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.classes = [];
                        for (var i = 0; i < res.data.length; i++) {
                            this.classes.push({
                                id: res.data[i].id,
                                name: res.data[i].name
                            });
                        }
                        this.isLoading = false;
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onClassButton(item: Classes) {
        this.selectClassText = item.name;
        this.classId = item.id.toString();
        this.getSections();
    }

    // onDelete(id: any) {
    //     this.isLoading = true;
    //     this.http
    //         .delete(Values.BASE_URL + "subjects/delete/" + id, {
    //             headers: this.headers
    //         })
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     Toast.makeText("Deleted", "short").show();
    //                     this.isLoading = false;
    //                     this.getSubjects();
    //                 }
    //             }
    //         }, error => {
    //             this.isLoading = false;
    //             console.log(error.error.error);
    //         });
    // }

    onActiveInactiveButton(item: any) {
        this.isLoading = true;
        if (item.status == "active") {
            this.section.status = "inactive";
        }
        else {
            this.section.status = "active";
        }
        this.http
            .put(Values.BASE_URL + "sections/update/" + item.id, this.section, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        Toast.makeText("Updated", "short").show();
                        this.isLoading = false;
                        this.getSections();
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onEdit(item: any) {
        this.sectionText = item.name;
        this.sectionId = item.id;
        this.type = "edit";
        this.dialogAddButton = "Update";
        this.addSectionDialog.show();
    }
}