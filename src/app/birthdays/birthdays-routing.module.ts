import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { BirthdaysComponent } from "./components/birthdays.component";

const routes: Routes = [
    { path: "", component: BirthdaysComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class BirthdaysRoutingModule { }
