import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from "~/app/services/user.service";
import * as Toast from 'nativescript-toast';
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { ModalComponent } from "~/app/modals/modal.component";
import { Messages } from "~/app/models/messages.model";

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-birthdays",
    moduleId: module.id,
    templateUrl: "./birthdays.component.html",
    styleUrls: ['./birthdays.component.css'],
})

export class BirthdaysComponent implements OnInit {
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;
    @ViewChild('selectSectionDialog', { static: false }) selectSectionDialog: ModalComponent;
    @ViewChild('addWishDialog', { static: false }) addWishDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    birthdays;
    year: string;
    classId: string;
    profilePic: string;
    isLoading: boolean;
    section: string;
    isClassPicker: boolean;
    isSectionPicker: boolean;
    classes;
    sections;
    classButton: string;
    sectionButton: string;
    height: string;
    marginTop: string;
    token: string;
    headers: HttpHeaders;
    isData: boolean;
    birthdayMessage: string;
    isSections: boolean;
    sectionMessage: string;
    wishHint: string;
    wishBorderColor: string;
    wishText: string;
    messages: Messages;
    msgId: string;
    pageNo: number;
    classTeacher;
    index: number;
    day: string;
    month: string;
    isStudent: boolean;
    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.classId = "";
        this.birthdays = [];
        this.profilePic = "res://profile";
        this.isLoading = false;
        this.section = "";
        this.classButton = "Select Class";
        this.sectionButton = "Select Section";
        this.classes = [];
        this.sections = [];
        this.token = "";
        this.isData = true;
        this.isSections = false;
        this.sectionMessage = "";
        this.wishHint = "Birthday message here";
        this.wishBorderColor = "black";
        this.wishText = "";
        this.messages = new Messages();
        this.msgId = "";
        this.pageNo = 1;
        this.classTeacher = [];
        this.index = 0;
        this.birthdayMessage = "Select class and section to show birthdays.";
        this.userService.activeScreen("birthdays");
        this.day = date.getDate().toString();
        this.month = (date.getMonth() + 1).toString();
        this.isStudent = false;
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        if (localstorage.getItem("userType") == "admin" || localstorage.getItem("userType") == "teacher") {
            this.isStudent = false;
            this.isClassPicker = true;
            this.isSectionPicker = false;
            this.height = "60%";
            this.marginTop = "40%";
        }
        else {
            this.isStudent = true;
            this.isClassPicker = false;
            this.isSectionPicker = false;
            this.height = "65%";
            this.marginTop = "35%";
            this.getClassSection();
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
        // this.birthdays.push({ date: "07 April", name: "Aarti", });
        // this.birthdays.push({ date: "14 April", name: "Emmanual", });
        // this.birthdays.push({ date: "16 April", name: "Abhishek Shukla", });
        // this.birthdays.push({ date: "21 April", name: "Diksha Rani", });
        // this.birthdays.push({ date: "28 April", name: "Shubham", });
        // this.birthdays.push({ date: "05 May", name: "Mukesh Kumar)", });
        // this.birthdays.push({ date: "12 May", name: "Aakshi Jain", });
        // this.birthdays.push({ date: "15 May", name: "Anita Rani", });
        // this.birthdays.push({ date: "19 May", name: "Anupreet Kaur", });
        // this.birthdays.push({ date: "26 May", name: "Aarti Rani", });

    }

    onClass() {
        this.isSectionPicker = false;
        this.sectionButton = "Select Section";
        this.birthdays = [];
        this.getClasses();
    }

    onSection() {
        if (localstorage.getItem("userType") == "teacher") {
            this.sections = [];
            for (var i = 0; i < this.classTeacher[this.index].section.length; i++) {
                this.sections.push({
                    name: this.classTeacher[this.index].section[i].classSection,
                });
            }
            this.selectSectionDialog.show();
        }
        else {
            this.getSections();
        }
    }

    getClasses() {
        this.isLoading = true;
        if (localstorage.getItem("userType") == "teacher") {
            this.http
                .get(Values.BASE_URL + `users/search?pageNo=${this.pageNo}&userType=teacher&regNo=${localstorage.getItem("regNo")}`, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("RESSSS", res);
                            if (res.data.users[0].classTeacher.length > 0) {
                                this.classTeacher = res.data.users[0].classTeacher;
                                if (res.data.users[0].classIncharge == "true") {
                                    this.classTeacher.push({
                                        "id": res.data.users[0].classId,
                                        "name": res.data.users[0].className,
                                        "section": [{ "classSection": res.data.users[0].classSection }]
                                    })
                                }
                                console.trace("CLASS TEACHER", this.classTeacher);
                                this.classes = [];
                                for (var i = 0; i < this.classTeacher.length; i++) {
                                    this.classes.push({
                                        index: i,
                                        id: this.classTeacher[i].id,
                                        name: this.classTeacher[i].name
                                    });
                                }
                                this.selectClassDialog.show();
                                this.isLoading = false;
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
        else {
            this.http
                .get(Values.BASE_URL + "classes", {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.classes = [];
                            for (var i = 0; i < res.data.length; i++) {
                                this.classes.push({
                                    id: res.data[i].id,
                                    name: res.data[i].name,
                                });
                            }
                            this.selectClassDialog.show();
                            // this.userService.showLoadingState(false);
                            this.isLoading = false;
                        }
                    }
                }, error => {
                    // this.userService.showLoadingState(false);
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    getSections() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "classes/" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        if (res.data.section.length > 0) {
                            this.isSections = false;
                            this.sections = [];
                            for (var i = 0; i < res.data.section.length; i++) {
                                this.sections.push({
                                    name: res.data.section[i].classSection,
                                });
                            }
                        } else {
                            this.isSections = true;
                            this.sectionMessage = "There is no sections for this class.";
                        }
                        this.selectSectionDialog.show();
                        // this.userService.showLoadingState(false);
                        this.isLoading = false;
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false
                console.log(error.error.error);
            });
    }

    onClassButton(item: any) {
        this.classId = item.id;
        this.classButton = item.name;
        this.index = item.index;
        this.isSectionPicker = true;
    }

    onSectionButton(item: any) {
        this.sectionButton = item.name;
        this.section = item.name;
        this.getBirthdays();
    }

    onClassOutsideClick() {
        this.selectClassDialog.hide();
    }

    onSectionOutsideClick() {
        this.selectSectionDialog.hide();
    }

    onOutsideClick() {
        this.addWishDialog.hide();
    }

    // getClasses() {
    //     // this.userService.showLoadingState(true);
    //     this.isLoading = true;
    //     this.http
    //         .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"))
    //         .subscribe((res: any) => {
    //             if (res != null && res != undefined) {
    //                 if (res.isSuccess == true) {
    //                     this.classId = res.data.classId;
    //                     this.getBirthdays();
    //                 }
    //             }
    //         }, error => {
    //             // this.userService.showLoadingState(false);
    //             this.isLoading = false;
    //             console.log(error.error.error);
    //         });
    // }

    getClassSection() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"), {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.classId = res.data.classId;
                        this.section = res.data.classSection;
                        this.getBirthdays();
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    getBirthdays() {
        // console.log(Values.BASE_URL + "birthdays?classId=" + this.classId + "&section=" + this.section + "&userType=student");
        this.http
            .get(Values.BASE_URL + "birthdays?classId=" + this.classId + "&classSection=" + this.section + "&userType=student", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.log("RES::::", res);
                        if (res.data.length > 0) {
                            this.isData = false;
                            this.birthdays = [];
                            for (var i = 0; i < res.data.length; i++) {
                                var birthdayDate = res.data[i].birthdayDate;
                                var month = birthdayDate.substr(5, 2);
                                var day = birthdayDate.substr(8, 2);
                                var isBirthday: boolean;
                                if (month <= this.month) {
                                    if (month < this.month) {
                                        isBirthday = true;
                                    }
                                    else {
                                        if (day <= this.day) {
                                            isBirthday = true;
                                        }
                                    }
                                }
                                else {
                                    isBirthday = false;
                                }

                                this.birthdays.push({
                                    date: res.data[i].birthdayDate,
                                    name: res.data[i].name,
                                    rollNo: res.data[i].rollNo,
                                    isBirthday: isBirthday,
                                    userId: res.data[i].userId
                                });
                            }
                        }
                        else {
                            this.isData = false;
                            this.birthdayMessage = "Birthday list is empty.";
                        }
                        // this.userService.showLoadingState(false);
                        this.isLoading = false;
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onWishTextChanged(args) {
        this.wishText = args.object.text;
        this.wishBorderColor = "white";
        if (this.wishText == "") {
            this.wishBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSectionDialogLoaded(args) {
        var sectionDialog = <any>args.object;
        setTimeout(() => {
            if (sectionDialog.android) {
                let nativeImageView = sectionDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (sectionDialog.ios) {
                let nativeImageView = sectionDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onWishDialogLoaded(args) {
        var wishDialog = <any>args.object;
        setTimeout(() => {
            if (wishDialog.android) {
                let nativeImageView = wishDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#A61F21'));
                nativeImageView.setElevation(10)
            } else if (wishDialog.ios) {
                let nativeImageView = wishDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onBack() {
        if (localstorage.getItem("userType") == "teacher") {
            this.router.navigate(['/homeTeacher']);
        }
        else if (localstorage.getItem("userType") == "student") {
            this.router.navigate(['/homeStudent']);
        }
        else {
            this.router.navigate(['/homeAdmin']);
        }
    }

    onWish(item: any) {
        this.messages.askedBy = item.userId;
        this.messages.question = `It's ${item.name} birthday today. Wish ${item.name} the best!`;
        this.messages.classId = this.classId;
        this.messages.classSection = this.section;
        this.messages.userType = "student";
        console.log(this.messages);
        this.isLoading = true;
        this.http.post(Values.BASE_URL + "msges", this.messages, {
            headers: this.headers
        })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.msgId = res.data.id;
                        this.isLoading = false;
                        this.addWishDialog.show();
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error);
                alert(error.error.error);
            });
    }

    onDialogWish() {
        if (this.wishText == "") {
            alert("Please enter birthday wish message.");
        }
        else {
            this.isLoading = true;
            this.messages.repliedBy = localstorage.getItem("userId");
            this.messages.answer = this.wishText;
            this.messages.id = this.msgId;
            this.messages.askedTo = localstorage.getItem("userId");
            this.http.post(Values.BASE_URL + "msges/sendMsg", this.messages, {
                headers: this.headers
            })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.isLoading = false;
                            this.addWishDialog.hide();
                            Toast.makeText("Message sent succesfully", "long").show();
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    alert(error.error.error);
                });
        }
    }
}