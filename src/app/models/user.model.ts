export class User {

    email: string;
    userName: string;
    oldPassword: string;
    newPassword: string;
    otpVerifyToken: string;
    otp: string;
    password: string;
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.email = obj.email;
        this.userName = obj.userName;
        this.newPassword = obj.newPassword;
        this.otpVerifyToken = obj.otpVerifyToken;
        this.otp = obj.otp;
        this.password = obj.password;
    }
}