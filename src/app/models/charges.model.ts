export class Charges {

    headName: string;
    headId: string;
    id: number;
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.headName = obj.headName;
        this.headId = obj.headId;
        this.id = obj.id;
    }
}