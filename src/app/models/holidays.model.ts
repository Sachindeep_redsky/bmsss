export class Holidays {

    year: string;
    month: number;
    fromDate: string;
    toDate: string;
    description: string;
    isEvent: string;
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.year = obj.year;
        this.month = obj.month;
        this.fromDate = obj.fromDate;
        this.toDate = obj.toDate;
        this.description = obj.description;
        this.isEvent = obj.isEvent;
    }
}