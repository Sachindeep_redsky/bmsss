export class Syllabus {
    classId: string;
    file_url: string;
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.classId = obj.classId;
        this.file_url = obj.file_url;
    }
}