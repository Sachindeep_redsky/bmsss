export class Messages {

    classId: string;
    askedBy: string;
    askedTo: string;
    question: string;
    id: string;
    repliedBy: string;
    answer: string;
    classSection: string;
    userType: string;
    classes: string;
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.classId = obj.classId;
        this.askedBy = obj.askedBy;
        this.question = obj.question;
        this.id = obj.id;
        this.repliedBy = obj.repliedBy;
        this.answer = obj.answer;
        this.classSection = obj.classSection;
        this.askedTo = obj.askedTo;
        this.userType = obj.userType;
        this.classes = obj.classes;
    }
}