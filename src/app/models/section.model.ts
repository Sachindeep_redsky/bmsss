export class Section {

    name: string;
    status: string; //active or inactive
    id: number;
    classId: number;
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.name = obj.name;
        this.status = obj.status;
        this.id = obj.id;
        this.classId = obj.classId;
    }
}