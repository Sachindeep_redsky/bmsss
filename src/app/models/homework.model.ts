export class Homework {
    subjectId: string;
    classWork: string;
    homeWork: string;
    classId: string;
    section: string;
    subjectName: string;
    id: string;
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.subjectId = obj.subjectId;
        this.classWork = obj.classWork;
        this.homeWork = obj.homeWork;
        this.section = obj.section;
        this.classId = obj.classId;
        this.subjectName = obj.subjectName;
        this.subjectId = obj.subjectId;
        this.id = obj.id;
    }
}