export class Fees {

    dueId: string;
    dueAmount: string;
    month: string;
    feeChargesId: number;
    admissionNo: string;
    status: string;
    configId: string;
    payAmount: number;
    receiptNo: string;
    transType: string;
    remark: string;
    transRemark: string;
    transNo: string;
    bankName: string;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.dueId = obj.dueId;
        this.dueAmount = obj.dueAmount;
        this.month = obj.month;
        this.feeChargesId = obj.feeChargesId;
        this.admissionNo = obj.admissionNo;
        this.status = obj.status;
        this.configId = obj.configid;
        this.payAmount = obj.payAmount;
        this.receiptNo = obj.receiptNo;
        this.transType = obj.transType;
        this.transRemark = obj.transRemark;
        this.transNo = obj.transNo;
        this.bankName = obj.bankName;
    }
}