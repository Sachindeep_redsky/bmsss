export class Leave {

    reason: string;
    userId: string;
    otp: string;
    status: string;
    leaveToken: string;
    leaveType: string;
    fullDayType: string;
    fromDate: string;
    toDate: string;
    rejectionReason: string;
    constructor(obj?: any) {
        if (!obj) {
            return;
        }
        this.reason = obj.reason;
        this.userId = obj.userId;
        this.otp = obj.otp;
        this.status = obj.status;
        this.leaveToken = obj.leaveToken;
        this.leaveType = obj.leaveType;
        this.fullDayType = obj.fullDayType;
        this.fromDate = obj.fromDate;
        this.toDate = obj.toDate;
        this.rejectionReason = obj.rejectionReason;
    }
}