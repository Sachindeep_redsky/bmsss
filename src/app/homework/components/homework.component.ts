import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import { Homework } from "~/app/models/homework.model";
import { ModalComponent } from "~/app/modals/modal.component";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from "~/app/services/user.service";
import * as Toast from 'nativescript-toast';
import { DatePicker } from "tns-core-modules/ui/date-picker";
import { Page, Observable } from "tns-core-modules/ui/page/page";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-homework",
    moduleId: module.id,
    templateUrl: "./homework.component.html",
    styleUrls: ['./homework.component.css'],
})

export class HomeworkComponent implements OnInit {
    @ViewChild('viewHomeworkDialog', { static: false }) viewHomeworkDialog: ModalComponent;
    @ViewChild('viewDatePickerDialog', { static: false }) viewDatePickerDialog: ModalComponent;
    @ViewChild('selectClassDialog', { static: false }) selectClassDialog: ModalComponent;
    @ViewChild('selectSectionDialog', { static: false }) selectSectionDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    homeworkList;
    date: string;
    homeworkHint: string;
    homeworkBorderColor: string;
    backgroundColor: string;
    height: string;
    marginTop: string;
    isVisibleAddButton: boolean;
    isVisibleEdit: boolean;
    isVisibleDelete: boolean;
    homework: Homework;
    homeworkDetail: string;
    classworkDetail: string;
    subjectName: string;
    classId: string;
    homeworkId: string;
    section: string;
    day: string;
    month: string;
    year: string;
    isDatePicker: boolean;
    isClassPicker: boolean;
    isSectionPicker: boolean;
    classes;
    sections;
    classButton: string;
    sectionButton: string;
    profilePic: string;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    isData: boolean;
    homeworkMessage: string;
    isSection: boolean;
    rowHeight: string;
    todayDate: string;
    pageNo: number;
    count: number;
    classTeacher;
    index: number;
    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.homeworkHint = "Enter homework";
        this.homeworkBorderColor = "white";
        this.backgroundColor = "#6D7CF1"
        this.homework = new Homework();
        this.homeworkDetail = "";
        this.classworkDetail = "";
        this.subjectName = "";
        this.classId = "";
        this.homeworkList = new ObservableArray();
        this.section = "";
        this.day = date.getDate().toString();
        this.month = (date.getMonth() + 1).toString();
        this.year = date.getFullYear().toString();
        this.classes = [];
        this.sections = [];
        this.classButton = "Select Class";
        this.sectionButton = "Select Section";
        this.isLoading = false;
        this.token = "";
        this.profilePic = "res://profilepic";
        this.isData = true;
        this.isSection = true;
        this.homeworkMessage = "Select date, class and section to show the homework.";
        this.rowHeight = "";
        this.todayDate = "";
        this.pageNo = 1;
        this.count = 10;
        this.classTeacher = [];
        this.index = 0;
        this.userService.activeScreen("homework");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        if (localstorage.getItem("userType") == "teacher") {
            // if (localstorage.getItem("classIncharge") == "true" || localstorage.getItem("classIncharge") == "") {
            //     this.isVisibleAddButton = true;
            //     this.height = "55%";
            //     this.marginTop = "35%";
            //     this.isVisibleEdit = true;
            //     this.isVisibleDelete = true;
            //     this.isDatePicker = true;
            //     this.isClassPicker = false;
            //     this.isSectionPicker = false;
            //     this.getClassSection();
            //     this.rowHeight = "30%";
            // } else {
            this.isVisibleAddButton = true;
            this.height = "50%";
            this.marginTop = "40%";
            this.isVisibleEdit = true;
            this.isVisibleDelete = true;
            this.isDatePicker = true;
            this.isClassPicker = true;
            this.isSectionPicker = false;
            this.rowHeight = "30%";
            // }
        }
        else if (localstorage.getItem("userType") == "student") {
            this.isVisibleAddButton = false;
            this.height = "65%";
            this.marginTop = "35%";
            this.isVisibleEdit = false;
            this.isVisibleDelete = false;
            this.isDatePicker = true;
            this.isClassPicker = false;
            this.isSectionPicker = false;
            this.getClassSection();
            this.rowHeight = "28%";
        }
        else {
            this.isVisibleAddButton = false;
            this.height = "60%";
            this.marginTop = "42%";
            this.isVisibleEdit = false;
            this.isVisibleDelete = false;
            this.isDatePicker = true;
            this.isClassPicker = true;
            this.isSectionPicker = false;
            this.rowHeight = "30%";
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        this.date = date.getDate().toString() + '/' + (date.getMonth() + 1).toString() + '/' + date.getFullYear().toString();
        this.todayDate = this.date;
        // this.homeworkList.push({ subject: "Hindi", homework: "kfsdj dhskjahs dhsajhd hkdskjasdh shdskjdhs ghjdsgf gshghds sjdgsdf sdgfsdbf sdgfjasd fjgds fjgdsf sdgf dsfgfsd sdfgsd fuygdsvf bvsdfs dfugsfjsdbfsf dfbsduyfsdfvuysdg ugsdug" });
        // this.homeworkList.push({ subject: "English", homework: "kfsdj dhskjahs dhsajhd hkdskjasdh shdskjdhs" });
        // this.homeworkList.push({ subject: "Mathematics", homework: "kfsdj dhskjahs dhsajhd hkdskjasdh shdskjdhs" });
        // this.homeworkList.push({ subject: "Punjabi", homework: "kfsdj dhskjahs dhsajhd hkdskjasdh shdskjdhs" });
        // this.homeworkList.push({ subject: "Hindi", homework: "kfsdj dhskjahs dhsajhd hkdskjasdh shdskjdhs" });
        // this.homeworkList.push({ subject: "English", homework: "kfsdj dhskjahs dhsajhd hkdskjasdh shdskjdhs" });
        // this.homeworkList.push({ subject: "Mathematics", homework: "kfsdj dhskjahs dhsajhd hkdskjasdh shdskjdhs" });
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
        this.page.on('navigatedTo', (data) => {
            if (data.isBackNavigation) {
                if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
                    this.token = localstorage.getItem("token");
                    this.headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "x-access-token": this.token
                    });
                }
                // this.getHolidays();
                this.homeworkList = new ObservableArray();
                this.getHomework();
            }
        });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onPickerLoaded(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.year = date.getFullYear();
        datePicker.month = date.getMonth() + 1;
        datePicker.day = date.getDate();
        datePicker.minDate = new Date(2020, 3, 1);
        datePicker.maxDate = new Date(2021, 2, 31);
    }

    onDateChanged(args) {

    }

    onDayChanged(args) {
        this.day = args.value;
    }

    onMonthChanged(args) {
        this.month = args.value;
    }

    onYearChanged(args) {
        this.year = args.value;
    }

    onOkDate() {
        this.viewDatePickerDialog.hide();
        this.date = this.day + "/" + this.month + "/" + this.year;
        if (localstorage.getItem("userType") == "teacher") {
            // if (localstorage.getItem("classIncharge") == "true" || localstorage.getItem("classIncharge") == "") {
            //     if (this.date != this.todayDate) {
            //         this.isVisibleAddButton = false;
            //         this.height = "65%";
            //         this.marginTop = "35%";
            //         this.isVisibleEdit = false;
            //         this.isVisibleDelete = false;
            //         this.isDatePicker = true;
            //         this.isClassPicker = false;
            //         this.isSectionPicker = false;
            //         this.rowHeight = "28%";
            //     }
            //     else {
            //         this.isVisibleAddButton = true;
            //         this.height = "55%";
            //         this.marginTop = "35%";
            //         this.isVisibleEdit = true;
            //         this.isVisibleDelete = true;
            //         this.isDatePicker = true;
            //         this.isClassPicker = false;
            //         this.isSectionPicker = false;
            //         this.rowHeight = "30%";
            //     }
            // }
            // else {
            if (this.date != this.todayDate) {
                this.isVisibleAddButton = false;
                this.height = "60%";
                this.marginTop = "42%";
                this.isVisibleEdit = false;
                this.isVisibleDelete = false;
                this.isDatePicker = true;
                this.isClassPicker = true;
                this.isSectionPicker = false;
                this.rowHeight = "30%";
            }
            else {
                this.isVisibleAddButton = true;
                this.height = "45%";
                this.marginTop = "42%";
                this.isVisibleEdit = false;
                this.isVisibleDelete = false;
                this.isDatePicker = true;
                this.isClassPicker = true;
                this.isSectionPicker = false;
                this.rowHeight = "30%";
            }
            // }
        }
        if (localstorage.getItem("userType") == "student") {
            this.getClassSection();
        } else {
            this.isClassPicker = true;
        }
    }

    onDate() {
        this.viewDatePickerDialog.show();
        this.classButton = "Select Class";
        this.homeworkList = new ObservableArray();
        this.isClassPicker = false;
        this.isSectionPicker = false;
    }

    getClassSection() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"), {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.classId = res.data.classId;
                        this.section = res.data.classSection;
                        this.getHomework();
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    getHomework() {
        console.log("URL:::", Values.BASE_URL + "homeworks?classId=" + this.classId + "&section=" + this.section + "&date=" + this.date);
        this.http
            .get(Values.BASE_URL + "homeworks?classId=" + this.classId + "&section=" + this.section + "&date=" + this.date, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        // this.homeworkList = new ObservableArray();
                        console.log("HOMEWORK::::", res);
                        if (res.data.length > 0) {
                            this.isData = false;
                            for (var i = 0; i < res.data.length; i++) {
                                this.homeworkList.push({
                                    id: res.data[i].id,
                                    classwork: res.data[i].classWork,
                                    homework: res.data[i].homeWork,
                                    subjectName: res.data[i].subjectName
                                });
                                this.isLoading = false;
                            }
                        }
                        else {
                            this.isData = true;
                            this.homeworkMessage = "There is no homework."
                            this.isLoading = false;
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onHomeworkDialogLoaded(args) {
        var homeworkDialog = <any>args.object;
        setTimeout(() => {
            if (homeworkDialog.android) {
                let nativeImageView = homeworkDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (homeworkDialog.ios) {
                let nativeImageView = homeworkDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onDatePickerDialogLoaded(args) {
        var datePickerDialog = <any>args.object;
        setTimeout(() => {
            if (datePickerDialog.android) {
                let nativeImageView = datePickerDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (datePickerDialog.ios) {
                let nativeImageView = datePickerDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onClassDialogLoaded(args) {
        var classDialog = <any>args.object;
        setTimeout(() => {
            if (classDialog.android) {
                let nativeImageView = classDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (classDialog.ios) {
                let nativeImageView = classDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSectionDialogLoaded(args) {
        var sectionDialog = <any>args.object;
        setTimeout(() => {
            if (sectionDialog.android) {
                let nativeImageView = sectionDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (sectionDialog.ios) {
                let nativeImageView = sectionDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onBack() {
        // if (localstorage.getItem("userType") == "teacher") {
        //     this.router.navigate(['/homeTeacher']);
        // }
        // else if (localstorage.getItem("userType") == "student") {
        //     this.router.navigate(['/homeStudent']);
        // }
        // else {
        //     this.router.navigate(['/homeAdmin']);
        // }
        this.routerExtensions.back();
    }

    onAddHomework() {
        // if (localstorage.getItem("classIncharge") == "false") {
        if (this.classId == "") {
            alert("Please select class.");
        } else if (this.section == "") {
            alert("Please select section.");
        }
        else {
            this.routerExtensions.navigate(['/addHomework'], {
                queryParams: {
                    "classId": this.classId,
                    "section": this.section
                }
            });
        }
        // }
        // else {
        //     this.routerExtensions.navigate(['/addHomework']);
        // }
    }

    onHomeworkClick(item: any) {
        this.homeworkDetail = item.homework;
        this.classworkDetail = item.classwork;
        this.subjectName = item.subjectName;
        this.homeworkId = item.id;
        this.viewHomeworkDialog.show();
    }

    onHomeworkEdit() {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "subjectName": this.subjectName,
                "homeworkDetail": this.homeworkDetail,
                "classworkDetail": this.classworkDetail,
                "type": "edit",
                "homeworkId": this.homeworkId
            },
        };
        this.router.navigate(['/addHomework'], navigationExtras);
    }

    onHomeworkOutsideClick() {
        this.viewHomeworkDialog.hide();
    }

    onDeleteHomework() {
        // this.userService.showLoadingState(true);
        this.isLoading = true;
        this.http
            .delete(Values.BASE_URL + "homeworks/delete/" + this.homeworkId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.viewHomeworkDialog.hide();
                        this.getHomework();
                        Toast.makeText("Homework deleted successfully!!!", "long").show();
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onCancelHomework() {
        this.viewHomeworkDialog.hide();
    }

    onClass() {
        this.isSectionPicker = false;
        this.sectionButton = "Select Section";
        this.homeworkList = new ObservableArray();
        this.getClasses();
    }

    onSection() {
        if (localstorage.getItem("userType") == "teacher") {
            this.sections = [];
            console.log(this.classTeacher[this.index].section.length);
            for (var i = 0; i < this.classTeacher[this.index].section.length; i++) {
                this.sections.push({
                    name: this.classTeacher[this.index].section[i].classSection,
                });
            }
            this.selectSectionDialog.show();
        }
        else {
            this.getSections();
        }
    }

    getClasses() {
        this.isLoading = true;
        console.log(`users/search?pageNo=${this.pageNo}&count=${this.count}&userType=teacher&active=true&classIncharge=false&regNo=${localstorage.getItem("regNo")}`)
        if (localstorage.getItem("userType") == "teacher") {
            this.http
                .get(Values.BASE_URL + `users/search?pageNo=${this.pageNo}&userType=teacher&regNo=${localstorage.getItem("regNo")}`, {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            console.log("RESSSS", res);
                            if (res.data.users[0].classTeacher.length > 0) {
                                this.classTeacher = res.data.users[0].classTeacher;
                                if (res.data.users[0].classIncharge == "true") {
                                    this.classTeacher.push({
                                        "id": res.data.users[0].classId,
                                        "name": res.data.users[0].className,
                                        "section": [{ "classSection": res.data.users[0].classSection }]
                                    })
                                }
                                console.trace("CLASS TEACHER", this.classTeacher);
                                this.classes = [];
                                for (var i = 0; i < this.classTeacher.length; i++) {
                                    this.classes.push({
                                        index: i,
                                        id: this.classTeacher[i].id,
                                        name: this.classTeacher[i].name
                                    });
                                }
                                this.selectClassDialog.show();
                                this.isLoading = false;
                            }
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
        else {
            this.http
                .get(Values.BASE_URL + "classes", {
                    headers: this.headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.classes = [];
                            for (var i = 0; i < res.data.length; i++) {
                                this.classes.push({
                                    id: res.data[i].id,
                                    name: res.data[i].name,
                                });
                            }
                            this.selectClassDialog.show();
                            this.isLoading = false;
                        }
                    }
                }, error => {
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    getSections() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "classes/" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.sections = [];
                        if (res.data.section.length > 0) {
                            this.isSection = true;
                            for (var i = 0; i < res.data.section.length; i++) {
                                this.sections.push({
                                    name: res.data.section[i].classSection,
                                });
                            }
                        }
                        else {
                            this.isSection = false;
                        }
                        this.selectSectionDialog.show();
                        // this.userService.showLoadingState(false);
                        this.isLoading = false;
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false
                console.log(error.error.error);
            });
    }

    onClassButton(item: any) {
        this.classId = item.id;
        this.classButton = item.name;
        this.index = item.index;
        this.isSectionPicker = true;
    }

    onSectionButton(item: any) {
        this.sectionButton = item.name;
        this.section = item.name;
        this.getHomework();

    }

    onClassOutsideClick() {
        this.selectClassDialog.hide();
    }

    onSectionOutsideClick() {
        this.selectSectionDialog.hide();
    }

    // onLeftArrow() {
    //     alert("left click");
    // }

    // onRightArrow() {
    //     alert("right click");
    // }
}