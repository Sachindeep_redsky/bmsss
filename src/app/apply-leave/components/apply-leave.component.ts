import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Leave } from "~/app/models/leave.model";
import * as localstorage from "nativescript-localstorage";
import { Page } from "tns-core-modules/ui/page/page";
import { DatePicker } from 'tns-core-modules/ui/date-picker/date-picker';
import { ModalComponent } from '~/app/modals/modal.component';
import { Values } from '~/app/values/values';
import * as Toast from 'nativescript-toast';

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-applyLeave",
    moduleId: module.id,
    templateUrl: "./apply-leave.component.html",
    styleUrls: ['./apply-leave.component.css'],
})

export class ApplyLeaveComponent implements OnInit {

    @ViewChild('datePickerDialog', { static: false }) datePickerDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    date: string;
    year: string;
    sessionYear: string;
    day: string;
    month: string;
    reasonHint: string;
    reasonBorderColor: string;
    reason: string;
    leave: Leave;
    userId: string;
    profilePic: string;
    isLoading: boolean;
    token: any;
    headers: HttpHeaders;
    isHalfDayLeave: boolean;
    isOneDayLeave: boolean;
    singleDate: string;
    fromDate: string;
    toDate: string;
    from: string;
    isShowReason: boolean;

    constructor(private router: Router, private userService: UserService, private http: HttpClient, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.sessionYear = Values.SESSION;
        this.reasonHint = "Enter a reason";
        this.reasonBorderColor = "black";
        this.reason = "";
        this.leave = new Leave();
        this.userId = "";
        this.profilePic = "res://profile";
        this.isLoading = false;
        this.userService.activeScreen("applyLeave");
        this.isHalfDayLeave = true;
        this.isOneDayLeave = true;
        this.fromDate = "From Date";
        this.toDate = "To date";
        this.singleDate = "Select Date";
        this.from = "";
        this.day = date.getDate().toString();
        this.month = (date.getMonth() + 1).toString();
        this.year = date.getFullYear().toString();
        this.date = "";
        this.isShowReason = true;
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        if (localstorage.getItem("userId") != null && localstorage.getItem("userId") != undefined) {
            this.userId = localstorage.getItem("userId");
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        // this.router.navigate(['/homeStudent']);
        this.routerExtensions.back();
    }

    onReasonTextChanged(args) {
        this.reason = args.object.text;
        this.reasonBorderColor = "black";
        if (this.reason == "") {
            this.reasonBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onDatePickerDialogLoaded(args) {
        var datePickerDialog = <any>args.object;
        setTimeout(() => {
            if (datePickerDialog.android) {
                let nativeImageView = datePickerDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#D9D9D9'));
                nativeImageView.setElevation(10)
            } else if (datePickerDialog.ios) {
                let nativeImageView = datePickerDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onApplyLeave() {
        if (this.isHalfDayLeave == true) {
            if (this.reason == "") {
                alert("Please enter reason.");
            }
            else {
                this.leave.leaveType = "halfDay";
                this.leave.reason = this.reason;
                this.leave.userId = this.userId;
                this.applyLeave();
            }
        }
        else {
            this.leave.leaveType = "fullDay";
            if (this.isOneDayLeave == true) {
                this.leave.fullDayType = "single"
            } else {
                this.leave.fullDayType = "multiple";
            }
            this.leave.fromDate = this.fromDate;
            this.leave.toDate = this.toDate;
            this.leave.reason = this.reason;
            this.leave.userId = this.userId;
            this.applyLeave();
        }
    }

    applyLeave() {
        this.isLoading = true;
        this.http
            .post("http://18.136.196.186:7000/api/leaves", this.leave, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        localstorage.setItem("leaveId", res.data.id);
                        this.isLoading = false;
                        Toast.makeText("Leave is applied successfully", "long").show();
                        this.routerExtensions.navigate(['/appliedLeave']);
                    }
                }
            }, error => {
                // this.userService.showLoadingState(false);
                this.isLoading = false;
                if (error.error.error == "date_already_passed") {
                    if (this.leave.leaveType == "fullDay") {
                        alert("For today, leave can be applied before 8am");
                    } else {
                        alert("For today, leave can be applied before 2pm");
                    }
                }
                if (error.error.error == "already_applied") {
                    alert("Sorry, leave has been already applied.");
                }
                else {
                    console.log(error.error.error);
                }
            });
    }

    onViewLeaves() {
        this.routerExtensions.navigate(['/appliedLeave']);
    }

    onHalfDayLeave() {
        this.isHalfDayLeave = true;
        this.isShowReason = true;
    }

    onFullDayLeave() {
        this.isHalfDayLeave = false;
        this.isShowReason = false;
    }

    onOneDayLeave() {
        this.isOneDayLeave = true;
        this.singleDate = "Select Date";
        this.isShowReason = false;
    }

    onMultipleDaysLeave() {
        this.isOneDayLeave = false;
        this.fromDate = "From Date";
        this.toDate = "To date";
        this.isShowReason = false;
    }

    onPickerLoaded(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.year = date.getFullYear();
        datePicker.month = date.getMonth() + 1;
        datePicker.day = date.getDate();
        datePicker.minDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        datePicker.maxDate = new Date(2021, 2, 31);
    }

    onDayChanged(args) {
        this.day = args.value;
    }

    onMonthChanged(args) {
        this.month = args.value;
    }

    onYearChanged(args) {
        this.year = args.value;
    }

    onOkDate() {
        this.datePickerDialog.hide();
        console.log(this.month.length);
        if (this.day.length < 2) {
            this.day = "0" + this.day;
        }
        if (this.month.length < 2) {
            this.month = "0" + this.month;
        }
        this.date = this.day + "/" + this.month + "/" + this.year;
        console.log(this.date);
        if (this.from == "single") {
            this.singleDate = this.date;
            this.fromDate = this.date;
            this.toDate = this.date;
            this.isShowReason = true;
        }
        if (this.from == "from") {
            this.fromDate = this.date;
        }
        if (this.from == "to") {
            this.toDate = this.date;
            this.isShowReason = true;
        }
    }

    onSingleDate() {
        this.datePickerDialog.show();
        this.from = "single";
    }

    onFromDate() {
        this.datePickerDialog.show();
        this.from = "from";
    }

    onToDate() {
        this.datePickerDialog.show();
        this.from = "to";
    }

}