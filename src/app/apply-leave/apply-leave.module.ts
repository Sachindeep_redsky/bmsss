import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ApplyLeaveComponent } from "./components/apply-leave.component";
import { ApplyLeaveRoutingModule } from "./apply-leave-routing.module";
import { GridViewModule } from 'nativescript-grid-view/angular';
import { NgModalModule } from "../modals/ng-modal";
// import { NgShadowModule } from 'nativescript-ng-shadow';


@NgModule({
    imports: [
        HttpModule,
        ApplyLeaveRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NgModalModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        ApplyLeaveComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class ApplyLeaveModule { }
