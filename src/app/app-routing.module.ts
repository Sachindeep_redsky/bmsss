import { NgModule } from "@angular/core";
import { Routes, PreloadAllModules } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
   { path: "", redirectTo: "/login", pathMatch: "full" },
   { path: "login", loadChildren: "~/app/login/login.module#LoginModule" },
   { path: "forgotPassword", loadChildren: "~/app/forgotpassword/forgotpassword.module#ForgotPasswordModule" },
   { path: "changePassword", loadChildren: "~/app/change-password/change-password.module#ChangePasswordModule" },
   { path: "otp", loadChildren: "~/app/otp/otp.module#OTPModule" },
   { path: "homeStudent", loadChildren: "~/app/home-student/home-student.module#HomeStudentModule" },
   { path: "homeTeacher", loadChildren: "~/app/home-teacher/home-teacher.module#HomeTeacherModule" },
   { path: "homeAdmin", loadChildren: "~/app/home-admin/home-admin.module#HomeAdminModule" },
   { path: "profileStudent", loadChildren: "~/app/profile-student/profile-student.module#ProfileStudentModule" },
   { path: "profileTeacher", loadChildren: "~/app/profile-teacher/profile-teacher.module#ProfileTeacherModule" },
   { path: "downloadSyllabus", loadChildren: "~/app/download-syllabus/download-syllabus.module#DownloadSyllabusModule" },
   { path: "uploadSyllabus", loadChildren: "~/app/upload-syllabus/upload-syllabus.module#UploadSyllabusModule" },
   { path: "viewHomework", loadChildren: "~/app/view-homework/view-homework.module#ViewHomeworkModule" },
   { path: "homework", loadChildren: "~/app/homework/homework.module#HomeworkModule" },
   { path: "addHomework", loadChildren: "~/app/add-homework/add-homework.module#AddHomeworkModule" },
   { path: "addHolidays", loadChildren: "~/app/add-holidays/add-holidays.module#AddHolidaysModule" },
   { path: "addClass", loadChildren: "~/app/add-class/add-class.module#AddClassModule" },
   { path: "addSubject", loadChildren: "~/app/add-subject/add-subject.module#AddSubjectModule" },
   { path: "addSection", loadChildren: "~/app/add-section/add-section.module#AddSectionModule" },
   { path: "calender", loadChildren: "~/app/calender/calender.module#CalenderModule" },
   { path: "birthdays", loadChildren: "~/app/birthdays/birthdays.module#BirthdaysModule" },
   { path: "teachers", loadChildren: "~/app/teachers/teachers.module#TeachersModule" },
   { path: "transport", loadChildren: "~/app/transport/transport.module#TransportModule" },
   { path: "applyLeave", loadChildren: "~/app/apply-leave/apply-leave.module#ApplyLeaveModule" },
   { path: "appliedLeave", loadChildren: "~/app/applied-leave/applied-leave.module#AppliedLeaveModule" },
   { path: "viewLeave", loadChildren: "~/app/view-leave/view-leave.module#ViewLeaveModule" },
   { path: "viewLeaves", loadChildren: "~/app/view-leaves/view-leaves.module#ViewLeavesModule" },
   { path: "uploadImage", loadChildren: "~/app/upload-image/upload-image.module#UploadImageModule" },
   { path: "downloadImage", loadChildren: "~/app/download-image/download-image.module#DownloadImageModule" },
   { path: "viewImages", loadChildren: "~/app/view-images/view-images.module#ViewImagesModule" },
   { path: "viewImagesFolder", loadChildren: "~/app/view-images-folder/view-images-folder.module#ViewImagesFolderModule" },
   { path: "addImages", loadChildren: "~/app/add-images/add-images.module#AddImagesModule" },
   { path: "addImagesFolder", loadChildren: "~/app/add-images-folder/add-images-folder.module#AddImagesFolderModule" },
   { path: "fees", loadChildren: "~/app/fees/fees.module#FeesModule" },
   { path: "addFees", loadChildren: "~/app/add-fees/add-fees.module#AddFeesModule" },
   { path: "messages", loadChildren: "~/app/messages/messages.module#MessagesModule" },
   { path: "messageCompose", loadChildren: "~/app/message-compose/message-compose.module#MessageComposeModule" },
   { path: "feeDetail", loadChildren: "~/app/fee-detail/fee-detail.module#FeeDetailModule" },
   { path: "feeReceipts", loadChildren: "~/app/fee-receipts/fee-receipts.module#FeeReceiptsModule" },
   { path: "feeCharges", loadChildren: "~/app/fee-charges/fee-charges.module#FeeChargesModule" },
   { path: "attendanceTeacher", loadChildren: "~/app/attendance-teacher/attendance-teacher.module#AttendanceTeacherModule" },
   { path: "attendanceStudent", loadChildren: "~/app/attendance-student/attendance-student.module#AttendanceStudentModule" },
   { path: "attendanceAdmin", loadChildren: "~/app/attendance-admin/attendance-admin.module#AttendanceAdminModule" },
];

@NgModule({
   imports: [NativeScriptRouterModule.forRoot(
      <any>routes, { preloadingStrategy: PreloadAllModules })],
   exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }