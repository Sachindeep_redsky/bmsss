import { UserService } from './../../services/user.service';
import { session } from 'nativescript-background-http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import * as localstorage from "nativescript-localstorage";
import { Page } from "tns-core-modules/ui/page/page";
import { Values } from "~/app/values/values";
import * as Toast from 'nativescript-toast';
import { ModalComponent } from '~/app/modals/modal.component';

declare const android: any;
declare const CGSizeMake: any;
var date = new Date();

@Component({
    selector: "app-calender",
    moduleId: module.id,
    templateUrl: "./calender.component.html",
    styleUrls: ['./calender.component.css'],
})

export class CalenderComponent implements OnInit {
    @ViewChild('deleteHolidayDialog', { static: false }) deleteHolidayDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    holidays = [];
    sessionYear: string;
    isVisibleAddButton: boolean;
    height: string;
    marginTop: string;
    isLoading: boolean;
    token: string;
    headers: HttpHeaders;
    year: string;
    query: string;
    isVisibleDeleteButton: boolean;
    profilePic: string;

    constructor(private routerExtensions: RouterExtensions, private page: Page, private http: HttpClient, private userService: UserService) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.sessionYear =  Values.SESSION;
        this.year = date.getFullYear().toString();
        this.isLoading = false;
        this.token = "";
        this.query = "";
        this.isVisibleDeleteButton = false;
        this.profilePic = "";
        this.userService.activeScreen("calender");

        if (localstorage.getItem("userType") == "admin") {
            this.isVisibleAddButton = true;
            this.isVisibleDeleteButton = true;
            this.height = "55%";
            this.marginTop = "25%";
        }
        else {
            this.isVisibleAddButton = false;
            this.isVisibleDeleteButton = false;
            this.height = "65%";
            this.marginTop = "35%";
        }

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.year = date.getFullYear().toString();
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        this.getHolidays();
        this.page.on('navigatedTo', (data) => {
            if (data.isBackNavigation) {
                if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
                    this.token = localstorage.getItem("token");
                    console.log(this.token);
                    this.headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "x-access-token": this.token
                    });
                }
                this.getHolidays();
            }
        });
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
        // this.holidays.push({ date: "07 April", color: "#6D7CF1", type: "holiday", reason: "Holiday of sunday", });
        // this.holidays.push({ date: "14 April", color: "#6D7CF1", type: "holiday", reason: "Holiday of sunday", });
        // this.holidays.push({ date: "16 April", color: "#28FE07", type: "event", reason: "Fresher's party", });
        // this.holidays.push({ date: "21 April", color: "#6D7CF1", type: "holiday", reason: "Holiday of sunday", });
        // this.holidays.push({ date: "28 April", color: "#6D7CF1", type: "holiday", reason: "Holiday of sunday", });
        // this.holidays.push({ date: "05 May", color: "#6D7CF1", type: "holiday", reason: "Holiday of sunday)", });
        // this.holidays.push({ date: "12 May", color: "#6D7CF1", type: "holiday", reason: "Holiday of sunday", });
        // this.holidays.push({ date: "15 May", color: "#28FE07", type: "event", reason: "Cultural function from 9am to 3pm", });
        // this.holidays.push({ date: "19 May", color: "#6D7CF1", type: "holiday", reason: "Holiday of sunday", });
        // this.holidays.push({ date: "26 May", color: "#6D7CF1", type: "holiday", reason: "Holiday of sunday", });

    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onDeleteHolidayDialogLoaded(args) {
        var deleteHolidayDialog = <any>args.object;
        setTimeout(() => {
            if (deleteHolidayDialog.android) {
                let nativeImageView = deleteHolidayDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (deleteHolidayDialog.ios) {
                let nativeImageView = deleteHolidayDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onBack() {
        // if (localstorage.getItem("userType") == "teacher") {
        //     this.routerExtensions.navigate(['/homeTeacher']);
        // }
        // else if (localstorage.getItem("userType") == "student") {
        //     this.routerExtensions.navigate(['/homeStudent']);
        // }
        // else {
        //     this.routerExtensions.navigate(['/homeAdmin']);
        // }
        this.routerExtensions.back();
    }

    onAddHolidays() {
        this.routerExtensions.navigate(['/addHolidays']);
    }

    onPressHoliday(item: any) {
        if (localstorage.getItem("userType") == "admin") {
            this.query = "year=" + item.year + "&month=" + item.month + "&id=" + item._id;
            console.log(this.query);
            this.deleteHolidayDialog.show();
        }
    }

    onDelete() {
        this.isLoading = true;
        this.http
            .delete(Values.BASE_URL + "holidays/delete?" + this.query, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.isLoading = false;
                        Toast.makeText("Holiday is deleted successfully.", "long").show();
                        this.deleteHolidayDialog.hide();
                        this.holidays = [];
                        this.getHolidays();
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onCancel() {
        this.deleteHolidayDialog.hide();
    }

    getHolidays() {
        // this.isLoading = true;
        console.log("URLLLLLL::::", Values.BASE_URL + "holidays?year=" + this.year);
        this.http
            .get(Values.BASE_URL + "holidays?year=" + this.year, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.trace(res)
                        // this.isLoading = false;
                        if (res.data.january.length > 0) {
                            for (var i = 0; i < res.data.january.length; i++) {
                                if (res.data.january[i].fromDate == res.data.january[i].toDate) {
                                    var date = res.data.january[i].fromDate;
                                }
                                else {
                                    date = res.data.january[i].fromDate + "-" + res.data.january[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.january[i].description,
                                    "isEvent": res.data.january[i].isEvent,
                                    "_id": res.data.january[i]._id,
                                    "month": "january",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.february.length > 0) {
                            for (var i = 0; i < res.data.february.length; i++) {
                                if (res.data.february[i].fromDate == res.data.february[i].toDate) {
                                    var date = res.data.february[i].fromDate;
                                }
                                else {
                                    date = res.data.february[i].fromDate + "-" + res.data.february[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.february[i].description,
                                    "isEvent": res.data.february[i].isEvent,
                                    "_id": res.data.february[i]._id,
                                    "month": "february",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.march.length > 0) {
                            for (var i = 0; i < res.data.march.length; i++) {
                                if (res.data.march[i].fromDate == res.data.march[i].toDate) {
                                    var date = res.data.march[i].fromDate;
                                }
                                else {
                                    date = res.data.march[i].fromDate + "-" + res.data.march[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.march[i].description,
                                    "isEvent": res.data.march[i].isEvent,
                                    "_id": res.data.march[i]._id,
                                    "month": "march",
                                    "year": res.data.year,

                                })
                            }
                        }
                        if (res.data.april.length > 0) {
                            for (var i = 0; i < res.data.april.length; i++) {
                                if (res.data.april[i].fromDate == res.data.april[i].toDate) {
                                    var date = res.data.april[i].fromDate;
                                }
                                else {
                                    date = res.data.april[i].fromDate + "-" + res.data.april[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.april[i].description,
                                    "isEvent": res.data.april[i].isEvent,
                                    "_id": res.data.april[i]._id,
                                    "month": "april",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.may.length > 0) {
                            for (var i = 0; i < res.data.may.length; i++) {
                                if (res.data.may[i].fromDate == res.data.may[i].toDate) {
                                    var date = res.data.may[i].fromDate;
                                }
                                else {
                                    date = res.data.may[i].fromDate + "-" + res.data.may[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.may[i].description,
                                    "isEvent": res.data.may[i].isEvent,
                                    "_id": res.data.may[i]._id,
                                    "month": "may",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.june.length > 0) {
                            for (var i = 0; i < res.data.june.length; i++) {
                                if (res.data.june[i].fromDate == res.data.june[i].toDate) {
                                    var date = res.data.june[i].fromDate;
                                }
                                else {
                                    date = res.data.june[i].fromDate + "-" + res.data.june[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.june[i].description,
                                    "isEvent": res.data.june[i].isEvent,
                                    "_id": res.data.june[i]._id,
                                    "month": "june",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.july.length > 0) {
                            for (var i = 0; i < res.data.july.length; i++) {
                                if (res.data.july[i].fromDate == res.data.july[i].toDate) {
                                    var date = res.data.july[i].fromDate;
                                }
                                else {
                                    date = res.data.july[i].fromDate + "-" + res.data.july[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.july[i].description,
                                    "isEvent": res.data.july[i].isEvent,
                                    "_id": res.data.july[i]._id,
                                    "month": "july",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.august.length > 0) {
                            for (var i = 0; i < res.data.august.length; i++) {
                                if (res.data.august[i].fromDate == res.data.august[i].toDate) {
                                    var date = res.data.august[i].fromDate;
                                }
                                else {
                                    date = res.data.august[i].fromDate + "-" + res.data.august[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.august[i].description,
                                    "isEvent": res.data.august[i].isEvent,
                                    "_id": res.data.august[i]._id,
                                    "month": "august",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.september.length > 0) {
                            for (var i = 0; i < res.data.september.length; i++) {
                                if (res.data.september[i].fromDate == res.data.september[i].toDate) {
                                    var date = res.data.september[i].fromDate;
                                }
                                else {
                                    date = res.data.september[i].fromDate + "-" + res.data.september[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.september[i].description,
                                    "isEvent": res.data.september[i].isEvent,
                                    "_id": res.data.september[i]._id,
                                    "month": "september",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.october.length > 0) {
                            for (var i = 0; i < res.data.october.length; i++) {
                                if (res.data.october[i].fromDate == res.data.october[i].toDate) {
                                    var date = res.data.october[i].fromDate;
                                }
                                else {
                                    date = res.data.october[i].fromDate + "-" + res.data.october[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.october[i].description,
                                    "isEvent": res.data.october[i].isEvent,
                                    "_id": res.data.october[i]._id,
                                    "month": "october",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.november.length > 0) {
                            for (var i = 0; i < res.data.november.length; i++) {
                                if (res.data.november[i].fromDate == res.data.november[i].toDate) {
                                    var date = res.data.november[i].fromDate;
                                }
                                else {
                                    date = res.data.november[i].fromDate + " - " + res.data.november[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.november[i].description,
                                    "isEvent": res.data.november[i].isEvent,
                                    "_id": res.data.november[i]._id,
                                    "month": "november",
                                    "year": res.data.year,
                                })

                            }
                        }
                        if (res.data.december.length > 0) {
                            for (var i = 0; i < res.data.december.length; i++) {
                                if (res.data.december[i].fromDate == res.data.december[i].toDate) {
                                    var date = res.data.december[i].fromDate;
                                }
                                else {
                                    date = res.data.december[i].fromDate + "-" + res.data.december[i].toDate;
                                }
                                this.holidays.push({
                                    "date": date,
                                    "reason": res.data.december[i].description,
                                    "isEvent": res.data.december[i].isEvent,
                                    "_id": res.data.december[i]._id,
                                    "month": "december",
                                    "year": res.data.year,
                                })

                            }
                        }
                    }
                }
            }, error => {
                // this.isLoading = false;
                console.log(error.error.error);
            });
    }
}