import { UserService } from './../../services/user.service';
import { Fees } from '../../models/fees.model';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { ActivatedRoute } from "@angular/router";
import { Values } from '~/app/values/values';
import * as localstorage from "nativescript-localstorage";
import { ModalComponent } from '~/app/modals/modal.component';
import * as Toast from 'nativescript-toast';

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-feeReceipts",
    moduleId: module.id,
    templateUrl: "./fee-receipts.component.html",
    styleUrls: ['./fee-receipts.component.css'],
})

export class FeeReceiptsComponent implements OnInit {

    @ViewChild('receiptDialog', { static: false }) receiptDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    feeReceipt = [];
    year: string;
    isLoading: boolean;
    admissionNo: string;
    token: string;
    headers: HttpHeaders;
    fees: Fees;
    dialogCancelButton: string;
    dialogPayButton: string;
    fess: Fees;
    receiptNumber: string;
    feeCharges: string;
    transType: string;
    transDate: string;
    dueAmount: string;
    payAmount: string;
    balanceAmount: string;
    bankName: string;
    remark: string;
    transRemark: string;
    isCheque: boolean;
    profilePic: string;
    constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private page: Page, private http: HttpClient) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.isLoading = false;
        this.admissionNo = "";
        this.token = "";
        this.dialogCancelButton = "Cancel";
        this.dialogPayButton = "Pay";
        this.fees = new Fees();
        this.receiptNumber = "";
        this.feeCharges = "";
        this.transType = "";
        this.transDate = "";
        this.dueAmount = "";
        this.payAmount = "";
        this.balanceAmount = "";
        this.bankName = "";
        this.remark = "";
        this.transRemark = "";
        this.isCheque = false;
        this.profilePic = "";
        this.userService.activeScreen("feeReceipts");

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
        }
        this.route.queryParams.subscribe(params => {
            this.admissionNo = params["admissionNo"];
        });
        this.getReceipts();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
        // this.feeDetail.push({ timePeriod: "QUARTER 1", totalFee: "6250", paidFee: "6250", dueFee: "0", date: "01-04-2019" });
        // this.feeDetail.push({ timePeriod: "QUARTER 2", totalFee: "6250", paidFee: "0", dueFee: "6250", date: "01-07-2019" });
        // this.feeDetail.push({ timePeriod: "QUARTER 3", totalFee: "3500", paidFee: "0", dueFee: "3500", date: "01-10-2019" });
        // this.feeDetail.push({ timePeriod: "QUARTER 4", totalFee: "3500", paidFee: "0", dueFee: "3500", date: "01-01-2019" });
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onReceiptDialogLoaded(args) {
        var receiptDialog = <any>args.object;
        setTimeout(() => {
            if (receiptDialog.android) {
                let nativeImageView = receiptDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(10)
            } else if (receiptDialog.ios) {
                let nativeImageView = receiptDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    getReceipts() {
        this.isLoading = true;
        this.fees.admissionNo = this.admissionNo;
        this.http
            .get(Values.BASE_URL + "feeReceipts?admissionNo=" + this.admissionNo, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        console.trace(res);
                        this.isLoading = false;
                        if (res.data.length > 0) {
                            this.feeReceipt = [];
                            for (var i = 0; i < res.data.length; i++) {
                                var dateTime = new Date(res.data[i].transDate);
                                var date = dateTime.getDate().toString() + "/" + (dateTime.getMonth() + 1).toString() + "/" + dateTime.getFullYear().toString();
                                this.feeReceipt.push({
                                    "feeCharges": res.data[i].feeCharges,
                                    "payAmount": res.data[i].payAmount,
                                    "receiptNo": res.data[i].receiptNo,
                                    "balanceAmount": res.data[i].balanceAmount,
                                    "remark": res.data[i].remark,
                                    "transRemark": res.data[i].transRemark,
                                    "transType": res.data[i].transType,
                                    "transDate": date,
                                    "dueAmount": res.data[i].dueAmount,
                                    "bankName": res.data[i].bankName
                                })
                            }
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onCancel() {
        this.receiptDialog.hide();
    }

    onBack() {
        this.routerExtensions.back();
    }

    onReceipt(item: any) {
        this.receiptNumber = item.receiptNo;
        this.feeCharges = item.feeCharges;
        this.payAmount = item.payAmount;
        this.balanceAmount = item.balanceAmount;
        this.remark = item.remark;
        this.transRemark = item.transRemark;
        if (item.transType == "cheque") {
            this.transType = item.transType;
            this.isCheque = true;
        }
        else {
            this.transType = "cash";
            this.isCheque = false;
        }
        this.transDate = item.transDate;
        this.dueAmount = item.dueAmount;
        this.bankName = item.bankName;
        this.receiptDialog.show();
    }

}