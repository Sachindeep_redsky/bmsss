import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { FeeReceiptsComponent } from "./components/fee-receipts.component";

const routes: Routes = [
    { path: "", component: FeeReceiptsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class FeeReceiptsRoutingModule { }
