import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ProfileTeacherComponent } from "./components/profile-teacher.component";
import { ProfileTeacherRoutingModule } from "./profile-teacher-routing.module";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from 'nativescript-ng-shadow';


@NgModule({
    imports: [
        HttpModule,
        ProfileTeacherRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        ProfileTeacherComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class ProfileTeacherModule { }
