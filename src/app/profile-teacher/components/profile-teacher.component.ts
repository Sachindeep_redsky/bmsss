import { base64 } from 'base-64';
import { RouterExtensions } from 'nativescript-angular/router';
import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { UserService } from "~/app/services/user.service";
import { Page } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-profileStudent",
    moduleId: module.id,
    templateUrl: "./profile-teacher.component.html",
    styleUrls: ['./profile-teacher.component.css'],
})

export class ProfileTeacherComponent implements OnInit {


    renderViewTimeout;
    isRendering: boolean;
    username: string;
    registrationNo: string;
    DOB: string;
    class: string;
    section: string;
    contactNumber: string;
    emailId: string;
    address: string;
    profilePic: string;
    token: string;
    isLoading: boolean;
    isAdmin: boolean;
    classIncharge: string;
    subjectName: string;
    classes: string;
    isIncharge: string;
    constructor(private router: Router, private userService: UserService, private http: HttpClient, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.username = ""
        this.registrationNo = "";
        this.DOB = "";
        this.class = "";
        this.section = "";
        this.contactNumber = "";
        this.emailId = ""
        this.address = ""
        this.token = "";
        this.isLoading = false;
        this.isAdmin = false;
        this.classIncharge = "";
        this.subjectName = "";
        this.classes = "";
        this.isIncharge = "";
        this.userService.activeScreen("profileTeacher");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            let headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            this.isLoading = true;
            if (localstorage.getItem("userType") == "admin") {
                this.isAdmin = true;
            }
            this.http
                .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"), {
                    headers: headers
                })
                .subscribe((res: any) => {
                    if (res != null && res != undefined) {
                        if (res.isSuccess == true) {
                            this.username = res.data.name;
                            this.registrationNo = res.data.regNo;
                            this.DOB = res.data.dateOfBirth;
                            if (localstorage.getItem("userType") == "teacher") {
                                if (res.data.classIncharge == "true") {
                                    this.class = res.data.className;
                                    this.section = res.data.classSection;
                                    this.classIncharge = this.class + " (" + this.section + ")";
                                }
                                else {
                                    // this.isAdmin = true;
                                    this.isIncharge = "collapse";
                                }
                                this.subjectName = res.data.subjectTeacher;
                                for (var i = 0; i < res.data.classTeacher.length; i++) {
                                    var className = res.data.classTeacher[i].name + "(";
                                    for (var j = 0; j < res.data.classTeacher[i].section.length; j++) {
                                        className = className + res.data.classTeacher[i].section[j].classSection;
                                        console.log(className);
                                        if (j < res.data.classTeacher[i].section.length - 1) {
                                            className = className + ", ";
                                        }
                                    }
                                    this.classes = this.classes + className + ")";
                                    if (i < res.data.classTeacher.length - 1) {
                                        this.classes = this.classes + ", "
                                    }
                                }
                                // } else {
                                //     this.class = res.data.className;
                                //     this.section = res.data.classSection;
                                //     this.classIncharge = this.class + " (" + this.section + ")";
                                // }
                            }
                            this.contactNumber = res.data.residenceMobile;
                            this.emailId = res.data.email;
                            this.address = res.data.residenceAddress1;
                            // this.profilePic = res.data.imageUrl;
                            // this.userService.showLoadingState(false);
                            this.isLoading = false;
                        }
                    }
                }, error => {
                    // this.userService.showLoadingState(false);
                    this.isLoading = false;
                    console.log(error.error.error);
                });
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onChangePassword() {
        this.routerExtensions.navigate(['/changePassword']);
    }

    onLogout() {
        localStorage.removeItem("userId");
        this.router.navigate(['/login']);
    }

    onBack() {
        this.routerExtensions.back();
        // if (localStorage.getItem("userType") != null && localstorage.getItem("userType") == "teacher") {
        //     this.router.navigate(['/homeTeacher']);
        // } else {
        //     this.router.navigate(['/homeAdmin']);
        // }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onGridTopLoaded(args: any) {
        var gridTop = <any>args.object;

        setTimeout(() => {
            if (gridTop.android) {
                let nativeGridMain = gridTop.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#A61F21'));
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridTop.ios) {
                let nativeGridMain = gridTop.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }

            // this.changeDetector.detectChanges();
        }, 400)

    }

    onGridMiddleLoaded(args: any) {
        var gridMiddle = <any>args.object;

        setTimeout(() => {
            if (gridMiddle.android) {
                let nativeGridMain = gridMiddle.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                shape.setCornerRadius(10)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridMiddle.ios) {
                let nativeGridMain = gridMiddle.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }

            // this.changeDetector.detectChanges();
        }, 400)

    }

    onGridBottomLoaded(args: any) {
        var gridBottom = <any>args.object;

        setTimeout(() => {
            if (gridBottom.android) {
                let nativeGridMain = gridBottom.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                shape.setCornerRadius(10)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(20)
            } else if (gridBottom.ios) {
                let nativeGridMain = gridBottom.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }

            // this.changeDetector.detectChanges();
        }, 400)

    }
}