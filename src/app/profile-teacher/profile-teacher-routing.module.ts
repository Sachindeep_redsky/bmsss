import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ProfileTeacherComponent } from "./components/profile-teacher.component";

const routes: Routes = [
    { path: "", component: ProfileTeacherComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ProfileTeacherRoutingModule { }
