import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AttendanceStudentComponent } from "./components/attendance-student.component";



const routes: Routes = [
    { path: "", component: AttendanceStudentComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AttendanceStudentRoutingModule { }