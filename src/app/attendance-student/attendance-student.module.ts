import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { AttendanceStudentRoutingModule } from "./attendance-student-routing.module";
import { AttendanceStudentComponent } from "./components/attendance-student.component";
import { NativeScriptUICalendarModule } from "nativescript-ui-calendar/angular"


@NgModule({
    imports: [
        HttpModule,
        AttendanceStudentRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUICalendarModule
    ],
    declarations: [
        AttendanceStudentComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AttendanceStudentModule { }