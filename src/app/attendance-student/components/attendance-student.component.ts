import { Values } from './../../values/values';
import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router"
import { HttpClient } from "@angular/common/http";
import { Color } from "tns-core-modules/color/color";
import { CalendarViewMode, CalendarSelectionShape, CalendarFontStyle, CalendarCellAlignment } from "nativescript-ui-calendar"
import { CalendarSelectionMode } from "nativescript-ui-calendar"
import { CalendarTransitionMode } from "nativescript-ui-calendar"
import { CalendarMonthViewStyle } from "nativescript-ui-calendar"
import { DayCellStyle } from "nativescript-ui-calendar"
import { Page } from "tns-core-modules/ui/page/page";
import * as localStorage from "nativescript-localstorage"
import { UserService } from "~/app/services/user.service";

declare const android: any;
declare const CGSizeMake: any;

class DayDate {
    dayDate: number;
    dayNum: number;
    status: string;

    constructor(obj?: any) {
        if (obj) {
            this.dayDate = obj.dayDate
            this.dayNum = obj.dayNum
            this.status = obj.status
        } else {
            this.dayDate = undefined;
            this.dayNum = undefined;
            this.status = undefined;
        }
    }
}

class Week {

    sun: DayDate;
    mon: DayDate;
    tue: DayDate;
    wed: DayDate;
    thu: DayDate;
    fri: DayDate;
    sat: DayDate;

    constructor(obj?: any) {

        if (obj) {
            this.sun = obj.sun;
            this.mon = obj.mon;
            this.tue = obj.tue;
            this.wed = obj.wed;
            this.thu = obj.thu;
            this.fri = obj.fri;
            this.sat = obj.sat;
        } else {
            this.sun = new DayDate();
            this.mon = new DayDate();
            this.tue = new DayDate();
            this.wed = new DayDate();
            this.thu = new DayDate();
            this.fri = new DayDate();
            this.sat = new DayDate();
        }

    }

}

@Component({
    selector: "app-attendancestudent",

    moduleId: module.id,
    templateUrl: "./attendance-student.component.html",
    styleUrls: ['./attendance-student.component.css'],
})

export class AttendanceStudentComponent {

    students;
    renderViewTimeout;
    isRendering: boolean;
    date: string;
    selectionMode;
    viewMode;
    transition;
    monthViewStyle;
    absentDayStyle;
    leaveDayStyle;
    holidayDayStyle;
    weeks;
    currentDate: Date;
    selectedDate: number;
    monthAttendance;

    query;

    isLoading: boolean;
    profilePic: string;
    presentDays: number;
    absentDays: number;
    holidays: number;
    leaveDays: number;
    year: string;
    totalWorkingDays: string;
    totalPresentDays: string;
    annualPercentage: string;
    disableDateRight: boolean;
    disableDateLeft: boolean;

    constructor(private routerExtensions: RouterExtensions, private http: HttpClient, private page: Page, private userService: UserService) {
        this.page.actionBarHidden = true;
        this.userService.activeScreen("attendanceStudent");

        this.profilePic = "res://profile";
        this.presentDays = 0;
        this.absentDays = 0;
        this.holidays = 0;
        this.leaveDays = 0;
        this.year =  Values.SESSION;
        this.totalPresentDays = "";
        this.totalWorkingDays = "";
        this.annualPercentage = "";

        var date = new Date();
        this.isRendering = false;
        this.isLoading = false;
        this.students = [];
        this.monthAttendance = [];
        this.selectionMode = CalendarSelectionMode.None;
        this.viewMode = CalendarViewMode.Month;
        this.transition = CalendarTransitionMode.Stack;

        this.monthViewStyle = new CalendarMonthViewStyle();
        this.monthViewStyle.backgroundColor = new Color('#696969');
        this.monthViewStyle.showTitle = true;
        this.monthViewStyle.showWeekNumbers = true;
        this.monthViewStyle.showDayNames = true;
        this.monthViewStyle.selectionShape = CalendarSelectionShape.Round;
        this.monthViewStyle.selectionShapeSize = 15;
        this.monthViewStyle.selectionShapeColor = new Color('#ff0000');



        const todayCellStyle = new DayCellStyle();
        todayCellStyle.cellBackgroundColor = new Color('#00ff00');
        todayCellStyle.cellBorderWidth = 2;
        todayCellStyle.cellBorderColor = new Color('#444444');
        // todayCellStyle.cellTextColor = this._darkBrownColor;
        // todayCellStyle.cellTextFontName = this._preferredFontName;
        // todayCellStyle.cellTextFontStyle = CalendarFontStyle.Bold;
        todayCellStyle.cellTextSize = 14;
        this.monthViewStyle.todayCellStyle = todayCellStyle;

        const dayCellStyle = new DayCellStyle();
        dayCellStyle.showEventsText = true;
        dayCellStyle.eventTextColor = new Color('#ffffff');
        // dayCellStyle.eventFontName = this._preferredFontName;
        dayCellStyle.eventFontStyle = CalendarFontStyle.BoldItalic;
        dayCellStyle.eventTextSize = 8;
        dayCellStyle.cellAlignment = CalendarCellAlignment.VerticalCenter;
        dayCellStyle.cellPaddingHorizontal = 10;
        dayCellStyle.cellPaddingVertical = 5;
        dayCellStyle.cellBackgroundColor = new Color('#008800');
        dayCellStyle.cellBorderWidth = 1;
        // dayCellStyle.cellBorderColor = this._lightYellowColor;
        // dayCellStyle.cellTextColor = this._brownColor;
        // dayCellStyle.cellTextFontName = this._preferredFontName;
        dayCellStyle.cellTextFontStyle = CalendarFontStyle.Bold;
        dayCellStyle.cellTextSize = 10;
        this.monthViewStyle.dayCellStyle = dayCellStyle;

        // const weekendCellStyle = new DayCellStyle();
        // weekendCellStyle.eventTextColor = new Color('#880088');
        // // weekendCellStyle.eventFontName = this._preferredFontName;
        // weekendCellStyle.eventFontStyle = CalendarFontStyle.BoldItalic;
        // weekendCellStyle.eventTextSize = 8;
        // weekendCellStyle.cellAlignment = CalendarCellAlignment.VerticalCenter;
        // weekendCellStyle.cellPaddingHorizontal = 10;
        // weekendCellStyle.cellPaddingVertical = 5;
        // weekendCellStyle.cellBackgroundColor = new Color('#666600');
        // weekendCellStyle.cellBorderWidth = 1;
        // // weekendCellStyle.cellBorderColor = this._lightYellowColor;
        // // weekendCellStyle.cellTextColor = this._brownColor;
        // // weekendCellStyle.cellTextFontName = this._preferredFontName;
        // weekendCellStyle.cellTextFontStyle = CalendarFontStyle.Bold;
        // weekendCellStyle.cellTextSize = 12;
        // this.monthViewStyle.weekendCellStyle = weekendCellStyle;

        const selectedCellStyle = new DayCellStyle();
        selectedCellStyle.eventTextColor = new Color('#0000ff88')
        // selectedCellStyle.eventFontName = this._preferredFontName;
        selectedCellStyle.eventFontStyle = CalendarFontStyle.Bold;
        selectedCellStyle.eventTextSize = 8;
        selectedCellStyle.cellAlignment = CalendarCellAlignment.VerticalCenter;
        selectedCellStyle.cellPaddingHorizontal = 10;
        selectedCellStyle.cellPaddingVertical = 5;
        selectedCellStyle.cellBackgroundColor = new Color('#ddff00');
        selectedCellStyle.cellBorderWidth = 2;
        // selectedCellStyle.cellBorderColor = this._brownColor;
        // selectedCellStyle.cellTextColor = this._blackColor;
        // selectedCellStyle.cellTextFontName = this._preferredFontName;
        selectedCellStyle.cellTextFontStyle = CalendarFontStyle.Bold;
        selectedCellStyle.cellTextSize = 18;
        this.monthViewStyle.selectedDayCellStyle = selectedCellStyle;

        this.disableDateRight = false;
        this.disableDateLeft = false;

        this.weeks = [];
        this.currentDate = new Date();
        this.selectedDate = date.getDate();
        this.query = '';
        this.date = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();

        var currentMonth = new Date().getMonth();
        // if (this.currentDate.getMonth() == currentMonth || this.currentDate.getMonth() == 2) {
        //     this.disableDateRight = true;
        // }
        this.disableDateRight = true;

        if (currentMonth == 3) {
            this.disableDateLeft = true;
        }

        if (date.getDate() < 10) {
            if ((date.getMonth() + 1) < 10) {
                this.date = '0' + date.getDate() + '-0' + date.getMonth() + '-' + date.getFullYear();
            }
            else {
                this.date = '0' + date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
            }
        }
        else {
            if ((date.getMonth() + 1) < 10) {
                this.date = date.getDate() + '-0' + date.getMonth() + '-' + date.getFullYear();
            }
            else {
                this.date = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
            }
        }
        this.getStudentAttendance();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localStorage.getItem("profilePic") != null && localStorage.getItem("profilePic") != undefined) {
            this.profilePic = localStorage.getItem("profilePic");
        }
    }

    onBack() {
        this.routerExtensions.back();
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    public whichOneChanged(args) {
        console.log("ARGS:::", args)
    }

    getStudentAttendance() {
        this.isLoading = true;
        this.monthAttendance = [];
        let headers = {
            'Content-Type': 'application/json',
            'x-access-token': localStorage.getItem('token')
        }

        this.query = `monthYear=${this.currentDate.getMonth() + 1}/${this.currentDate.getFullYear()}`

        this.http.get(Values.BASE_URL + `attendanceRecords/search?${this.query}`, { headers: headers }).subscribe((res: any) => {
            console.trace('RES:::', res);
            this.monthAttendance = res.data.attendance;
            this.createCalendarModal();
            this.totalWorkingDays = res.data.schoolWorkingDays;
            this.totalPresentDays = res.data.studentWorkingDays;
            this.annualPercentage = res.data.annualPercentage;
            var annualPercentage = parseInt(this.annualPercentage);
            this.annualPercentage = annualPercentage.toPrecision(4);
            this.presentDays = 0;
            this.absentDays = 0;
            this.holidays = 0;
            this.leaveDays = 0;
            if (res.data.attendance.length > 0) {
                for (var i = 0; i < res.data.attendance.length; i++) {
                    if (res.data.attendance[i].status == "absent") {
                        this.absentDays++;
                    }
                    else if (res.data.attendance[i].status == "holiday") {
                        this.holidays++;
                    }
                    else if (res.data.attendance[i].status == "leave") {
                        this.leaveDays++;
                    }
                    else if (res.data.attendance[i].status == "present") {
                        this.presentDays++;
                    }
                }
            }
            setTimeout(() => {
                this.isLoading = false;
            }, 10)

        }, error => {
            console.log('Error:::', error);
            setTimeout(() => {
                this.isLoading = false;
            }, 10)

        })
    }


    createCalendarModal() {
        this.weeks = [];
        var tempDate = new Date()
        var totalMonthDays = this.getNumberOfDays(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1);
        var firstDay = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1).getDay();
        var firstWeek = new Week();
        var lastWeek = new Week();
        var intermediateWeeks = new Week();
        var firstDayDateOfSecondWeek;

        for (var i = firstDay; i < 7; i++) {
            switch (i) {
                case 0:
                    firstWeek.sun.dayNum = 7;                           //It can not be zero as ngIf considers it false
                    firstWeek.sun.dayDate = i + 1 - firstDay;
                    break;
                case 1:
                    firstWeek.mon.dayNum = 1;
                    firstWeek.mon.dayDate = i + 1 - firstDay;
                    break;
                case 2:
                    firstWeek.tue.dayNum = 2;
                    firstWeek.tue.dayDate = i + 1 - firstDay;
                    break;
                case 3:
                    firstWeek.wed.dayNum = 3;
                    firstWeek.wed.dayDate = i + 1 - firstDay;
                    break;
                case 4:
                    firstWeek.thu.dayNum = 4;
                    firstWeek.thu.dayDate = i + 1 - firstDay;
                    break;
                case 5:
                    firstWeek.fri.dayNum = 5;
                    firstWeek.fri.dayDate = i + 1 - firstDay;
                    break;
                case 6:
                    firstDayDateOfSecondWeek = 6 - firstDay + 2;
                    firstWeek.sat.dayNum = 6;
                    firstWeek.sat.dayDate = i + 1 - firstDay;
                    break;
            }
        }

        this.weeks.push(firstWeek);

        var lastDay = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), totalMonthDays).getDay();
        // console.log('NOD:::', typeof (lastDay));

        var lastDayDateOfSecondLastWeek;

        for (var i = lastDay; i >= 0; i--) {
            switch (i) {
                case 0:
                    lastWeek.sun.dayNum = 7;
                    lastWeek.sun.dayDate = totalMonthDays - lastDay + i;
                    lastDayDateOfSecondLastWeek = totalMonthDays - lastDay + i - 1;
                    break;
                case 1:
                    lastWeek.mon.dayNum = 1;
                    lastWeek.mon.dayDate = totalMonthDays - lastDay + i;

                    break;
                case 2:
                    lastWeek.tue.dayNum = 2;
                    lastWeek.tue.dayDate = totalMonthDays - lastDay + i;

                    break;
                case 3:
                    lastWeek.wed.dayNum = 3;
                    lastWeek.wed.dayDate = totalMonthDays - lastDay + i;

                    break;
                case 4:
                    lastWeek.thu.dayNum = 4;
                    lastWeek.thu.dayDate = totalMonthDays - lastDay + i;

                    break;
                case 5:
                    lastWeek.fri.dayNum = 5;
                    lastWeek.fri.dayDate = totalMonthDays - lastDay + i;

                    break;
                case 6:
                    lastWeek.sat.dayNum = 6;
                    lastWeek.sat.dayDate = totalMonthDays - lastDay + i;
                    // console.log("INNN:::", lastDayDateOfSecondLastWeek, typeof (lastDayDateOfSecondLastWeek))

                    break;
            }

        }

        // console.log('NOD:::', typeof (lastDay), typeof (totalMonthDays));

        var numberOfIntermediateWeeks = (lastDayDateOfSecondLastWeek + 1 - firstDayDateOfSecondWeek) / 7;
        // console.log('NOW:::', numberOfIntermediateWeeks, typeof (lastDayDateOfSecondLastWeek), typeof (firstDayDateOfSecondWeek))
        var firstDayDateOfNextWeek = firstDayDateOfSecondWeek;

        for (var w = 1; w <= numberOfIntermediateWeeks; w++) {
            var currentWeek: Week = new Week();
            for (var i = 0; i < 7; i++) {
                switch (i) {
                    case 0:
                        currentWeek.sun.dayNum = 7;
                        currentWeek.sun.dayDate = firstDayDateOfNextWeek;
                        break;
                    case 1:
                        currentWeek.mon.dayNum = 1;
                        currentWeek.mon.dayDate = firstDayDateOfNextWeek + 1;
                        break;
                    case 2:
                        currentWeek.tue.dayNum = 2;
                        currentWeek.tue.dayDate = firstDayDateOfNextWeek + 2;
                        break;
                    case 3:
                        currentWeek.wed.dayNum = 3;
                        currentWeek.wed.dayDate = firstDayDateOfNextWeek + 3;
                        break;
                    case 4:
                        currentWeek.thu.dayNum = 4;
                        currentWeek.thu.dayDate = firstDayDateOfNextWeek + 4;
                        break;
                    case 5:
                        currentWeek.fri.dayNum = 5;
                        currentWeek.fri.dayDate = firstDayDateOfNextWeek + 5;
                        break;
                    case 6:
                        currentWeek.sat.dayNum = 6;
                        currentWeek.sat.dayDate = firstDayDateOfNextWeek + 6;
                        firstDayDateOfNextWeek = currentWeek.sat.dayDate + 1;
                        break;
                }
                // console.log('NOW:::', currentWeek)
            }

            this.weeks.push(currentWeek);
        }

        this.weeks.push(lastWeek);

        // console.trace('Weeks:::::::', this.weeks)


        for (var k = 0; k < this.monthAttendance.length; k++) {


            for (var i = 0; i < this.weeks.length; i++) {
                for (var j = 0; j < 7; j++) {

                    // switch (this.monthAttendance[k].date) {
                    //     case this.weeks[i].sun.dayDate:
                    //         this.weeks[i].sun.status = this.monthAttendance[k].status;
                    //         console.log('Status:::', this.weeks[i].sun.status)
                    //         break;
                    //     case this.weeks[i].mon.dayDate:
                    //         this.weeks[i].mon.status = this.monthAttendance[k].status;
                    //         break;
                    //     case this.weeks[i].tue.dayDate:
                    //         this.weeks[i].tue.status = this.monthAttendance[k].status;
                    //         break;
                    //     case this.weeks[i].wed.dayDate:
                    //         this.weeks[i].wed.status = this.monthAttendance[k].status;
                    //         break;
                    //     case this.weeks[i].thu.dayDate:
                    //         this.weeks[i].thu.status = this.monthAttendance[k].status;
                    //         break;
                    //     case this.weeks[i].fri.dayDate:
                    //         this.weeks[i].fri.status = this.monthAttendance[k].status;
                    //         break;
                    //     case this.weeks[i].sat.dayDate:
                    //         this.weeks[i].sat.status = this.monthAttendance[k].status;
                    //         break;
                    // }
                    if (this.monthAttendance[k].date == this.weeks[i].sun.dayDate) {
                        this.weeks[i].sun.status = this.monthAttendance[k].status;
                    }
                    else if (this.monthAttendance[k].date == this.weeks[i].mon.dayDate) {
                        this.weeks[i].mon.status = this.monthAttendance[k].status;
                    }
                    else if (this.monthAttendance[k].date == this.weeks[i].tue.dayDate) {
                        this.weeks[i].tue.status = this.monthAttendance[k].status;
                    }
                    else if (this.monthAttendance[k].date == this.weeks[i].wed.dayDate) {
                        this.weeks[i].wed.status = this.monthAttendance[k].status;
                    }
                    else if (this.monthAttendance[k].date == this.weeks[i].thu.dayDate) {
                        this.weeks[i].thu.status = this.monthAttendance[k].status;
                    }
                    else if (this.monthAttendance[k].date == this.weeks[i].fri.dayDate) {
                        this.weeks[i].fri.status = this.monthAttendance[k].status;
                    }
                    else if (this.monthAttendance[k].date == this.weeks[i].sat.dayDate) {
                        this.weeks[i].sat.status = this.monthAttendance[k].status;
                    }
                }


            }
        }
        // console.trace('Weeks:::::::', this.weeks)
        console.log('TOKEN:::', localStorage.getItem('token'))
    }

    //Month is 1-Indexed i.e 12 == December

    getNumberOfDays(year: number, month: number) {
        return new Date(year, month, 0).getDate();
    }

    //Weekday is Sun=0, Mon=1....
    //Month is 1-indexed

    getNoOfWeekDay(year: number, month: number, weekday: number) {

        var day, counter, date;

        day = 1;
        counter = 0;
        date = new Date(year, month - 1, day);
        while (date.getMonth() === month - 1) {
            if (date.getDay() === weekday) { // Sun=0, Mon=1, Tue=2, etc.
                counter += 1;
            }
            day += 1;
            date = new Date(year, month - 1, day);
        }
        return counter;
    }


    onDateLeftButton() {
        this.isLoading = true;
        if (this.currentDate.getMonth() == 0) {
            this.currentDate = new Date(this.currentDate.getFullYear() - 1, 11, 1);
        } else {
            this.currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() - 1, 1);
        }
        if (this.currentDate.getMonth() == 3) {
            this.disableDateLeft = true;
            this.disableDateRight = false;
        } else {
            this.disableDateLeft = false;
            this.disableDateRight = false;
        }

        this.getStudentAttendance();
    }

    onDateRightButton() {

        if (this.currentDate.getMonth() == 11) {
            this.currentDate = new Date(this.currentDate.getFullYear() + 1, 0, 1);
        } else {
            this.currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 1);
        }


        var currentMonth = new Date().getMonth();
        if (this.currentDate.getMonth() == currentMonth || this.currentDate.getMonth() == 2) {
            this.disableDateRight = true;
            this.disableDateLeft = false;
        } else {
            this.disableDateLeft = false;
            this.disableDateRight = false;
        }



        this.getStudentAttendance();
    }

    onDateTap(args: any, dayDate: number) {
        this.selectedDate = dayDate;
        // var dateLabel = <GridLayout>args.object;
        // var label = dateLabel.getChildAt(0);
        // label.color = new Color('#ffffff')
        // setTimeout(() => {
        //     if (dateLabel.android) {
        //         let nativeGridMain = dateLabel.android;
        //         var shape = new android.graphics.drawable.GradientDrawable();
        //         shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
        //         shape.setColor(android.graphics.Color.parseColor('blue'));
        //         nativeGridMain.setBackgroundDrawable(shape);
        //         nativeGridMain.setElevation(20)
        //     } else if (dateLabel.ios) {
        //         let nativeGridMain = dateLabel.ios;
        //         nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
        //         nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
        //         nativeGridMain.layer.shadowOpacity = 0.5
        //         nativeGridMain.layer.shadowRadius = 5.0
        //         nativeGridMain.layer.shadowRadius = 5.0
        //     }
        //     // this.changeDetector.detectChanges();
        // }, 50)
    }

}