import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { HomeTeacherComponent } from "./components/home-teacher.component";

const routes: Routes = [
    { path: "", component: HomeTeacherComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class HomeTeacherRoutingModule { }
