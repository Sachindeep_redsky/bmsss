import { Syllabus } from '~/app/models/syllabus.model';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import * as localstorage from "nativescript-localstorage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { UserService } from "~/app/services/user.service";
import { Downloader, DownloadOptions } from 'nativescript-downloader';
import { Folder } from "tns-core-modules/file-system/file-system";
import * as Toast from 'nativescript-toast';
import { ModalComponent } from "~/app/modals/modal.component";
import { Page } from "tns-core-modules/ui/page/page";

declare const android: any;
declare const CGSizeMake: any;
const downloader = new Downloader();

@Component({
    selector: "app-downloadSyllabus",
    moduleId: module.id,
    templateUrl: "./download-syllabus.component.html",
    styleUrls: ['./download-syllabus.component.css'],
})

export class DownloadSyllabusComponent implements OnInit {
    @ViewChild('downloadProgressDialog', { static: false }) downloadProgressDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    year: string;
    classId: string;
    folder: Folder
    // downloader: Downloader;
    profilePic: string;
    headers: HttpHeaders;
    token: string;
    syllabus: Syllabus;
    downloadProgressValue: number;
    syllabusMessage: string;
    isData: boolean;
    isVisibleButton: boolean;
    constructor(private routerExtensions: RouterExtensions, private http: HttpClient, private userService: UserService, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.classId = "";
        this.token = "";
        this.syllabus = new Syllabus();
        this.downloadProgressValue = 10;
        this.syllabusMessage = "";
        this.isData = true;
        this.isVisibleButton = true;
        this.userService.activeScreen("downloadSyllabus");

        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            this.getClass();
        }
        // this.downloader = new Downloader();
        this.folder = Folder.fromPath('/storage/emulated/0/bmsss-syllabus');
        this.profilePic = "res://profile";

    }

    ngOnInit(): void {
        Downloader.init();
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onOutsideClick() {
        this.downloadProgressDialog.hide();
    }

    getClass() {
        this.http
            .get(Values.BASE_URL + "users/" + localstorage.getItem("userId"), {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.classId = res.data.classId;
                        this.getSyllabus();
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    getSyllabus() {
        this.http
            .get(Values.BASE_URL + "syllabus?classId=" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true && res.data != null) {
                        console.log(res);
                        if (res.data.file_url != null && res.data.file_url != undefined) {
                            this.syllabusMessage = res.data.file_url;
                            var index = this.syllabusMessage.lastIndexOf('/');
                            this.syllabusMessage = this.syllabusMessage.substr(index + 1, this.syllabusMessage.length);
                            this.isVisibleButton = true;
                        }
                        else {
                            this.syllabusMessage = "There is no syllabus uploaded.";
                            this.isVisibleButton = false;
                        }
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    downloadSyllabus() {
        var that = this;
        this.http
            .get(Values.BASE_URL + "syllabus?classId=" + this.classId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true && res.data != null) {
                        console.log(res);
                        var syllabusUrl = res.data.file_url;
                        console.log(syllabusUrl);
                        // downloader = new Downloader();
                        console.log(that.folder.path);
                        var syllabusDownloaderId = downloader.createDownload({
                            url: syllabusUrl,
                            path: that.folder.path,
                        });

                        downloader
                            .start(syllabusDownloaderId, progressData => {
                                console.log('Entwered2')
                                that.downloadProgressDialog.show();
                                that.downloadProgressValue = progressData.value;
                                // this.progressValue = progressData.value;
                                // this.userService.showLoadingState(true);
                                console.log(`Progress : ${progressData.value}%`);
                            })
                            .then(completed => {
                                that.downloadProgressDialog.hide();
                                this.userService.showLoadingState(false);
                                Toast.makeText(`Syllabus downloaded successfully at path : ${completed.path}`, "long").show();
                            })
                            .catch(error => {
                                console.log(error.message);
                            });
                    }
                }
            }, error => {
                this.userService.showLoadingState(false);
                console.log(error.error.error);
            });
    }

    onBack() {
        this.routerExtensions.back();
        // if (localstorage.getItem("userType") != null && localstorage.getItem("userType") == "student") {
        //     this.routerExtensions.navigate(['/homeStudent']);
        // }
        // else {
        //     this.routerExtensions.navigate(['/homeAdmin']);
        // }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

}