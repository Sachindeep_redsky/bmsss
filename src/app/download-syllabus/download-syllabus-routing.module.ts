import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { DownloadSyllabusComponent } from "./components/download-syllabus.component";

const routes: Routes = [
    { path: "", component: DownloadSyllabusComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DownloadSyllabusRoutingModule { }
