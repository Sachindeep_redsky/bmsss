import { RouterExtensions } from 'nativescript-angular/router';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { Color } from "tns-core-modules/color/color";
import { ModalComponent } from "~/app/modals/modal.component";
import { UserService } from "~/app/services/user.service";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as Toast from 'nativescript-toast';
import { Page } from "tns-core-modules/ui/page/page";
import * as localstorage from "nativescript-localstorage";
import { Charges } from '~/app/models/charges.model';

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-feeCharges",
    moduleId: module.id,
    templateUrl: "./fee-charges.component.html",
    styleUrls: ['./fee-charges.component.css'],
})

export class FeeChargesComponent implements OnInit {

    @ViewChild('addChargesDialog', { static: false }) addChargesDialog: ModalComponent;
    @ViewChild('deleteChargesDialog', { static: false }) deleteChargesDialog: ModalComponent;

    chargesList;
    renderViewTimeout;
    isRendering: boolean;
    year: string;
    chargesHint: string;
    chargesIdHint: string;
    chargesBorderColor: string;
    chargesIdBorderColor: string;
    chargesText: string;
    chargesIdText: string;
    dialogAddButton: string;
    type: string;
    chargesId: number;
    charges: Charges;
    token: string;
    headers: HttpHeaders;
    isLoading: boolean;
    feeChargesMessage: string;
    isData: boolean;
    profilePic: string;
    constructor(private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.chargesList = [];
        this.year =  Values.SESSION;
        this.chargesIdHint = "Head id";
        this.chargesHint = "Head name";
        this.chargesBorderColor = "black";
        this.chargesText = "";
        this.chargesIdText = "";
        this.dialogAddButton = "";
        this.chargesId = 0;
        this.charges = new Charges();
        this.isLoading = false;
        this.isData = false;
        this.profilePic = "";
        this.userService.activeScreen("feeCharges");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
            this.headers = new HttpHeaders({
                "Content-Type": "application/json",
                "x-access-token": this.token
            });
            this.getCharges();
        }
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    getCharges() {
        this.isLoading = true;
        this.http
            .get(Values.BASE_URL + "feeCharges", {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        this.chargesList = [];
                        if (res.data.length > 0) {
                            this.isData = false;
                            for (var i = 0; i < res.data.length; i++) {
                                this.chargesList.push({
                                    id: res.data[i].id,
                                    headId: res.data[i].headId,
                                    headName: res.data[i].headName,
                                });
                            }
                            this.isLoading = false;
                        }
                        else {
                            this.isData = true;
                            this.feeChargesMessage = "There is no fee charges added."
                        }
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onBack() {
        this.routerExtensions.back();
    }

    onOutsideClick() {
        this.addChargesDialog.hide();
    }

    onChargesTextChanged(args) {
        this.chargesText = args.object.text;
        this.chargesBorderColor = "white";
        if (this.chargesText == "") {
            this.chargesBorderColor = "black";
        }
    }

    onChargesIdTextChanged(args) {
        this.chargesIdText = args.object.text;
        this.chargesIdBorderColor = "white";
        if (this.chargesIdText == "") {
            this.chargesIdBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onChargesDialogLoaded(args) {
        var chargesDialog = <any>args.object;
        setTimeout(() => {
            if (chargesDialog.android) {
                let nativeImageView = chargesDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#D9D9D9'));
                nativeImageView.setElevation(10)
            } else if (chargesDialog.ios) {
                let nativeImageView = chargesDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)

    }

    onDeleteChargesDialogLoaded(args) {
        var deleteChargesDialog = <any>args.object;
        setTimeout(() => {
            if (deleteChargesDialog.android) {
                let nativeImageView = deleteChargesDialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#D9D9D9'));
                nativeImageView.setElevation(10)
            } else if (deleteChargesDialog.ios) {
                let nativeImageView = deleteChargesDialog.ios;
                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onAddCharges() {
        this.addChargesDialog.show();
        this.dialogAddButton = "Add";
        this.chargesText = "";
        this.chargesIdText = "";
        this.chargesHint = "Head name";
        this.chargesIdHint = "Head id";
        this.type = "";
    }

    onAdd() {
        if (this.chargesIdText == "") {
            alert("Head id cannot be empty.");
        }
        else if (this.chargesText == "") {
            alert("Head name cannot be empty.");
        }
        else {
            this.charges.headId = this.chargesIdText;
            this.charges.headName = this.chargesText;
            this.isLoading = true;
            if (this.type == "edit") {
                console.log(this.charges);
                this.http
                    .put(Values.BASE_URL + "feeCharges/" + this.chargesId, this.charges, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.isLoading = false;
                                Toast.makeText("Charges updated successfully.", "long").show();
                                this.addChargesDialog.hide();
                                this.getCharges();
                            }
                        }
                    }, error => {
                        this.isLoading = false;
                        console.log(error.error.error);
                    });
            }
            else {
                this.chargesText = "";
                this.chargesIdText = "";
                this.http
                    .post(Values.BASE_URL + "feeCharges", this.charges, {
                        headers: this.headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.isLoading = false;
                                Toast.makeText("Charges added successfully.", "long").show();
                                this.addChargesDialog.hide();
                                this.getCharges();
                            }
                        }
                    }, error => {
                        this.isLoading = false;
                        console.log(error.error.error);
                    });
            }
        }
    }

    onEdit(charges: Charges) {
        this.chargesText = charges.headName;
        this.chargesIdText = charges.headId;
        this.addChargesDialog.show();
        this.chargesId = charges.id;
        this.type = "edit";
        this.dialogAddButton = "Update";
    }



    deleteCharges(id: any) {
        this.deleteChargesDialog.show();
        this.chargesId = id;
    }

    onDelete() {
        this.isLoading = true;
        this.http
            .delete(Values.BASE_URL + "feeCharges/delete/" + this.chargesId, {
                headers: this.headers
            })
            .subscribe((res: any) => {
                if (res != null && res != undefined) {
                    if (res.isSuccess == true) {
                        Toast.makeText("Deleted", "long").show();
                        this.getCharges();
                        this.isLoading = false;
                        this.deleteChargesDialog.hide();
                    }
                }
            }, error => {
                this.isLoading = false;
                console.log(error.error.error);
            });
    }

    onCancel() {
        this.deleteChargesDialog.hide();
    }
}