import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { FeeChargesComponent } from "./components/fee-charges.component";

const routes: Routes = [
    { path: "", component: FeeChargesComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class FeeChargesRoutingModule { }
