import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import * as imagepicker from "nativescript-imagepicker";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ImageCropper } from 'nativescript-imagecropper';
import { PhotoViewer, PhotoViewerOptions, PaletteType, NYTPhotoItem } from "nativescript-photoviewer";
import { ModalComponent } from "~/app/modals/modal.component";
import { session, Request } from 'nativescript-background-http';
import { Folder, path, knownFolders, File } from "tns-core-modules/file-system";
import { ImageSource, fromFile } from "tns-core-modules/image-source/image-source";
import * as camera from "nativescript-camera";
import * as permissions from "nativescript-permissions";
import { UserService } from "~/app/services/user.service";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Page } from "tns-core-modules/ui/page/page";
import * as localstorage from "nativescript-localstorage";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-uploadImage",
    moduleId: module.id,
    templateUrl: "./upload-image.component.html",
    styleUrls: ['./upload-image.component.css'],
})

export class UploadImageComponent implements OnInit {
    @ViewChild('photoUploadDialog', { static: false }) photoUploadDialog: ModalComponent;
    @ViewChild('uploadProgressDialog', { static: false }) uploadProgressDialog: ModalComponent;

    renderViewTimeout;
    isRendering: boolean;
    year: string;
    nameHint: string;
    nameBorderColor: string;
    eventName: string;
    from: string;
    isRenderingTextField: boolean;
    heading: string;
    private imageCropper: ImageCropper;
    imageUrl: any;
    photoViewer: PhotoViewer;
    myImages = [];
    file: any;
    url: string;
    imageName: string;
    extension: string;
    shouldImageUpdate: string;
    eventImage: string | ImageSource;
    folderId: string;
    type: string;
    isVisibleImage: boolean;
    uploadProgressValue: number;
    profilePic: string;
    token: string;
    count: number;
    constructor(private router: Router, private route: ActivatedRoute, private userService: UserService, private http: HttpClient, private page: Page) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.nameHint = "Folder name";
        this.nameBorderColor = "black";
        this.from = "";
        this.isRenderingTextField = false;
        this.heading = "";
        this.imageCropper = new ImageCropper();
        this.imageUrl = null;
        this.photoViewer = new PhotoViewer();
        this.eventImage = "res://add_image";
        this.extension = 'jpg';
        this.eventName = "";
        this.folderId = "";
        this.type = "";
        this.shouldImageUpdate = "true";
        this.isVisibleImage = true;
        this.uploadProgressValue = 10;
        this.profilePic = "res://profile";
        this.token = "";
        this.count = 0;
        this.userService.activeScreen("uploadImage");
        if (localstorage.getItem("token") != null && localstorage.getItem("token") != undefined) {
            this.token = localstorage.getItem("token");
        }

        this.route.queryParams.subscribe(params => {
            this.folderId = params["folderId"];
            this.from = params["from"];
            this.type = params["type"];
        });

        if (this.type == "edit") {
            if (this.folderId != null && this.folderId != undefined) {
                this.userService.showLoadingState(true);
                var headers = new HttpHeaders({
                    "Content-Type": "application/json",
                    "x-access-token": this.token,
                });
                this.http
                    .get(Values.BASE_URL + "events/" + this.folderId, {
                        headers: headers
                    })
                    .subscribe((res: any) => {
                        if (res != null && res != undefined) {
                            if (res.isSuccess == true) {
                                this.eventName = res.data.name;
                                this.eventImage = res.data.image_url;
                                this.userService.showLoadingState(false);
                            }
                        }
                    }, error => {
                        this.userService.showLoadingState(false);
                        console.log(error.error.error);
                    });
            }
        }

        if (this.from != "" && this.from == "addImages") {
            this.isRenderingTextField = false;
            this.heading = "Add image";
        }
        else {
            this.isRenderingTextField = true;
            this.heading = "Add folder";
        }
        console.log(this.from);
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }
    }

    onBack() {
        this.router.navigate(['/addImagesFolder'])
    }

    onNameTextChanged(args) {
        this.eventName = args.object.text;
        this.nameBorderColor = "#A61F21";
        if (this.eventName == "") {
            this.nameBorderColor = "black";
        }
    }

    onUploadImage() {
        this.photoUploadDialog.show();
    }

    onGallery() {
        this.photoUploadDialog.hide();
        var that = this;
        let context = imagepicker.create({
            mode: "single"
        });
        context
            .authorize()
            .then(() => {
                return context.present();
            })
            .then(selection => {
                selection.forEach(function (selected) {
                    var image = new ImageSource();
                    image.fromAsset(selected).then((source) => {
                        that.imageCropper.show(source, { lockSquare: true }).then((args: any) => {
                            if (args.image !== null) {
                                var folder: Folder = Folder.fromPath("/storage/emulated/0" + "/bmsss");
                                var file: File = File.fromPath(path.join(folder.path, 'bmsss.jpg'));
                                args.image.saveToFile(file.path, 'jpg');
                                that.file = "/storage/emulated/0/bmsss/bmsss.jpg";
                                that.imageName = that.file.substr(that.file.lastIndexOf("/") + 1);
                                that.extension = that.imageName.substr(that.imageName.lastIndexOf(".") + 1);
                                that.eventImage = undefined;
                                that.eventImage = fromFile("/storage/emulated/0/bmsss/bmsss.jpg");
                                that.isVisibleImage = false;
                                that.shouldImageUpdate = "true";
                            }
                        })
                            .catch(function (e) {
                                console.log(e);
                            });
                    }).catch((err) => {
                        console.log("Error -> " + err.message);
                    });
                });
            });
    }

    onCamera() {
        this.photoUploadDialog.hide();
        if (camera.isAvailable()) {
            var that = this;
            permissions.requestPermission([android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE])
                .then(() => {
                    camera.takePicture({ width: 512, height: 512, keepAspectRatio: true })
                        .then((selected) => {
                            let source = new ImageSource();
                            source.fromAsset(selected).then((source) => {
                                this.imageCropper.show(source, { lockSquare: true }).then((args) => {
                                    if (args.image !== null) {
                                        var folder: Folder = Folder.fromPath("/storage/emulated/0" + "/bmsss");
                                        var file: File = File.fromPath(path.join(folder.path, 'bmsss.jpg'));
                                        args.image.saveToFile(file.path, 'jpg');
                                        that.file = "/storage/emulated/0/bmsss/bmsss.jpg";
                                        that.imageName = that.file.substr(that.file.lastIndexOf("/") + 1);
                                        that.extension = that.imageName.substr(that.imageName.lastIndexOf(".") + 1);
                                        that.eventImage = undefined;
                                        that.eventImage = fromFile("/storage/emulated/0/bmsss/bmsss.jpg");
                                        that.isVisibleImage = false;
                                        that.shouldImageUpdate = "true";
                                    }
                                })
                                    .catch(function (e) {
                                        console.log(e);
                                    });
                            });
                        }).catch((err) => {
                            console.log("Error -> " + err.message);
                        });
                })
                .catch(function () {
                    alert("User denied permissions");
                });
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onGridLoaded(args) {
        var grid = <any>args.object;

        setTimeout(() => {
            if (grid.android) {
                let nativeImageView = grid.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#FFFFFF'));
                nativeImageView.setElevation(10)
            } else if (grid.ios) {
                let nativeImageView = grid.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onSubmit() {
        this.count++;
        if (this.count == 1) {
            if (this.eventImage == null) {
                alert("Please select event image.");
            }
            else if (this.eventName == "" && this.from == "addImagesFolder") {
                alert("Please enter event name.");
            }
            // console.log(this.imageUrl);
            // this.myImages = ["res://profilepic"];
            // let photoviewerOptions: PhotoViewerOptions = {
            //     startIndex: 0,
            //     android: {
            //         paletteType: PaletteType.DarkVibrant,
            //         showAlbum: false
            //     }
            // };
            // this.photoViewer.showGallery(this.myImages, photoviewerOptions);
            this.userService.showLoadingState(true);
            var that = this;
            var mimeType = "image/" + that.extension;
            var uploadSession = session('image-upload');
            if (that.from == "addImages") {
                var request = {
                    url: Values.BASE_URL + "events/files",
                    method: "POST",
                    headers: {
                        "Content-Type": "application/octet-stream",
                        "File-Name": "my file",
                        "x-access-token": that.token
                    },
                    // headers: headers,
                    description: "{'uploading':" + "my file" + "}"
                }
                const params = [
                    { name: "file", filename: that.file, mimeType: mimeType },
                    { name: "event_id", value: that.folderId }
                ]
                var task = uploadSession.multipartUpload(params, request);
                task.on("progress", (e) => {
                    this.uploadProgressValue = (e.currentBytes / e.totalBytes) * 100;
                    this.uploadProgressDialog.show();
                });

                task.on("responded", (e: any) => {
                    this.userService.showLoadingState(false);
                    let navigationExtras: NavigationExtras = {
                        queryParams: {
                            "folderId": this.folderId
                        }
                    }
                    this.uploadProgressDialog.hide();
                    this.router.navigate(['./addImages'], navigationExtras);
                });
                task.on("error", (e) => {
                    console.log("ERRORRR:::::", e.error);
                    this.uploadProgressDialog.hide();
                    alert("May be your network connection is low.");
                });
                task.on("complete", this.completeEvent);
            }
            else {
                if (that.type == "edit") {
                    if (that.file == null) {
                        that.file = "/storage/emulated/0/farmersHut/FarmersHut.jpg";
                        that.shouldImageUpdate = "false";
                    }
                    var headers = new HttpHeaders({
                        "Content-Type": "application/octet-stream",
                        "x-access-token": that.token,
                        "File-Name": "my file"
                    });
                    var request = {
                        url: Values.BASE_URL + "events",
                        method: "POST",
                        headers: {
                            "Content-Type": "application/octet-stream",
                            "File-Name": "my file",
                            "x-access-token": that.token
                        },
                        // headers: headers,
                        description: "{'uploading':" + "my file" + "}"
                    }
                    const params = [
                        { name: "file", filename: that.file, mimeType: mimeType },
                        { name: "name", value: that.eventName },
                        { name: "isUpdate", value: "true" },
                        { name: "shouldImageUpdate", value: that.shouldImageUpdate },
                        { name: "event_id", value: that.folderId }
                    ]
                    console.log("REQUEST::::", request);
                    console.log("PARAMS::::", params);
                    var task = uploadSession.multipartUpload(params, request);
                    task.on("progress", (e) => {
                        this.uploadProgressValue = (e.currentBytes / e.totalBytes) * 100;
                        this.uploadProgressDialog.show();
                    });
                    task.on("responded", (e: any) => {
                        this.userService.showLoadingState(false);
                        this.uploadProgressDialog.hide();
                        this.router.navigate(['./addImagesFolder']);
                    });
                    task.on("error", (e) => {
                        console.log("ERRORRR:::::", JSON.stringify(e.error));
                        this.uploadProgressDialog.hide();
                        alert("May be your network connection is low.");
                    });
                    task.on("complete", this.completeEvent);
                }
                else {
                    var request = {
                        url: Values.BASE_URL + "events",
                        method: "POST",
                        headers: {
                            "Content-Type": "application/octet-stream",
                            "File-Name": "my file",
                            "x-access-token": that.token,
                        },
                        // headers: headers,
                        description: "{'uploading':" + "my file" + "}"
                    }
                    const params = [
                        { name: "file", filename: that.file, mimeType: mimeType },
                        { name: "name", value: that.eventName },
                    ]
                    console.log("REQUEST::::", request);
                    console.log("PARAMS::::", params);
                    var task = uploadSession.multipartUpload(params, request);
                    task.on("progress", (e) => {
                        this.uploadProgressValue = (e.currentBytes / e.totalBytes) * 100;
                        this.uploadProgressDialog.show();
                    });
                    task.on("responded", (e: any) => {
                        this.userService.showLoadingState(false);
                        this.uploadProgressDialog.hide();
                        this.router.navigate(['./addImagesFolder']);
                    });
                    task.on("error", (e) => {
                        console.log("ERRORRR:::::", JSON.stringify(e.error));
                        this.uploadProgressDialog.hide();
                        alert("May be your network connection is low.");
                    });
                    task.on("complete", this.completeEvent);
                }
            }
        }
    }

    respondedEvent(e) {
        console.log("RESPONSE: " + e.data);
    }

    errorEvent(e) {
        console.log("Error is: " + JSON.stringify(e));
    }

    completeEvent(e) {
        console.log("Completed :" + JSON.stringify(e));
    }

    onOutsideClick() {
        this.photoUploadDialog.hide();
    }
}