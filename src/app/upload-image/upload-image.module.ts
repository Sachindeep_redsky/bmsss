import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { UploadImageComponent } from "./components/upload-image.component";
import { UploadImageRoutingModule } from "./upload-image-routing.module";
import { GridViewModule } from 'nativescript-grid-view/angular';
// import { NgShadowModule } from 'nativescript-ng-shadow';
import { NgModalModule } from "../modals/ng-modal";

@NgModule({
    imports: [
        HttpModule,
        UploadImageRoutingModule,
        GridViewModule,
        // NgShadowModule,
        NgModalModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        UploadImageComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class UploadImageModule { }
