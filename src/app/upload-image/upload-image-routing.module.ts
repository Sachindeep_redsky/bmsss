import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { UploadImageComponent } from "./components/upload-image.component";

const routes: Routes = [
    { path: "", component: UploadImageComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class UploadImageRoutingModule { }
