import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "RadioButton",
    moduleId: module.id,
    styleUrls: ["./radio-button.component.css"],
    templateUrl: "./radio-button.component.html",
})
export class RadioButtonComponent {
    @Input() width: string = "50vh"
    @Input() height: string = "20vh"
    @Input() leftWidth: string = this.height;
    @Input() rightWidth: string = this.height;
    // @Input() borderColor: string = "red"
    @Input() borderRadius: string = "10vh"
    @Input() borderWidth: string = "3.75vh"
    @Input() backgroundColor: string = "#ffffff"
    @Input() selectedLeftColor: string = "#F66236"
    @Input() selectedRightColor: string = "#6D7CF1"

    @Input() buttonLeftMarginTop: string = '0';
    @Input() buttonLeftMarginLeft: string = '3.33vh';
    @Input() buttonLeftMarginRight: string = "1.66vh";
    @Input() buttonLeftMarginBottom: string = '0';
    @Input() buttonRightMarginTop: string = '0';
    @Input() buttonRightMarginRight: string = '3.33vh';
    @Input() buttonRightMarginLeft: string = "1.66vh";
    @Input() buttonRightMarginBottom: string = '0';

    // @Input() leftLabelText: string = "Left"
    // @Input() rightLabelText: string = "Right"
    // @Input() leftLabelTextColor: string = "black"
    // @Input() rightLabelTextColor: string = "white"
    // @Input() fontSize: string = "24"
    // @Input() alignment: string = "left"


    @Output() leftButton = new EventEmitter();
    @Output() rightButton = new EventEmitter();
    @Output() whichOne = new EventEmitter();

    // selectedLeftColor: string = this.backgroundColor;
    // selectedRightColor: string = this.backgroundColor;

    leftButtonColor: string;
    rightButtonColor: string;
    leftCount: number;
    rightCount: number;

    constructor() {
        this.leftButtonColor = this.backgroundColor;
        this.rightButtonColor = this.backgroundColor;
        this.leftCount = 0;
        this.rightCount = 0;
        console.log("REsult:", this.getNumberFromVh('34vh'))
    }

    public onLeftClick(arg: number) {
        // this.selectedLeftColor = this.selectedColor;
        // this.selectedLeftColor = this.
        this.rightButtonColor = this.backgroundColor;
        this.leftButtonColor = this.selectedLeftColor;
        this.leftCount++;

        if (this.rightButtonColor == this.backgroundColor && this.leftButtonColor == this.selectedLeftColor && this.leftCount == 2) {
            this.leftButtonColor = this.backgroundColor;
            this.whichOne.emit('')
            this.leftCount = 0;
        } else {
            this.whichOne.emit('left')
        }
        // this.leftLabelTextColor = "black"
        // this.rightLabelTextColor = "white"
        this.leftButton.emit(arg)
    }

    public onRightClick(arg: number) {
        // this.selectedRightColor = this.selectedColor;
        // this.leftLabelTextColor = "white"
        // this.rightLabelTextColor = "black"
        this.rightButtonColor = this.selectedRightColor;
        this.leftButtonColor = this.backgroundColor;
        this.rightCount++;

        if (this.rightButtonColor == this.selectedRightColor && this.leftButtonColor == this.backgroundColor && this.rightCount == 2) {
            this.rightButtonColor = this.backgroundColor;
            this.whichOne.emit('')
            this.rightCount = 0;
        } else {
            this.whichOne.emit('right')
        }
        this.rightButton.emit(arg)
    }

    public getNumberFromVh(stringWithUnit: string): number {
        if (!stringWithUnit) {
            return 0;
        }
        var total = stringWithUnit.length;

        console.log('Length:', total)
        console.log(stringWithUnit.substring(0, 2))
        for (var i = 0; i < total; i++) {
            if (Number.parseInt(stringWithUnit.charAt(i)) == NaN) {
                if (i == 0) {
                    console.log('isZero:', '0')

                    return 0;
                }
                console.log('CharAt', i, 'is', stringWithUnit.charAt(i), 'Coverted no. id', Number.parseInt(stringWithUnit.charAt(i)))

                return Number.parseInt(stringWithUnit.substring(0, i));
            }

        }

    }

}