import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { RadioButtonComponent } from "./components/radio-button.component";


@NgModule({
    imports: [NativeScriptCommonModule],
    declarations: [RadioButtonComponent],
    schemas: [NO_ERRORS_SCHEMA],
    exports: [RadioButtonComponent]
})

export class RadioButtonModule { }