import { UserService } from './../../services/user.service';
import { Component, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { Router, NavigationExtras } from "@angular/router";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-downloadImage",
    moduleId: module.id,
    templateUrl: "./download-image.component.html",
    styleUrls: ['./download-image.component.css'],
})

export class DownloadImageComponent implements OnInit {

    renderViewTimeout;
    isRendering: boolean;
    nameHint: string;
    nameBorderColor: string;
    imageName: string;
    image: string;

    constructor(private router: Router, private userService: UserService) {
        this.isRendering = false;
        this.nameHint = "Image name";
        this.nameBorderColor = "black";
        this.image = "res://profilepic";
        this.userService.activeScreen("downloadImage");
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
    }

    onNameTextChanged(i: number, args) {
        this.imageName = args.object.text;
        this.nameBorderColor = "white";
        if (this.imageName == "") {
            this.nameBorderColor = "black";
        }
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#888888'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onGridLoaded(args) {
        var grid = <any>args.object;

        setTimeout(() => {
            if (grid.android) {
                let nativeImageView = grid.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#6D7CF1'));
                nativeImageView.setElevation(20)
            } else if (grid.ios) {
                let nativeImageView = grid.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onBack() {
        alert("back click");
    }

    onSubmit() {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "imageName": this.imageName
            }
        }
        alert("submit click!!!");
        // this.router.navigate(['./appliedLeave'], navigationExtras);
    }
}