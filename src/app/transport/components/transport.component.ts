import { Component, OnInit, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { RouterExtensions } from "nativescript-angular/router";
import * as localstorage from "nativescript-localstorage";
import { Values } from "~/app/values/values";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from "~/app/services/user.service";
import * as Toast from 'nativescript-toast';
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { ModalComponent } from "~/app/modals/modal.component";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "app-transport",
    moduleId: module.id,
    templateUrl: "./transport.component.html",
    styleUrls: ['./transport.component.css'],
})

export class TransportComponent implements OnInit {

    renderViewTimeout;
    isRendering: boolean;
    transport;
    year: string;
    isVillageTransport: boolean;
    isLoading: boolean;
    villageTransport;
    cityTransport;
    profilePic: string;
    constructor(private router: Router, private http: HttpClient, private userService: UserService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.isRendering = false;
        this.year =  Values.SESSION;
        this.transport = [];
        this.villageTransport = [];
        this.cityTransport = [];
        this.isLoading = false;
        this.isVillageTransport = true;
        this.profilePic = "res://profile";
        this.userService.activeScreen("transport");
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        if (localstorage.getItem("profilePic") != null && localstorage.getItem("profilePic") != undefined) {
            this.profilePic = localstorage.getItem("profilePic");
        }

        this.villageTransport.push({ driverName: "Sh. Jai Parkash", contactNumber: "9417977637", address: "Ramsara" });
        this.villageTransport.push({ driverName: "S. Palwinder Singh", contactNumber: "9464209425", address: "JhurarKhera" });
        this.villageTransport.push({ driverName: "S. Gurdev Singh", contactNumber: "8968483685", address: "Bhawwala" });
        this.villageTransport.push({ driverName: "S. Pala Singh", contactNumber: "9878379635", address: "Rajpura, Doda" });
        this.villageTransport.push({ driverName: "S. Bagicha Singh (Raju)", contactNumber: "9876712483", address: "Kandhwala, Dharampura" });
        // this.villageTransport.push({ driverName: "asddddf", contactNumber: "1234567890", address: "asgd ghjgasd jgasdjgsd" });
        // this.villageTransport.push({ driverName: "asddddf", contactNumber: "1234567890", address: "asgd ghjgasd jgasdjgsd" });
        // this.villageTransport.push({ driverName: "asddddf", contactNumber: "1234567890", address: "asgd ghjgasd jgasdjgsd" });
        // this.villageTransport.push({ driverName: "asddddf", contactNumber: "1234567890", address: "asgd ghjgasd jgasdjgsd" });
        // this.villageTransport.push({ driverName: "asddddf", contactNumber: "1234567890", address: "asgd ghjgasd jgasdjgsd" });
        // this.villageTransport.push({ driverName: "asddddf", contactNumber: "1234567890", address: "asgd ghjgasd jgasdjgsd" });

        this.cityTransport.push({ driverName: "S. Satpal Singh", contactNumber: "9780121583", address: "Abohar City" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });
        // this.cityTransport.push({ driverName: "ram kumar", contactNumber: "7665656565", address: "jkh jkhjkwqheu qwewq" });

        this.transport = this.villageTransport;
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onImageLoaded(args) {
        var image = <any>args.object;

        setTimeout(() => {
            if (image.android) {
                let nativeImageView = image.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.OVAL);
                shape.setColor(android.graphics.Color.parseColor('#ffffff'));
                nativeImageView.setBackgroundDrawable(shape);
                nativeImageView.setElevation(20)
            } else if (image.ios) {
                let nativeImageView = image.ios;

                nativeImageView.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeImageView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeImageView.layer.shadowOpacity = 0.5
                nativeImageView.layer.shadowRadius = 5.0
            }
        }, 400)
    }

    onBack() {
        this.routerExtensions.back();
    }

    onVillageTransport() {
        this.isVillageTransport = true;
        this.transport = [];
        this.transport = this.villageTransport;
    }

    onCityTransport() {
        this.isVillageTransport = false;
        this.transport = [];
        this.transport = this.cityTransport;
    }

}